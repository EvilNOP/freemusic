//
//  AppDelegate.m
//  Drive
//
//  Created by EvilNOP on 15/3/3.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "AppDelegate.h"
#import "SettingsViewController.h"
#import "FreeMusic-Swift.h"

@import OneDriveSDK;
@interface AppDelegate () {
    UIBackgroundTaskIdentifier _backgroundTaskID;
    NSDate *_lastRatingTime;
}

@end


@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [FEUtil sharedInstance];
    [Global sharedInstance];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = FEHexRGBA(0xffffff, 1.0);
    [self.window makeKeyAndVisible];
    
    RootController *c = [[RootController alloc] init];
    [self.window setRootViewController:c];
    
    [MobClick startWithAppkey:kUmengAppKey];
    
    FEAddObserver(self, OnlineParamsManagerDidUpdateParamsNotification, @selector(onlineParamsDidUpdate:));
    
    [OnlineDataManager setupWithAppKey:kOnlineDataAppKey];
    [OnlineDataManager run];
    
    [Dropbox setupWithAppKey:kDropboxAppKey];
    [ODClient setMicrosoftAccountAppId:kOneDriveAppKey scopes:@[@"onedrive.readwrite"]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    _backgroundTaskID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        //
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.

    [[UIApplication sharedApplication] endBackgroundTask:_backgroundTaskID];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    
    [kParamsManager update];
    
    //
    if (_lastRatingTime) {
        if ([[NSDate new] timeIntervalSinceDate:_lastRatingTime] < 15) {
            [self showRatingAlertIfNeeded:false];
        }
        else {
            FESetUserDefaults(kUDKeyHasRate, @YES);
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [DropboxHelper handleRedirectURL:url];
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event
{
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlNextTrack:
            [kGlobal.playerViewController playNextTrack];
            break;
        case UIEventSubtypeRemoteControlPreviousTrack:
            [kGlobal.playerViewController playPreviousTrack];
            break;
        case UIEventSubtypeRemoteControlPlay:
            [kGlobal.playerViewController play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [kGlobal.playerViewController pause];
            break;
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if (kGlobal.playerViewController.isPlaying) {
                [kGlobal.playerViewController pause];
            }
            else {
                [kGlobal.playerViewController play];
            }
            break;
        default:
            break;
    }
}

#pragma mark - Notification
- (void)onlineParamsDidUpdate:(NSNotification*)notification {
    
    // FLOW模式改变
    NSInteger newFLOWMode = [kParamsManager integerForName:kParamNameFLOWMode];
    if (newFLOWMode != FEUserDefaultsIntegerForKey(kUDKeyFLOWMode)) {
        
        BOOL shouldChangeLocalFLOWMode = YES;
        if (newFLOWMode == FLOWModeLimited &&
            [kParamsManager boolForName:kParamNameForceToSetFLOWMode] == NO &&
            [kGlobal canOpenFLOWableSwitch]) {
            
            shouldChangeLocalFLOWMode = NO;
        }
        
        if (shouldChangeLocalFLOWMode) {
            FESetUserDefaults(kUDKeyFLOWMode, @(newFLOWMode));
            FEPostNotification(kFLOWModeChangedNotification, nil, nil);
        }
    }
    
    // 判断是否强制关闭FLOWable开关
    if (FEUserDefaultsIntegerForKey(kUDKeyFLOWMode) == FLOWModeLimited ||
        ( [kGlobal canOpenFLOWableSwitch] == NO && FEUserDefaultsBoolForKey(kUDKeyFLOWableSwitchOn) )
        
        ) {
        FESetUserDefaults(kUDKeyFLOWableSwitchOn, @NO);
        FEPostNotification(kForceFLOWableSwitchToOffNotification, nil, nil);
    }
    
    //
    if (FEUserDefaultsIntegerForKey(kUDKeyLoginTimes) == 1) {
        [self showRatingAlertIfNeeded:true];
    }
    
    //
    FEPostNotification(kParamsManagerDidUpdateParamsNotification, nil, nil);
}

- (void)showRatingAlertIfNeeded:(BOOL)strict {
    _lastRatingTime = nil;
    
    if (FEUserDefaultsBoolForKey(kUDKeyHasRate) == false && [kParamsManager boolForName:kParamNameShowsRating]) {
        NSString *title = [kParamsManager valueForName:kParamNameRatingTitle];
        NSString *message = [kParamsManager valueForName:kParamNameRatingMessage];
        NSString *cancelTitle = [kParamsManager valueForName:kParamNameRatingCancelTitle];
        NSString *confirmTitle = [kParamsManager valueForName:kParamNameRateingConfirmTitle];
        
        FEAlertController *alert = [[FEAlertController alloc] initWithTitle:title message:message buttonTitles:@[confirmTitle] cancelButtonTitle:cancelTitle destructiveButtonTitle:nil preferredStyle:UIAlertControllerStyleAlert handler:^(FEAlertController *c, NSString *buttonTitle) {
            if ([buttonTitle isEqualToString:confirmTitle]) {
                [kUtil openAppInAppStoreWithAppID:kAppID];
                
                if (strict) {
                    _lastRatingTime = [NSDate new];
                }
                else {
                    FESetUserDefaults(kUDKeyHasRate, @YES);
                }
            }
            else {
                if (strict) {
                    setTimeout(0.2, ^{
                        [self showRatingAlertIfNeeded:false];
                    });
                }
            }
        }];
        [alert showInController:[kGlobal topViewController] animated:YES completion:nil];
    }
}

@end

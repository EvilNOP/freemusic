//
//  main.m
//  Drive
//
//  Created by EvilNOP on 15/3/3.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

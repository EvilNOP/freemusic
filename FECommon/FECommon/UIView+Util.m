//
//  UIView+Mixed.m
//  FECommon
//
//  Created by EvilNOP on 16/1/7.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import "UIView+Util.h"

@implementation UIView (Util)

- (CGRect)convertBoundsToView:(UIView*)view {
    return [self convertRect:self.bounds toView:view];
}

@end

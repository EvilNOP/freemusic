//
//  NSArray+Util.h
//  Account
//
//  Created by EvilNOP on 1/18/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Util)

- (NSArray*)filter:(NSArray*)values;
- (BOOL)objectsContainedByArray:(NSArray*)array;
- (NSDictionary*)group:(id (^)(id item))enumBlock;
- (id)reduceWithInitialValue:(id)initialValue enumBlock:(id (^)(id previousValue, id item, NSInteger index))aBlock;
- (id)objectAtIndexSafely:(NSUInteger)index;

@end

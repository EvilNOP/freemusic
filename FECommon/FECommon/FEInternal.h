//
//  FEInternal.h
//  FECommon
//
//  Created by EvilNOP on 5/23/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

#define _(key) [FEInternal localizedString:key]

@interface FEInternal : NSObject

+ (NSString*)localizedString:(NSString*)key;

@end

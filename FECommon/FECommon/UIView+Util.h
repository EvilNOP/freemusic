//
//  UIView+Mixed.h
//  FECommon
//
//  Created by EvilNOP on 16/1/7.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Util)

- (CGRect)convertBoundsToView:(UIView*)view;

@end

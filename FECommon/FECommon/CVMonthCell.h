//
//  CalendarViewCell.h
//  Account
//
//  Created by EvilNOP on 1/11/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CVMonthCellDelegate;

//
@interface CVMonthCell : UICollectionViewCell

@property (weak) id<CVMonthCellDelegate> delegate;
@property (nonatomic, strong) NSDate *selectedDate;
@property (nonatomic, strong) NSDateComponents *monthComponents;

@property (nonatomic, strong) UIColor *weekTextColor;
@property (nonatomic, strong) UIColor *dayTextColor;
@property (nonatomic, strong) UIColor *dayFadeoutTextColor;
@property (nonatomic, strong) UIColor *selectedDayBackgroundColor;
@property (nonatomic, strong) UIColor *selectedDayTextColor;

@end


//
@protocol CVMonthCellDelegate <NSObject>

@optional
- (void)monthCell:(CVMonthCell*)monthCell didSelectDate:(NSDate*)date;

@end

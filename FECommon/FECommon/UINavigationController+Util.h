//
//  UINavigationController+Util.h
//  Account
//
//  Created by EvilNOP on 16/2/1.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (Util)

@property (nonatomic, readonly) id rootViewController;

@end

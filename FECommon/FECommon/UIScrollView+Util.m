//
//  UIScrollView+Util.m
//  Account
//
//  Created by EvilNOP on 3/16/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "UIScrollView+Util.h"
#import "UIView+Position.h"

@implementation UIScrollView (Util)

- (CGFloat)offsetX {
    return self.contentOffset.x;
}

- (void)setOffsetX:(CGFloat)offsetX {
    self.contentOffset = CGPointMake(offsetX, self.contentOffset.y);
}

- (CGFloat)offsetY {
    return self.contentOffset.y;
}

- (void)setOffsetY:(CGFloat)offsetY {
    self.contentOffset = CGPointMake(self.contentOffset.x, offsetY);
}

- (CGFloat)offsetRight {
    return self.contentOffset.x + self.width;
}

- (void)setOffsetRight:(CGFloat)offsetRight {
    self.contentOffset = CGPointMake(offsetRight - self.width, self.contentOffset.y);
}

- (CGFloat)offsetBottom {
    return self.contentOffset.y + self.height;
}

- (void)setOffsetBottom:(CGFloat)offsetBottom {
    self.contentOffset = CGPointMake(self.contentOffset.x, offsetBottom - self.height);
}

@end

//
//  FEInternal.m
//  FECommon
//
//  Created by EvilNOP on 5/23/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

@implementation FEInternal

+ (NSString*)localizedString:(NSString*)key {
    static NSBundle *bundle = nil;
    if (bundle == nil) {
        bundle = [NSBundle bundleForClass:[self class]];
    }
    return NSLocalizedStringWithDefaultValue(key, @"FELocalizations", bundle, @"", @"");
}

@end

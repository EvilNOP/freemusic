//
//  UINavigationController+Util.m
//  Account
//
//  Created by EvilNOP on 16/2/1.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import "UINavigationController+Util.h"

@implementation UINavigationController (Util)

- (id)rootViewController {
    return [self.viewControllers firstObject];
}

@end

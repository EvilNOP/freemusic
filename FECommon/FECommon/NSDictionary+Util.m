//
//  NSDictionary+Util.m
//  Account
//
//  Created by EvilNOP on 16/2/5.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import "NSDictionary+Util.h"

@implementation NSDictionary (Util)

- (void)sortKeysUsingComparator:(NSComparator)cmptr thenEnumerateKeysAndObjectsUsingBlock:(void (^)(id key, id obj, BOOL *stop))block {
    NSArray *keysSorted = [[self allKeys] sortedArrayUsingComparator:cmptr];
    for (id key in keysSorted) {
        BOOL stop = NO;
        block(key, self[key], &stop);
        if (stop) {
            break;
        }
    }
}

@end

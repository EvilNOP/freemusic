//
//  ADInfo.h
//  HiFolder
//
//  Created by Tracy on 15/8/1.
//
//

#import <Foundation/Foundation.h>

@interface ADApp : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *nameWithFree;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *appID;
@property (nonatomic, strong) NSString *urlScheme;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *numberOfReviews;
@property (nonatomic, strong) NSString *details;
@property (nonatomic, strong) NSString *descriptions;
@property (nonatomic, strong) NSString *iconPath;
@end

@interface ADMoreAppsInfo : NSObject
@property (nonatomic, strong) ADApp *app;
@property (nonatomic, strong) NSArray *screenshotPathList;
@end

@interface ADTask : NSObject
@property (nonatomic, strong) ADApp *app;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *cancelTitle;
@property (nonatomic, strong) NSString *confirmTitle;
@end

@interface ADAlertInfo : NSObject
@property (nonatomic, strong) ADApp *app;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *cancelTitle;
@property (nonatomic, strong) NSString *confirmTitle;
@end

@interface ADSheetInfo : NSObject
@property (nonatomic, strong) ADApp *app;
@property (nonatomic, strong) NSString *imagePath;
@end

@interface ADImageInfo : NSObject
@property (nonatomic, strong) ADApp *app;
@property (nonatomic, strong) NSString *imagePath;
@property (nonatomic, strong) NSString *url;
@end


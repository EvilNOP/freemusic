//
//  ADManager.h
//  HiFolder
//
//  Created by Tracy on 14-7-14.
//
//

#import <Foundation/Foundation.h>
#import "ADDef.h"

#define ADInfoListDidUpdateNotification @"ADInfoListDidUpdateNotification"

@interface ADManager : NSObject

+ (void)run;

+ (void)updateToVersion:(int)version;
+ (int)currentVersion;

+ (NSArray*)moreAppsInfoList;
+ (NSArray*)sheetAdInfoList;
+ (NSArray*)imageAdInfoList;
+ (NSArray*)alertAdInfoList;
+ (ADTask*)task;

+ (void)setAppDownloaded:(NSString*)appID;

@end

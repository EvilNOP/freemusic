//
//  ADManager.m
//  HiFolder
//
//  Created by Tracy on 14-7-14.
//
//

#import "ADManager.h"
#import "OnlineDataManager.h"
#import <FECommon/FECommon.h>
#import "JSONKit.h"
#import "Objective-Zip.h"
#import "AFNetworking.h"

#define kLocalADPath [[FEUtil sharedInstance] join:[[FEUtil sharedInstance] documentsPath], @".ad", nil]
#define kUDKeyADVersion @"ADM.m.1"
#define kUDKeyADExistUnreadUpdatesFlag @"ADM.m.2"
#define kUDKeyADHasDownloadedAppIDList @"ADM.m.3"

id local(NSDictionary* dict);
id local(NSDictionary* dict) {
    if ([dict isKindOfClass:[NSDictionary class]] == NO) {
        return dict;
    }
    
    NSString *c = [[[NSBundle mainBundle] preferredLocalizations] firstObject]; //language code
    id r = dict[c];
    if (r == nil) {
        r = dict[@"en"];
    }
    
    return r;
}

static NSDictionary *JSON;
static NSMutableArray *apps;

@implementation ADManager

+ (void)run {
    BOOL existsLocalAd = [[NSFileManager defaultManager] fileExistsAtPath:kLocalADPath];
    if (existsLocalAd) {
        [self loadData];
    }
}

+ (void)updateToVersion:(int)version {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSURL *url = [OnlineDataManager adURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    SDT *task = [manager DLTaskWithRequest:request progress:nil destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        NSString *tmpZipPath = [[FEUtil sharedInstance] join:[[FEUtil sharedInstance] tmpPath], @"ad.zip", nil];
        [[NSFileManager defaultManager] removeItemAtPath:tmpZipPath error:nil];
        return [NSURL fileURLWithPath:tmpZipPath];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@", error);
            return;
        }
        
        [self loadZipDataAtPath:filePath.path];
        NSLog(@"new version %d", version);
        FESetUserDefaults(kUDKeyADVersion, [NSNumber numberWithInt:version]);
        FESetUserDefaults(kUDKeyADExistUnreadUpdatesFlag, @YES);
        [[NSNotificationCenter defaultCenter] postNotificationName:ADInfoListDidUpdateNotification
                                                            object:nil];
    }];
    [task resume];
}

+ (int)currentVersion {
    return (int)FEUserDefaultsIntegerForKey(kUDKeyADVersion);
}

+ (BOOL)isAppInstalled:(ADApp*)app {
    if (app.urlScheme.length == 0) {
        NSArray *hasDownloadedAppIDList = FEUserDefaultsValueForKey(kUDKeyADHasDownloadedAppIDList);
        return [hasDownloadedAppIDList containsObject:app.appID];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@://", app.urlScheme];
    NSURL *url = [NSURL URLWithString:urlString];
    return [[UIApplication sharedApplication] canOpenURL:url];
}

+ (void)setAppDownloaded:(NSString *)appID {
    NSArray *hasDownloadedAppIDList = FEUserDefaultsValueForKey(kUDKeyADHasDownloadedAppIDList);
    NSMutableArray *newList = nil;
    if (hasDownloadedAppIDList.count > 0) {
        newList = [hasDownloadedAppIDList mutableCopy];
    }
    else {
        newList = [NSMutableArray array];
    }
    
    if ([newList containsObject:appID] == NO) {
        [newList addObject:appID];
        FESetUserDefaults(kUDKeyADHasDownloadedAppIDList, newList);
    }
}

+ (ADTask*)task {
    NSDictionary *rawInfo = JSON[@"task"];
    if (rawInfo) {
        int index = [rawInfo[@"index"] intValue];
        ADApp *app = apps[index];
        if ([self isAppInstalled:app]) {
            return nil;
        }
        
        ADTask *task = [[ADTask alloc] init];
        task.app = app;
        task.title = local(rawInfo[@"title"]);
        task.message = local(rawInfo[@"message"]);
        task.url = local(rawInfo[@"url"]);
        task.cancelTitle = local(rawInfo[@"cancelTitle"]);
        task.confirmTitle = local(rawInfo[@"confirmTitle"]);
        
        return task;
    }
    
    return nil;
}

+ (NSArray*)moreAppsInfoList {
    NSMutableArray *r = [NSMutableArray array];
    
    for (NSDictionary *rawInfo in JSON[@"moreapps"]) {
        
        int index = [rawInfo[@"index"] intValue];
        ADApp *app = apps[index];
        
        ADMoreAppsInfo *info = [[ADMoreAppsInfo alloc] init];
        info.app = app;
        info.screenshotPathList = rawInfo[@"screenshotPathList"];
        
        [r addObject:info];
    }
    
    return r;
}

+ (NSArray*)sheetAdInfoList {
    NSMutableArray *r = [NSMutableArray array];
    
    for (NSDictionary *rawInfo in JSON[@"sheetAds"]) {
        int index = [rawInfo[@"index"] intValue];
        ADApp *app = apps[index];
        if ([self isAppInstalled:app]) {
            continue;
        }
        
        ADSheetInfo *info = [[ADSheetInfo alloc] init];
        info.app = app;
        info.imagePath = local(rawInfo[@"imagePath"]);
        
        [r addObject:info];
    }
    
    return r;
}

+ (NSArray*)alertAdInfoList {
    NSMutableArray *r = [NSMutableArray array];
    
    for (NSDictionary *rawInfo in JSON[@"alertAds"]) {
        int index = [rawInfo[@"index"] intValue];
        ADApp *app = apps[index];
        if ([self isAppInstalled:app]) {
            continue;
        }
        
        ADAlertInfo *info = [[ADAlertInfo alloc] init];
        info.app = app;
        info.title = local(rawInfo[@"title"]);
        info.message = local(rawInfo[@"message"]);
        info.url = local(rawInfo[@"url"]);
        info.cancelTitle = local(rawInfo[@"cancelTitle"]);
        info.confirmTitle = local(rawInfo[@"confirmTitle"]);
        
        [r addObject:info];
    }
    
    return r;
}

+ (NSArray*)imageAdInfoList {
    NSMutableArray *r = [NSMutableArray array];
    
    for (NSDictionary *rawInfo in JSON[@"imageAds"]) {
        int index = [rawInfo[@"index"] intValue];
        ADApp *app = apps[index];
        if ([self isAppInstalled:app]) {
            continue;
        }
        
        ADImageInfo *info = [[ADImageInfo alloc] init];
        info.app = app;
        info.imagePath = local(rawInfo[@"imagePath"]);
        info.url = local(rawInfo[@"url"]);
        
        [r addObject:info];
    }
    
    return r;
}

#pragma mark - Private
+ (void)loadZipDataAtPath:(NSString*)path {
    OZZipFile *zipFile = nil;
    @try {
        zipFile = [[OZZipFile alloc] initWithFileName:path mode:OZZipFileModeUnzip];
    }
    @catch (NSException *exception) {
        return;
    }
    
    NSString *unzipRoot = [[FEUtil sharedInstance] join:[[FEUtil sharedInstance] tmpPath], @"unzip-ad", nil];
    [[NSFileManager defaultManager] removeItemAtPath:unzipRoot error:nil];
    [[NSFileManager defaultManager] createDirectoryAtPath:unzipRoot withIntermediateDirectories:YES attributes:nil error:nil];
    
    [zipFile goToFirstFileInZip];
    do {
        @autoreleasepool {
            OZFileInZipInfo *info = [zipFile getCurrentFileInZipInfo];
            NSString *unzipToPath = [[FEUtil sharedInstance] join:unzipRoot, info.name, nil];
            NSString *dir = [unzipToPath stringByDeletingLastPathComponent];
            [[NSFileManager defaultManager] createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:nil];
            
            OZZipReadStream *stream = [zipFile readCurrentFileInZip];
            NSMutableData *buffer = [NSMutableData dataWithLength:info.length];
            [stream readDataWithBuffer:buffer];
            if (buffer.length > 0) {
                [buffer writeToFile:unzipToPath atomically:YES];
            }
        }
        
    } while ([zipFile goToNextFileInZip]);
    
    NSString *local = kLocalADPath;
    [[NSFileManager defaultManager] removeItemAtPath:local error:nil];
    [[NSFileManager defaultManager] moveItemAtPath:unzipRoot toPath:local error:nil];
    
    [self loadData];
}

+ (void)loadData {
    NSString *infoPath = [NSString stringWithFormat:@"%@/%@", kLocalADPath, @"Info.json"];
    NSString *infoJSON = [NSString stringWithContentsOfFile:infoPath encoding:NSUTF8StringEncoding error:nil];
    infoJSON = [infoJSON stringByReplacingOccurrencesOfString:@"#ROOT#" withString:kLocalADPath];
    JSON = [infoJSON objectFromJSONString];
    
    apps = [NSMutableArray array];
    for (NSDictionary *rawInfo in JSON[@"apps"]) {
        ADApp *app = [[ADApp alloc] init];
        app.name = local(rawInfo[@"name"]);
        
        app.nameWithFree = local(rawInfo[@"nameWithFree"]);
        if (app.nameWithFree == nil) {
            app.nameWithFree = app.name;
        }
        
        app.details = local(rawInfo[@"details"]);
        app.descriptions = local(rawInfo[@"descriptions"]);
        app.price = local(rawInfo[@"price"]);
        app.appID = rawInfo[@"appID"];
        app.rating = rawInfo[@"rating"];
        app.numberOfReviews = rawInfo[@"numberOfReviews"];
        app.urlScheme = rawInfo[@"urlScheme"];
        app.iconPath = rawInfo[@"iconPath"];
        [apps addObject:app];
    }
}

@end

//
//  AlertAdController.m
//  FreeMusic
//
//  Created by Tina on 16/8/7.
//  Copyright © 2016年 Tracy. All rights reserved.
//

#import "AlertAdController.h"

@implementation AlertAdController

- (id)initWithAdInfo:(ADAlertInfo*)adInfo resultHandler:(void (^)(BOOL confirmed))resultHandler {
    NSString *confirmTitle = adInfo.confirmTitle ? adInfo.confirmTitle : @"Download";
    NSString *cancelTitle = adInfo.cancelTitle ? adInfo.cancelTitle : @"Cancel";
    
    self = [self initWithTitle:adInfo.title message:adInfo.message buttonTitles:@[confirmTitle] cancelButtonTitle:cancelTitle destructiveButtonTitle:nil preferredStyle:UIAlertControllerStyleAlert handler:^(FEAlertController *c, NSString *buttonTitle) {
        
        BOOL confirmed = NO;
        
        if ([buttonTitle isEqualToString:confirmTitle]) {
            NSString *urlString = [adInfo.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSURL *url = [NSURL URLWithString:urlString];
            if (url) {
                [[UIApplication sharedApplication] openURL:url];
            }
            else {
                [[FEUtil sharedInstance] openAppInAppStoreWithAppID:adInfo.app.appID];
            }
            [ADManager setAppDownloaded:adInfo.app.appID];
            
            confirmed = YES;
        }
        
        if (resultHandler) {
            resultHandler(confirmed);
        }
    }];
    
    return self;
}

@end

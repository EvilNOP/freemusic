//
//  NoticesManager.h
//  iVault
//
//  Created by Darlene on 14-1-8.
//
//

#import <Foundation/Foundation.h>

#define NoticesVersionDidUpdateNotification @"NoticesVersionDidUpdateNotification"

@interface NoticesManager : NSObject

+ (void)work;

+ (void)updateToVersion:(int)version;
+ (int)currentVersion;

+ (NSURL*)remoteNoticesURL;

+ (BOOL)existUnreadUpdates;
+ (void)hasReadUpdates;

@end

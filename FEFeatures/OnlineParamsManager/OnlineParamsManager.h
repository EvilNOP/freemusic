//
//  OnlineParams.h
//  Coolplayer
//
//  Created by Tracy on 7/8/16.
//  Copyright © 2016 flowdev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FECommon/FECommon.h>
#import "OnlineParam.h"

static NSString * const OnlineParamsManagerDidUpdateParamsNotification = @"OnlineParamsManagerDidUpdateParamsNotification";

typedef enum : NSUInteger {
    OnlineParamsModeDebug,
    OnlineParamsModeRelease
} OnlineParamsMode;

@interface OnlineParamsManager : FESingleton

- (void)registerWithAppKey:(NSString*)key mode:(OnlineParamsMode)mode defaults:(NSDictionary*)defaults; // mode 仅在DEBUG有效，在非DEBUG时，强制设置OnlineParamsModeRelease
- (void)update;

- (BOOL)boolForName:(NSString*)name;
- (NSInteger)integerForName:(NSString*)name;
- (id)valueForName:(NSString*)name;

@end

//
//  OnlineParams.m
//  Coolplayer
//
//  Created by Tracy on 7/8/16.
//  Copyright © 2016 flowdev. All rights reserved.
//

#import "OnlineParamsManager.h"
#import "AFNetworking.h"

static NSString * const kUDKeyOnlineParams = @"OPM.m.1";

@interface OnlineParamsManager ()
@property (nonatomic, strong) NSMutableArray *params;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSDictionary *defaults;
@property (nonatomic) OnlineParamsMode mode;
@end

@implementation OnlineParamsManager

- (void)sharedInstanceInitializer {
    id params = FEUserDefaultsValueForKey(kUDKeyOnlineParams);
    [self parseRawParams:params];
}

- (void)update {
    if (self.url == nil) {
        [NSException raise:@"OnlineParamsManagerError" format:@"先调用 -[OnlineParamsManager registerURL: defaults:] 方法"];
    }
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:self.url parameters:nil error:nil];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURLSessionDataTask *task = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error == nil && responseObject) {
            [self didFetchOnlineParams:responseObject];
        }
    }];
    
    [task resume];
}

#pragma mark - Public
- (void)registerWithAppKey:(NSString*)key mode:(OnlineParamsMode)mode defaults:(NSDictionary *)defaults {
#ifdef DEBUG
    self.mode = mode;
#else
    self.mode = OnlineParamsModeRelease;
#endif
    
    if (self.mode == OnlineParamsModeDebug) {
        self.url = [NSString stringWithFormat:@"http://tracycool.com:5000/p/%@", key];
        NSLog(@"%s 在线参数: %@", __func__, self.url);
    }
    else {
        self.url = [NSString stringWithFormat:@"http://analytics.flowever.net/p/%@", key];
    }
    
    self.defaults = defaults;
}

- (BOOL)boolForName:(NSString*)name {
    return [[self paramForName:name] boolValue];
}

- (NSInteger)integerForName:(NSString*)name {
    return [[self paramForName:name] integerValue];
}

- (id)valueForName:(NSString*)name {
    return [[self paramForName:name] value];
}

#pragma mark - Private
- (void)didFetchOnlineParams:(NSDictionary*)rawParams {
    [self parseRawParams:rawParams];
    
    FESetUserDefaults(kUDKeyOnlineParams, rawParams);
    FEPostNotification(OnlineParamsManagerDidUpdateParamsNotification, nil, nil);
}

- (void)parseRawParams:(NSDictionary*)rawParams {
    self.params = [NSMutableArray array];
    
    for (NSString *name in rawParams) {
        OnlineParam *param = [[OnlineParam alloc] init];
        param.name = name;
        param.rawValue = rawParams[name];
        [self.params addObject:param];
    }
}

- (OnlineParam*)paramForName:(NSString*)name {
    OnlineParam *result = nil;
    
    for (OnlineParam *param in self.params) {
        if ([param.name isEqualToString:name]) {
            result = param;
            break;
        }
    }
    
    if (result == nil && self.defaults[name]) {
        result = [[OnlineParam alloc] init];
        result.name = name;
        result.rawValue = self.defaults[name];
        [self.params addObject:result];
    }
    
    return result;
}

@end

//
//  FFMpegWrapper.m
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "FFmpegWrapper.h"
#include "ffmpeg.h"

static NSString * const kThreadName = @"FFmpegWrapper";

@interface FFmpegWrapper ()

@property (atomic, strong) NSMutableArray *screenshotThreadQueue;
@property (atomic, strong) NSMutableDictionary *screenshotThreadNameToCompletionHandler;

@end

@implementation FFmpegWrapper

+ (instancetype)defaultWrapper {
    static FFmpegWrapper *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[FFmpegWrapper alloc] init];
    });
    return _instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.screenshotThreadQueue = [NSMutableArray array];
        self.screenshotThreadNameToCompletionHandler = [NSMutableDictionary dictionary];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(threadWillExit:) name:NSThreadWillExitNotification object:nil];
    }
    return self;
}

#pragma mark - Notification
- (void)threadWillExit:(NSNotification *)notification {
    NSThread *thread = notification.object;
    NSString *threadName = thread.name;
    
    if (thread == self.screenshotThreadQueue.firstObject) {
        // 回调
        FFmpegWrapperCreateScreenshotCompletionHandler handler = self.screenshotThreadNameToCompletionHandler[threadName];
        if (handler) {
            NSString *screenshotPath = [self screenshotPathForThreadName:threadName];
            [kUtil performBlockInMainThread:^{
                handler(screenshotPath);
            }];
            
            [self.screenshotThreadNameToCompletionHandler removeObjectForKey:threadName];
        }
        
        // 处理队列
        [self.screenshotThreadQueue removeObjectAtIndex:0];
        if (self.screenshotThreadQueue.count > 0) {
            NSThread *thread = self.screenshotThreadQueue.firstObject;
            [thread start];
        }
    }
}

#pragma mark - Public
/**
 *  解析字幕并保存到Library下
 *  转换规则: Documents/folder/test.mkv -> Library/Caches/folder#test.mkv_sub0.srt
 *
 *  @param videoPath 视频文件地址
 */
- (void)extractSubtitleFromVideo:(NSString *)videoPath {
    NSString *videoName = [videoPath lastPathComponent];
    NSString *folder = [[videoPath stringByDeletingLastPathComponent] lastPathComponent];
    NSString *prefix = [NSString stringWithFormat:@"%@#%@_sub", folder, videoName];
    
    NSString *subtitleName = [NSString stringWithFormat:@"%@%d.srt", prefix, 0];
    NSString *subtitlePath = [kUtil.libraryPath stringByAppendingPathComponent:subtitleName];

//    NSString *command = [NSString stringWithFormat:@"ffmpeg -y -i \"%@\" -map 0:s:%d \"%@\"", videoPath, 0, subtitlePath];
    
    videoPath = [NSString stringWithFormat:@"%@", videoPath];
    subtitlePath = [NSString stringWithFormat:@"%@", subtitlePath];
    
    NSArray *commands = @[@"ffmpeg", @"-y", @"-i", videoPath, @"-map", @"0:s:0", subtitlePath];
    
    NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(performCommand:) object:commands];
    thread.name = kThreadName;
    [thread start];
}

- (void)createScreenshotOfVideoAtPath:(NSString*)path atTime:(NSTimeInterval)time size:(CGSize)size completion:(FFmpegWrapperCreateScreenshotCompletionHandler)completion {
    NSString *threadName = [NSString stringWithFormat:@"%d", arc4random()];
    NSString *screenshotPath = [self screenshotPathForThreadName:threadName];
    NSString *scaleOption = [NSString stringWithFormat:@"scale=%ld:%ld", (NSInteger)size.width, (NSInteger)size.height];
//    NSString *command = [NSString stringWithFormat:@"ffmpeg -y -i %@ -ss %.0lf -vframes 1 -vf scale=%@ %@", path, time, scale, screenshotPath];
    
    NSString *timePoint = [NSString stringWithFormat:@"%.0lf", time];
    NSArray *commands = @[@"ffmpeg", @"-y", @"-i", path, @"-ss", timePoint, @"-vframes", @"1", @"-vf", scaleOption,
                          screenshotPath];
    
    NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(performCommand:) object:commands];
    thread.name = threadName;
    
    [self.screenshotThreadQueue addObject:thread];
    self.screenshotThreadNameToCompletionHandler[threadName] = completion;
    
    if (self.screenshotThreadQueue.count == 1) {
        [thread start];
    }
}

- (NSTimeInterval)durationOfVideoAtPath:(NSString *)path {
    AVFormatContext *pFormatCtx;
    
    av_register_all();
    avformat_network_init();
    pFormatCtx = avformat_alloc_context();
    
    if (avformat_open_input(&pFormatCtx, [path UTF8String], NULL, NULL) != 0){
        NSLog(@"FFmpegWrapper::Couldn't open input stream");
        return 0;
    }
    
    if (avformat_find_stream_info(pFormatCtx, NULL) < 0){
        NSLog(@"FFmpegWrapper::Couldn't find stream information");
        return 0;
    }
    
    NSInteger duration = pFormatCtx->duration * 1.0 / AV_TIME_BASE;
    
    avformat_close_input(&pFormatCtx);
    
    return duration;
}

#pragma mark - Private
int ffmpeg_main(int argc, char **argv);

- (void)performFFmpegCommand:(NSString *)command {
    NSLog(@"%@", [NSThread currentThread]);
    NSLog(@"command = %@", command);
    
    NSArray *argvArray = [command componentsSeparatedByString:@" "];
    int argc = (int)argvArray.count;
    
    char** argv = (char**)malloc(sizeof(char*)*argc);
    
    for(int i = 0; i < argc; i++) {
        argv[i] = (char*)malloc(sizeof(char)*1024);
        strcpy(argv[i], [argvArray[i] UTF8String]);
    }
    
    for(int i = 0; i < argc; i++) {
        NSLog(@"%s", argv[i]);
    }
    
    ffmpeg_main(argc, argv);
    
    for(int i = 0; i < argc;i++)
        free(argv[i]);
    free(argv);
}

- (void)performCommand:(NSArray <NSString *> *)commands {
    NSLog(@"%@", [NSThread currentThread]);
    
    int argc = (int)commands.count;
    
    char** argv = (char**)malloc(sizeof(char*)*argc);
    
    for(int i = 0; i < argc; i++) {
        argv[i] = (char*)malloc(sizeof(char)*1024);
        strcpy(argv[i], [commands[i] UTF8String]);
    }
    
    for(int i = 0; i < argc; i++) {
        NSLog(@"!!!%s", argv[i]);
    }
    
    ffmpeg_main(argc, argv);
    
    for(int i = 0; i < argc;i++)
        free(argv[i]);
    free(argv);
}

- (NSString*)screenshotPathForThreadName:(NSString*)threadName {
    NSString *screenshotFilename = [NSString stringWithFormat:@"%@.JPG", threadName];
    return [kUtil join:kUtil.tmpPath, screenshotFilename, nil];
}

@end

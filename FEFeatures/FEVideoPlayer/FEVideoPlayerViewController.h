//
//  VideoViewController.h
//  Coolplayer
//
//  Created by flowdev on 16/4/26.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FEVideoPlayerViewController : UIViewController

@property (nonatomic, copy) NSString *filePath;
@property (nonatomic) NSInteger index;

- (instancetype)initWithItems:(NSMutableArray *)items atIndex:(NSInteger)index;
- (instancetype)initWithFilePath:(NSString *)filePath;

@end

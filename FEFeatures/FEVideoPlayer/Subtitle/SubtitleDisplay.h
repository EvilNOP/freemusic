//
//  SubtitleDisplay.h
//  VideoDemo
//
//  Created by flowdev on 16/4/12.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubtitleDisplay : UIView

@property (nonatomic, copy) NSArray *subtitleArray;
@property (nonatomic, strong) UILabel *subtitleText;

- (instancetype)initWithFrame:(CGRect)frame andFilePath:(NSString *)path;
- (void)changeFilePath:(NSString *)filePath;
- (void)displayByTime:(double)time;

@end

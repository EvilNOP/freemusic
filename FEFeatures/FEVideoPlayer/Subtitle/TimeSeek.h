//
//  TimeSeek.h
//  VideoDemo
//
//  Created by flowdev on 16/4/14.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeSeek : UIView

- (void)setDiffText:(double)diff;
- (void)setTimeText:(double)time duration:(double)duration;

@end

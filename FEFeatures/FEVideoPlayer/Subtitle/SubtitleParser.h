//
//  SubtitleParser.h
//  VideoDemo
//
//  Created by flowdev on 16/4/11.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubtitleParser : NSObject

+ (NSArray *)parseWithFilePath:(NSString *)path;

@end

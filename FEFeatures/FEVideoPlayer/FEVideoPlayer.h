//
//  FEVideoPlayer.h
//  Photorange
//
//  Created by Tracy on 7/19/16.
//
//

#import "FFmpegWrapper.h"
#import "VideoInfo.h"
#import "FEVideoPlayerViewController.h"

#define FEVideoPlayerPrimaryColor [[UIColor blackColor] colorWithAlphaComponent:0.6]
#define FEVideoPlayerProgressSliderTintColor FEHexRGBA(0xffffff, 1.0)
#define FEVideoPlayerProgressSliderTrackTintColor FEHexRGBA(0x282828, 1.0)
#define FEVideoPlayerSettingSelectedColor FEHexRGBA(0x0d0d0d, 1.0)
#define FEVideoPlayerSettingDiselectedColor FEHexRGBA(0x999999, 1.0)
#define FEVideoPlayerSpeedSliderTintColor [UIColor whiteColor]
#define FEVideoPlayerSpeedSliderMaximumTrackTintColor [UIColor whiteColor]
#define FEVideoPlayerSpeedSliderThumbnailColor [UIColor whiteColor]
#define FEVideoPlayerSppedSliderThumbnailTextColor [UIColor blackColor]
#define FEVideoPlayerUnlockBackgroundColor FEHexRGBA(0x01dbe7, 1.0)

static CGFloat const FEVideoPlayerFirstFontSize = 18.0;
static CGFloat const FEVideoPlayerSecondFontSize = 14.0;
static CGFloat const FEVideoPlayerThirdFontSize = 12.0;

static NSString * const FEVideoPlayerVideoButtonDeselectedNotification = @"FEVideoPlayerVideoButtonDeselectedNotification";

static NSString * const kUDKeyVideoScale = @"FEVideoPlayer.h.1";
static NSString * const kUDKeyVideoLandscape = @"FEVideoPlayer.h.2";
static NSString * const kUDKeyVideoAutoPlayNext = @"FEVideoPlayer.h.3";
static NSString * const kUDKeySubtitleFontSize = @"FEVideoPlayer.h.4";
static NSString * const kUDKeySubtitleFontFamily = @"FEVideoPlayer.h.5";
static NSString * const kUDKeySubtitleFontColor = @"FEVideoPlayer.h.6";
static NSString * const kUDKeyVideoPauseWhenEnterBackground = @"FEVideoPlayer.h.7";

//
//  VideoRepeatBar.m
//  Coolplayer
//
//  Created by flowdev on 16/5/17.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoRepeatBar.h"
#import <Masonry.h>

@interface VideoRepeatBar ()

@property (nonatomic, copy) void (^startAction)();
@property (nonatomic, copy) void (^endAction)();

@end

@implementation VideoRepeatBar

- (instancetype)initWithStartAction:(void (^)())aStartAction endAction:(void (^)())aEndAction {
    self = [super init];
    if (self) {
        self.startAction = aStartAction;
        self.endAction = aEndAction;
        
        self.start = [[UIButton alloc] init];
        [self.start setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.start setBackgroundImage:[UIImage imageNamed:@"RepeatStart"] forState:UIControlStateNormal];
        [self.start addTarget:self action:@selector(actionStart) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.start];
        
        self.end = [[UIButton alloc] init];
        [self.end setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.end setBackgroundImage:[UIImage imageNamed:@"RepeatEnd"] forState:UIControlStateNormal];
        [self.end addTarget:self action:@selector(actionEnd) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.end];
        
        [self setupConstraints];
    }
    return self;
}

- (void)actionStart {
    self.start.selected = !self.start.selected;
    if (self.startAction) {
        self.startAction();
    }
}

- (void)actionEnd {
    self.end.selected = !self.end.selected;
    if (self.endAction) {
        self.endAction();
    }
}

- (void)setStartTitleWithTime:(double)time {
    int floorTime = floor(time);
    int hour = floorTime / 3600;
    int minute = floorTime / 60 % 60;
    int second = floorTime % 60;

    NSString *title = [NSString stringWithFormat:@"A %02d:%02d:%02d", hour, minute, second];
    [self.start setTitle:title forState:UIControlStateNormal];
}

- (void)setEndTitleWithTime:(double)time {
    int floorTime = floor(time);
    int hour = floorTime / 3600;
    int minute = floorTime / 60 % 60;
    int second = floorTime % 60;
    
    NSString *title = [NSString stringWithFormat:@"B %02d:%02d:%02d", hour, minute, second];
    [self.end setTitle:title forState:UIControlStateNormal];
}

- (void)setupConstraints {
    [self.start mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self);
        make.width.mas_equalTo (125);
    }];
    
    [self.end mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(self);
        make.width.mas_equalTo (125);
    }];
}

@end

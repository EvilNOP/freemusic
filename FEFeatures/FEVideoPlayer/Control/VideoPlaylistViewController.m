//
//  VideoPlaylistViewController.m
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoPlaylistViewController.h"
#import "FEVideoPlayer.h"

@interface VideoPlaylistViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *list;
@property (nonatomic, strong) void (^selectedAction)(NSInteger);

@end

@implementation VideoPlaylistViewController

- (instancetype)initWithlist:(NSArray *)list selectedAction:(void (^)(NSInteger))block {
    self = [super init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.list = list;
    self.selectedAction = block;
    
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.index inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    NSString *path = self.list[indexPath.row];

    cell.textLabel.text = path.lastPathComponent;

    [cell.textLabel setFont:[UIFont systemFontOfSize:FEVideoPlayerSecondFontSize]];
    [cell.textLabel setLineBreakMode:NSLineBreakByTruncatingMiddle];

    if (self.index == indexPath.row) {
        cell.textLabel.textColor = FEVideoPlayerSettingSelectedColor;
    } else {
        cell.textLabel.textColor = FEVideoPlayerSettingDiselectedColor;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.index = indexPath.row;
        
    [self.tableView reloadData];
    
    self.selectedAction(indexPath.row);
    [[NSNotificationCenter defaultCenter] postNotificationName:FEVideoPlayerVideoButtonDeselectedNotification object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 47;
}

@end

//
//  VideoRightSideBar.m
//  Coolplayer
//
//  Created by flowdev on 16/5/16.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoRightSideBar.h"
#import <Masonry.h>

static const CGFloat kButtonWidth = 44;
static const CGFloat kButtonHeight = 44;

@interface VideoRightSideBar ()

@property (nonatomic, copy) void (^speedAction)();
@property (nonatomic, copy) void (^aspectAction)();
@property (nonatomic, copy) void (^listAction)();

@end

@implementation VideoRightSideBar

- (instancetype)initWithSpeedAction:(void (^)())aSpeedAction
                       aspectAction:(void (^)())aAspectAction
                         listAction:(void (^)())aListAction {
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.speedAction = aSpeedAction;
        self.aspectAction = aAspectAction;
        self.listAction = aListAction;
        
        self.speed = [[UIButton alloc] init];
        [self.speed setImage:[UIImage imageNamed:@"Speed"] forState:UIControlStateNormal];
        [self.speed setImage:[UIImage imageNamed:@"Speed2"] forState:UIControlStateSelected];
        [self.speed setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.speed addTarget:self action:@selector(actionSpeed) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.speed];
        
        self.aspect = [[UIButton alloc] init];
        [self.aspect setImage:[UIImage imageNamed:@"Screen"] forState:UIControlStateNormal];
        [self.aspect setImage:[UIImage imageNamed:@"Screen2"] forState:UIControlStateSelected];
        [self.aspect setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.aspect addTarget:self action:@selector(actionAspect) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.aspect];
        
        self.list = [[UIButton alloc] init];
        [self.list setImage:[UIImage imageNamed:@"List"] forState:UIControlStateNormal];
        [self.list setImage:[UIImage imageNamed:@"List2"] forState:UIControlStateSelected];
        [self.list setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.list addTarget:self action:@selector(actionList) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.list];
        
        [self setupConstraints];
    }
    return self;
}

- (void)actionSpeed {
    if (self.speedAction) {
        self.speed.selected = !self.speed.selected;
        self.speedAction();
    }
}

- (void)actionAspect {
    if (self.aspectAction) {
        self.aspect.selected = !self.aspect.selected;
        self.aspectAction();
    }
}

- (void)actionList {
    if (self.listAction) {
        self.list.selected = !self.list.selected;
        self.listAction();
    }
}

- (void)setupConstraints {
    [self.speed mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kButtonWidth, kButtonHeight));
        make.right.equalTo(self).offset(-10);
        make.top.equalTo(self);
    }];
    
    [self.aspect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kButtonWidth, kButtonHeight));
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-10);
    }];
    
    [self.list mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(kButtonWidth, kButtonHeight));
        make.bottom.equalTo(self);
        make.right.equalTo(self).offset(-10);
    }];

}

@end

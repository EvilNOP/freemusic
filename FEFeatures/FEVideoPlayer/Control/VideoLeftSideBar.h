//
//  VideoLeftSideBar.h
//  Coolplayer
//
//  Created by flowdev on 16/5/16.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoLeftSideBar : UIView


@property (nonatomic, strong) UIButton *subtitle;
@property (nonatomic, strong) UIButton *repeat;

- (instancetype)initWithLockRotateAction:(void (^)())aLockRotateAction
                            repeatAction:(void (^)())aRepeatAction
                          subtitleAction:(void (^)())aSubtitleAction;

@end

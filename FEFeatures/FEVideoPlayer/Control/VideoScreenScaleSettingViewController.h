//
//  VideoScreenScaleSettingViewController.h
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoScreenScaleSettingViewController : UIViewController

@property (nonatomic) NSInteger index;
- (instancetype)initWithAspectChangedBlock:(void (^)(CGFloat, BOOL))block;

@end

//
//  VideoNavigationBarView.m
//  VideoDemo
//
//  Created by flowdev on 16/4/14.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import "VideoNavigationBarView.h"
#import <Masonry.h>
#import "FEVideoPlayer.h"

static const CGFloat kPaddingTop = 24;

@interface VideoNavigationBarView ()

typedef void(^clickAction)();

@property (nonatomic, copy) void (^dismissAction)();

@property (nonatomic, strong) UILabel *currentTime;
@property (nonatomic, strong) UILabel *totalTime;

@end

@implementation VideoNavigationBarView

- (instancetype)initWithDismissAction:(void (^)())dismissAction {
    self = [super init];
    if (self) {
        self.dismissAction = dismissAction;
        self.backgroundColor = FEVideoPlayerPrimaryColor;
        
        // 返回按钮
        self.dismiss = [[UIButton alloc] init];
        [self.dismiss setImage:[UIImage imageNamed:@"Off"] forState:UIControlStateNormal];
        [self.dismiss addTarget:self action:@selector(actionDismiss) forControlEvents:UIControlEventTouchUpInside];
        [self.dismiss.titleLabel setFont:[UIFont boldSystemFontOfSize:16]];
        [self.dismiss setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:self.dismiss];
        
        // 当前播放时间
        self.currentTime = [[UILabel alloc] init];
        self.currentTime.textAlignment = NSTextAlignmentCenter;
        self.currentTime.font = [UIFont systemFontOfSize:14];
        self.currentTime.text = @"--:--";
        self.currentTime.textColor = [UIColor whiteColor];
        self.currentTime.adjustsFontSizeToFitWidth = YES;
        self.currentTime.minimumScaleFactor = 0.8;
        [self addSubview:self.currentTime];
        
        // 总播放时间
        self.totalTime = [[UILabel alloc] init];
        self.totalTime.textAlignment = NSTextAlignmentCenter;
        self.totalTime.font = [UIFont systemFontOfSize:14];
        self.totalTime.text = @"--:--";
        self.totalTime.textColor = [UIColor whiteColor];
        self.totalTime.adjustsFontSizeToFitWidth = YES;
        self.totalTime.minimumScaleFactor = 0.8;
        [self addSubview:self.totalTime];
        
        // 进度条
        self.progressSlider = [[UISlider alloc] init];
        self.progressSlider.tintColor = FEVideoPlayerProgressSliderTintColor;
        self.progressSlider.maximumTrackTintColor = FEVideoPlayerProgressSliderTrackTintColor;
        self.progressSlider.value = 0;
        self.progressSlider.continuous = YES;
        [self.progressSlider setThumbImage:[UIImage imageNamed:@"sliderThumbnail"] forState:UIControlStateNormal];
        
        [self.progressSlider setMaximumValue:100];
        [self.progressSlider setMinimumValue:0];
        [self addSubview:self.progressSlider];
        
        [self setupConstraints];
    }
    return self;
}

- (void)setupConstraints {
    [self.dismiss mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self).offset(kPaddingTop);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [self.currentTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.dismiss.mas_right).offset(15);
        make.centerY.equalTo(self.dismiss);
    }];

    [self.totalTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-15);
        make.centerY.equalTo(self.dismiss);
    }];

    [self.progressSlider mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.currentTime.mas_right).offset(10);
        make.right.equalTo(self.totalTime.mas_left).offset(-10);
        make.height.mas_equalTo(30);
        make.centerY.equalTo(self.dismiss);
    }];
}

- (void)actionDismiss {
    if (self.dismissAction) {
        self.dismissAction();
    }
}

- (void)setTimeDisplayWithTime:(double)time duration:(double)duration {
    NSLog(@"time: %f, duration: %f", time, duration);
    int floorTime = floor(time);
    int floorDuration = floor(duration);
    
    int totalHour = floorDuration / 3600;
    int totalMinute = floorDuration / 60 % 60;
    int totalSecond = floorDuration % 60;
    
    int currentHour = floorTime / 3600;
    int currentMinute = floorTime / 60 % 60;
    int currentSecond = floorTime % 60;
    
    if (totalHour > 0) {
        self.currentTime.text = [NSString stringWithFormat:@"%2d:%02d:%02d", currentHour, currentMinute, currentSecond];
        self.totalTime.text = [NSString stringWithFormat:@"%2d:%02d:%02d", totalHour, totalMinute, totalSecond];
    } else {
        self.currentTime.text = [NSString stringWithFormat:@"%02d:%02d", currentMinute, currentSecond];
        self.totalTime.text = [NSString stringWithFormat:@"%02d:%02d", totalMinute, totalSecond];
    }
}

@end
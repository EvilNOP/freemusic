//
//  ListView.m
//  Coolplayer
//
//  Created by flowdev on 16/7/1.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoSubtitleSettingViewController.h"
#import "VideoSubtitleSettingChoiceViewController.h"
#import "FEVideoPlayer.h"

@interface VideoSubtitleSettingViewController()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation VideoSubtitleSettingViewController

- (instancetype)init {
    self = [super init];
    
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
}

- (void)viewWillLayoutSubviews {
    self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (indexPath.row == 0) {
        cell.textLabel.text = _(@"字体");
    } else if (indexPath.row == 1) {
        cell.textLabel.text = _(@"字体大小");
    } else {
        cell.textLabel.text = _(@"字体颜色");
    }
    
    [cell.textLabel setFont:[UIFont systemFontOfSize:FEVideoPlayerSecondFontSize]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row) {
        case 0: {
            VideoSubtitleSettingChoiceViewController *videoSubtitleSettingChoiceViewController = [[VideoSubtitleSettingChoiceViewController alloc] initWithSettingType: SubtitleSettingTypeFontFamily];
            [self.navigationController pushViewController:videoSubtitleSettingChoiceViewController animated:YES];
        }
            break;
        case 1: {
            VideoSubtitleSettingChoiceViewController *videoSubtitleSettingChoiceViewController = [[VideoSubtitleSettingChoiceViewController alloc] initWithSettingType: SubtitleSettingTypeFontSize];
            [self.navigationController pushViewController:videoSubtitleSettingChoiceViewController animated:YES];
        }
            break;
        case 2:{
            VideoSubtitleSettingChoiceViewController *videoSubtitleSettingChoiceViewController = [[VideoSubtitleSettingChoiceViewController alloc] initWithSettingType: SubtitleSettingTypeFontColor];
            [self.navigationController pushViewController:videoSubtitleSettingChoiceViewController animated:YES];
        }
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 47;
}

@end

//
//  VideoSubtitleControl.m
//  Coolplayer
//
//  Created by flowdev on 16/5/17.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoSubtitleControl.h"
#import <Masonry.h>

#define UIColorFromRGB(rgbValue, alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]

static const NSInteger kBackgroundColor = 0x3A3A3A;

@interface VideoSubtitleControl () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UILabel *viewTitle;
@property (nonatomic, strong) NSArray *optionTitle;
@property (nonatomic, copy) void (^cellAction)(NSInteger cellIndex);

@end

@implementation VideoSubtitleControl

- (instancetype)initWithCellAction:(void (^)(NSInteger cellIndex))aCellAction {
    self = [super init];
    if (self) {
        self.cellAction = aCellAction;
        self.optionTitle = @[_(@"字体"), _(@"字体大小"), _(@"字体颜色")];
        
        self.backgroundColor = UIColorFromRGB(kBackgroundColor, 1);
        self.viewTitle = [[UILabel alloc] init];
        self.viewTitle.textColor = [UIColor whiteColor];
        self.viewTitle.textAlignment = NSTextAlignmentCenter;
        self.viewTitle.text = @"字幕设置";
        [self addSubview:self.viewTitle];
        
        [self.viewTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
            make.height.mas_equalTo(30);
        }];

        self.tableView = [[UITableView alloc] init];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.tableFooterView = [[UIView alloc] init];
        self.tableView.scrollEnabled = NO;
        [self addSubview:self.tableView];
        
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self);
            make.top.equalTo(self.viewTitle.mas_bottom).offset(2);
        }];
    }
    return self;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"optionCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"optionCell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.text = self.optionTitle[indexPath.row];
    
    switch (indexPath.row) {
        case 1:
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", self.fontSize];
            break;
        case 2:
            cell.detailTextLabel.text = @"颜色";
            cell.detailTextLabel.textColor = self.fontColor;
            
        default:
            break;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.cellAction) {
        self.cellAction(indexPath.row);
    }
}

@end

//
//  VideoScreenScaleSettingViewController.m
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoScreenScaleSettingViewController.h"
#import "VideoSettingTableViewCell.h"
#import "FEVideoPlayer.h"

@interface VideoScreenScaleSettingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic, strong) void (^aspectChanged)(CGFloat, BOOL);

@end

@implementation VideoScreenScaleSettingViewController

- (instancetype)initWithAspectChangedBlock:(void (^)(CGFloat, BOOL))block {
    self = [super init];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.data = @[_(@"默认"), @"1:1", @"3:2", @"4:3", @"5:3", @"16:9"];
    self.aspectChanged = block;
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView registerClass:[VideoSettingTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if (FEUserDefaultsValueForKey(kUDKeyVideoScale) != nil) {
        self.index = [self.data indexOfObject:FEUserDefaultsValueForKey(kUDKeyVideoScale)];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: YES];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.index inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)viewWillLayoutSubviews {
    self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoSettingTableViewCell *cell = (VideoSettingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[VideoSettingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    cell.label.text = self.data[indexPath.row];

    [cell.label setFont:[UIFont systemFontOfSize:FEVideoPlayerSecondFontSize]];
    
    if (self.index == indexPath.row) {
        cell.label.textColor = FEVideoPlayerSettingSelectedColor;
    } else {
        cell.label.textColor = FEVideoPlayerSettingDiselectedColor;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.index = indexPath.row;
    [self.tableView reloadData];
    

    CGFloat aspect = 0.0;
    if (indexPath.row != 0) {
        NSArray *scales = [self.data[indexPath.row] componentsSeparatedByString:@":"];
         aspect = [scales[0] floatValue] / [scales[1] floatValue];
    }
    FESetUserDefaults(kUDKeyVideoScale, self.data[indexPath.row]);
    
    if (indexPath.row == 0) {
        self.aspectChanged(aspect, YES);
    } else {
        self.aspectChanged(aspect, NO);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FEVideoPlayerVideoButtonDeselectedNotification object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 47;
}

@end

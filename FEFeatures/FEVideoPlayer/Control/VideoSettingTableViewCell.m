//
//  VideoSettingTableViewCell.m
//  Coolplayer
//
//  Created by flowdev on 16/7/4.
//  Copyright © 2016年 flowdev. All rights reserved.
//

#import "VideoSettingTableViewCell.h"
#import "Masonry.h"

@implementation VideoSettingTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    self.label = [[UILabel alloc] init];
    [self.contentView addSubview:self.label];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.contentView);
    }];
}

@end

//
//  VideoControlPanel.m
//  VideoDemo
//
//  Created by flowdev on 16/4/15.
//  Copyright © 2016年 罗思成. All rights reserved.
//

#import "VideoControlPanel.h"
#import "FEVideoPlayer.h"
#import <Masonry.h>

@interface VideoControlPanel ()

@property (nonatomic, strong) UILabel *timeDisplay;
@property (nonatomic, strong) UIButton *playOrPause;
@property (nonatomic, strong) UIButton *lock;
@property (nonatomic, strong) UIButton *changeAspect;
@property (nonatomic, strong) UIButton *rewind;
@property (nonatomic, strong) UIButton *forward;

@property (nonatomic, copy) void (^playAction)();
@property (nonatomic, copy) void (^pauseAction)();
@property (nonatomic, copy) void (^lockAction)();
@property (nonatomic, copy) void (^rewindAction)();
@property (nonatomic, copy) void (^forwardAction)();


@property (nonatomic) VideoPlayState currentState;

@end

@implementation VideoControlPanel

- (instancetype)initWithPlayAction:(void (^)())aPlayAction
                       pauseAction:(void (^)())aPauseAction
                        lockAction:(void (^)())aLockAction
                      rewindAction:(void (^)())aRewindAction
                     forwardAction:(void (^)())aForwardAction{
    
    self = [super init];
    
    if (self) {
        self.backgroundColor = FEVideoPlayerPrimaryColor;
        
        self.playAction = aPlayAction;
        self.pauseAction = aPauseAction;
        self.lockAction = aLockAction;
        self.rewindAction = aRewindAction;
        self.forwardAction = aForwardAction;
        self.currentState = VideoPlayStatePlaying;
        
        // 播放暂停按钮
        self.playOrPause = [[UIButton alloc] init];
        [self.playOrPause setImage:[UIImage imageNamed:@"Pause"] forState:UIControlStateNormal];
        [self.playOrPause addTarget:self action:@selector(actionPlayOrPause) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.playOrPause];
        
        // 锁定按钮
        self.lock = [[UIButton alloc] init];
        [self.lock setImage:[UIImage imageNamed:@"Lock2"] forState:UIControlStateNormal];
        [self.lock addTarget:self action:@selector(actionLock) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.lock];
        
        // 上一个视频
        self.rewind = [[UIButton alloc] init];
        [self.rewind setImage:[UIImage imageNamed:@"Last_Video"] forState:UIControlStateNormal];
        [self.rewind addTarget:self action:@selector(actionRewind) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.rewind];
        
        // 下一个视频
        self.forward = [[UIButton alloc] init];
        [self.forward setImage:[UIImage imageNamed:@"Next_Video"] forState:UIControlStateNormal];
        [self.forward addTarget:self action:@selector(actionForward) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.forward];
        
        [self setupConstraints];
    }
    
    return self;
}

- (void)setupConstraints {
    [self.playOrPause mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(44);
        make.center.equalTo(self);
    }];
    
    [self.lock mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.left.equalTo(self).offset(15);
        make.centerY.equalTo(self);
    }];
    
    [self.rewind mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(44);
        make.right.equalTo(self.playOrPause.mas_left).offset(-35);
        make.centerY.equalTo(self);
    }];
    
    [self.forward mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(40);
        make.left.equalTo(self.playOrPause.mas_right).offset(35);
        make.centerY.equalTo(self);
    }];
}

- (void)actionPlayOrPause {
    if (self.currentState == VideoPlayStateStopped) {
        if (self.playAction) {
            self.playAction();
        }
        [self changePlayButton:VideoPlayStatePlaying];
    } else if (self.currentState == VideoPlayStatePlaying) {
        if (self.pauseAction) {
            self.pauseAction();
        }
        [self changePlayButton:VideoPlayStateStopped];
    }
}

- (void)actionLock {
    if (self.lockAction) {
        self.lockAction();
    }
}

- (void)changePlayButton:(VideoPlayState)state {
    self.currentState = state;
    switch (self.currentState) {
        case VideoPlayStatePlaying:
            [self.playOrPause setImage:[UIImage imageNamed:@"Pause"] forState:UIControlStateNormal];
            break;
            
        case VideoPlayStateStopped:
            [self.playOrPause setImage:[UIImage imageNamed:@"Stop"] forState:UIControlStateNormal];
            break;
    }
}

- (void)actionRewind {
    if (self.rewindAction) {
        self.rewindAction();
    }
}

- (void)actionForward {
    if (self.forwardAction) {
        self.forwardAction();
    }
}

@end

//
//  PlayerUserInfo.m
//  Drive
//
//  Created by EvilNOP on 15/4/15.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "PlayerUserInfo.h"

@implementation PlayerUserInfo
+ (id)userInfoWithDatasourceType:(PlayerDatasourceType)datasourceType object:(id)object
{
    PlayerUserInfo *userInfo = [[PlayerUserInfo alloc] init];
    userInfo.datasourceType = datasourceType;
    userInfo.object = object;
    return userInfo;
}
@end
 

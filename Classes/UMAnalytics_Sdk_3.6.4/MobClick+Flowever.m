//
//  MobClick+Flowever.m
//  Soso
//
//  Created by EvilNOP on 15/5/30.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "MobClick+Flowever.h"
#import <objc/runtime.h>

static NSDictionary *defaults = nil;

@implementation MobClick (Flowever)

+ (void)load
{
    Method m1 = class_getClassMethod(self, @selector(getConfigParams:));
    Method m2 = class_getClassMethod(self, @selector(customGetConfigParams:));
    method_exchangeImplementations(m1, m2);
}

+ (NSString*)customGetConfigParams:(NSString*)key
{
    NSString *value = [self customGetConfigParams:key]; //call original method +[Moblick getConfigParams:]
    if (value == nil) {
        value = defaults[key];
    }
    return value;
}

#pragma mark - Public
+ (void)registerDefaults:(NSDictionary*)dict
{
    defaults = [dict copy];
}

@end

//
//  MobClick+Flowever.h
//  Soso
//
//  Created by EvilNOP on 15/5/30.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "MobClick.h"

@interface MobClick (Flowever)

+ (void)registerDefaults:(NSDictionary*)dict;

@end

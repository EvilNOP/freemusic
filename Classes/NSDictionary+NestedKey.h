//
//  NSDictionary+NestedKey.h
//  Drive
//
//  Created by EvilNOP on 15/3/10.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NestedKey)

- (id)valueForNestedKey:(NSString*)nestedKey;

@end

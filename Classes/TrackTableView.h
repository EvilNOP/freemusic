//
//  TrackTableView.h
//  Drive
//
//  Created by EvilNOP on 15/3/10.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackTableView : UITableView

@property (nonatomic, readwrite) BOOL hidesLoadMoreIndicator;

@end

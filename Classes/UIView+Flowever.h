//
//  UIView+IgnoreSubviewEvent.h
//  OnePass
//
//  Created by EvilNOP on 13-9-20.
//  Copyright (c) 2013年 Elite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Flowever)

- (void)setIgnoreSubviewEvent:(BOOL)ignoreSubviewEvent;
- (BOOL)ignoreSubviewEvent;
- (void)setIncludeOutsideSubviewEvent:(BOOL)includeOutsideSubviewEvent;
- (BOOL)includeOutsideSubviewEvent;

@end

//
//  OneDriveSource.swift
//  FreeMusic
//
//  Created by EvilNOP on 16/8/11.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

import Foundation
import OneDriveSDK
import Alamofire

@objc class OneDriveSource: NSObject {
    static let sharedInstance = OneDriveSource()
    
    var rootViewController: UIViewController?
    
    typealias FetchStreamRequestCompletionHandler = (request: NSURLRequest) -> Void
    
    func isAuthorized() -> Bool {
        return ODClient.loadCurrentClient() != nil
    }
    
    func authorizeWithCompletionHandler(completionHandler: (authorized: Bool) -> Void) {
        ODClient.clientWithCompletion { (currentClient, error) in
            completionHandler(authorized: currentClient != nil)
        }
    }
    
    func fetchStreamRequestForTrack(track: Track, completionHandler: FetchStreamRequestCompletionHandler) {
        if let urlString = track.contentURL {
            if let url = NSURL(string: urlString) {
                let request = NSURLRequest(URL: url)
                completionHandler(request: request)
            }
        }
    }
    
    func createTrackWithItem(item: ODItem) -> Track {
        let track = Track()
        track.remoteID = item.id
        track.title = item.name
        track.contentURL = item.dictionaryFromItem()["@content.downloadUrl"] as? String
        track.source = TrackSourceOneDrive
        return track
    }
}

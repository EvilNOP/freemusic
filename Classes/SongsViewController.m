//
//  SongsViewController.m
//  Drive
//
//  Created by EvilNOP on 15/3/11.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "SongsViewController.h"
#import "PlayListViewController.h"
#import "FFCircularProgressView.h"
#import "ProgressButton.h"

enum {
    kSectionTagTool,
    kSectionTagTracks
};

#define isRegularType (_type == SongsViewControllerTypeRegular)
#define isPlaylistDetailsType (_type == SongsViewControllerTypePlaylistDetails)
#define isSelectionType (_type == SongsViewControllerTypeSelection)

@interface SongsViewController () <UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate> {
    NSMutableArray *_tracks;
    NSMutableArray *_sectionInfoList;
    
    UIBarButtonItem *_playerBarButtonItem;
    UIBarButtonItem *_endEditingBarButtonItem;
    
    __weak IBOutlet TrackTableView *_tableView;
    __weak IBOutlet UIButton *_editButton;
    IBOutlet TrackCell *_toolCell;
    
    __weak IBOutlet UIView *_bottomToolViewForRegularType;
    __weak IBOutlet UIView *_bottomToolViewForPlaylistDetailsType;
    
    __weak IBOutlet UISwitch *_offlineSwitch;
    __weak IBOutlet UILabel *_offlineLabel;
    __weak IBOutlet UIImageView *_shuffleImageView;
    
    __weak IBOutlet UIButton *_addToPlaylistButton;
    __weak IBOutlet UIButton *_deleteButton4RegularType;
    __weak IBOutlet UIButton *_addSongsButton;
    __weak IBOutlet UIButton *_deleteButton4PlaylistDetails;
    
    __weak IBOutlet GADBannerView *_bannerView;
}

@end

@implementation SongsViewController

- (id)initWithType:(SongsViewControllerType)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    _bottomToolViewForRegularType.backgroundColor = colorForKey(@"toolViewColor");
    _bottomToolViewForPlaylistDetailsType.backgroundColor = _bottomToolViewForRegularType.backgroundColor;
    
    UIColor *hightlightedColor = FEHexRGBA(0x9e9e9e, 1.0);
    
    if (isRegularType) {
        [kGlobal navigationItem:self.navigationItem customTitle:@"SONGS"];
        _endEditingBarButtonItem = [self createEndEditingBarButtonItem];
        _playerBarButtonItem = [kGlobal createPlayerBarButtonItem];
        self.navigationItem.rightBarButtonItem = _playerBarButtonItem;
        
        [_deleteButton4RegularType setTitleColor:hightlightedColor forState:UIControlStateHighlighted];
        UIImage *icon = [kUtil tintImage:_deleteButton4RegularType.imageView.image withColor:hightlightedColor];
        [_deleteButton4RegularType setImage:icon forState:UIControlStateHighlighted];
        
        [_addToPlaylistButton setTitleColor:hightlightedColor forState:UIControlStateHighlighted];
        icon = [kUtil tintImage:_addSongsButton.imageView.image withColor:hightlightedColor];
        [_addToPlaylistButton setImage:icon forState:UIControlStateHighlighted];
    }
    else if (isPlaylistDetailsType) {
        self.navigationItem.title = _playlist.name;
        _endEditingBarButtonItem = [self createEndEditingBarButtonItem];
        _playerBarButtonItem = [kGlobal createPlayerBarButtonItem];
        self.navigationItem.rightBarButtonItem = _playerBarButtonItem;
        
        [_deleteButton4PlaylistDetails setTitleColor:hightlightedColor forState:UIControlStateHighlighted];
        UIImage *icon = [kUtil tintImage:_deleteButton4PlaylistDetails.imageView.image withColor:hightlightedColor];
        [_deleteButton4PlaylistDetails setImage:icon forState:UIControlStateHighlighted];
        
        [_addSongsButton setTitleColor:hightlightedColor forState:UIControlStateHighlighted];
        icon = [kUtil tintImage:_addSongsButton.imageView.image withColor:hightlightedColor];
        [_addSongsButton setImage:icon forState:UIControlStateHighlighted];
    }
    else if (isSelectionType) {
        [kGlobal navigationItem:self.navigationItem customTitle:@"Add songs"];
        self.navigationItem.leftBarButtonItem = [self barButtonItem:@"CANCEL" selector:@selector(cancelButtonItemHandler:)];
        self.navigationItem.rightBarButtonItem = [self barButtonItem:@"DONE" selector:@selector(finishSelectingTracksButtonItemHandler:)];
        [_tableView setEditing:YES];
        _editButton.hidden = YES;
    }
    
    [self tintIcons];
    
    [self setupBannerAdIfNeeded];
    
    _tableView.hidesLoadMoreIndicator = YES;
    _tableView.delegate = (id)self;
    _tableView.dataSource = (id)self;
    _tableView.allowsSelectionDuringEditing = YES;
    if (_editButton.hidden == NO) {
        UIEdgeInsets contentInset = _tableView.contentInset;
        contentInset.bottom = 50;
        _tableView.contentInset = contentInset;
    }
    
    _bottomToolViewForRegularType.hidden = YES;
    _bottomToolViewForPlaylistDetailsType.hidden = YES;
    [self updateOfflineButtonAndOfflineLabel];
    
    FEAddObserver(self, PlayerViewControllerPlayingStateChangedNotification, @selector(playerPlayingStateChanged:));
    FEAddObserver(self, DriveFileFetcherFileFetchingProgressUpdatedNotification, @selector(fileDownloadProgressUpdated:));
    FEAddObserver(self, kFLOWModeChangedNotification, @selector(downloadModeChanged:));
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _offlineSwitch.on = FEUserDefaultsBoolForKey(kUDKeyOffline);
    
    [kGlobal setAllTracksUnselected];
    [self reloadTrackData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _bannerView.frame = CGRectMake(0, 0, self.view.width, 50);
    _bannerView.bottom = self.view.height;
    
    [self bottomToolView].bottom = _bannerView.y;
    
    _editButton.right = self.view.width - 10;
    _editButton.bottom = _bannerView.hidden ? (self.view.height - 10) : (_bannerView.y - 10);
    
    CGFloat tableViewHeight = self.view.height - (_bannerView.hidden ? 0 : _bannerView.height);
    _tableView.frame = CGRectMake(0, 0, self.view.width, tableViewHeight);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notification
- (void)fileDownloadProgressUpdated:(NSNotification*)notification {
    NSDictionary *downloadingInfo = notification.userInfo;
    
    for (TrackCell *cell in [_tableView visibleCells]) {
        if (cell == _toolCell) {
            continue;
        }
        
        NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
        Track *track = _tracks[indexPath.row];
        if ([track.remoteID isEqualToString:downloadingInfo[@"key"]]) {
            [self updateAccessoryViewOfCell:cell withTrack:track];
            break;
        }
    }
}

- (void)playerPlayingStateChanged:(NSNotification*)notification {
    [_tableView reloadData];
}

- (void)downloadModeChanged:(NSNotification*)notification {
    [self updateOfflineButtonAndOfflineLabel];
}

#pragma mark - Tableview delegate & datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _sectionInfoList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    SectionInfo *sectionInfo = _sectionInfoList[section];
    return sectionInfo.items.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    
    if (sectionInfo.tag == kSectionTagTool) {
        return _toolCell;
    }
    else if (sectionInfo.tag == kSectionTagTracks) {
        static NSString *kIdentifier = @"C";
        
        TrackCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifier];
        if (cell == nil) {
            cell = [[TrackCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kIdentifier];
            
            ProgressButton *progressButton = [[ProgressButton alloc] initWithFrame:CGRectMake(0, 0, 48, _tableView.rowHeight)];
            progressButton.progressView.tintColor = colorForKey(@"tintColor");
            [progressButton addTarget:(id)self action:@selector(progressButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
            progressButton.progressView.size = CGSizeMake(25, 25);
            progressButton.progressView.circularState = FFCircularStateIcon;
            cell.accessoryView = progressButton;
            
            [cell setDidLayoutSubviews:^(TrackCell *cell) {
                cell.accessoryView.x += 15;
            }];
        }
        
        Track *track = _tracks[indexPath.row];
        
        cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:track.title attributes:[kGlobal thirdFontAttribute]];
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:track.iconURL] placeholderImage:kDefaultTrackArtwork];
        cell.detailTextLabel.text = [kGlobal formatSeconds:track.duration];
        
        // 如果未获取duration，则获取duration并保存track
        if (track.isAudioDownloaded && track.duration == 0) {
            [self updateTrackDuration:track andUpdateCellDurationText:cell];
        }
        
        // 更新Progress button状态
        [self updateAccessoryViewOfCell:cell withTrack:track];
        
        //
        cell.isSelected = (track.isSelected && _tableView.isEditing);
        
        // rhythm
        cell.showsRhythmView = [kGlobal.playerViewController.currentTrack.remoteID isEqualToString:track.remoteID];
        if (cell.showsRhythmView && kGlobal.playerViewController.isPlaying) {
            [cell.rhythmView startAnimating];
        }
        else {
            [cell.rhythmView stopAnimating];
        }
        
        return cell;
    }
    
    return nil;
}

- (void)updateTrackDuration:(Track*)track andUpdateCellDurationText:(TrackCell*)cell {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *symbolLinkPath = [NSString stringWithFormat:@"%@.mp3", track.audioLocalPath];
        [kFM createSymbolicLinkAtPath:symbolLinkPath withDestinationPath:track.audioLocalPath error:nil];
        
        NSURL *url = [NSURL fileURLWithPath:symbolLinkPath];
        AVURLAsset* asset = [AVURLAsset URLAssetWithURL:url options:nil];
        track.duration = CMTimeGetSeconds(asset.duration);
        [track save];
        
        [kFM removeItemAtPath:symbolLinkPath error:nil];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSIndexPath *nowIndexPath = [_tableView indexPathForCell:cell];
            if (_tracks[nowIndexPath.row] == track) {
                cell.detailTextLabel.text = [kGlobal formatSeconds:track.duration];
            }
        });
    });
}

- (void)updateAccessoryViewOfCell:(TrackCell*)cell withTrack:(Track*)track {
    NSDictionary *downloadingInfo = [kDriveFileFetcher fetchingInfoForKey:track.remoteID];
    ProgressButton *progressButton = (ProgressButton*)cell.accessoryView;
    
    if ([kGlobal isTrackFLOWable:track]) {
        if (downloadingInfo) {            
            progressButton.hidden = NO;
            CGFloat newProgress = [downloadingInfo[@"progress"] floatValue];
            progressButton.progressView.progress = newProgress;
            
            if (newProgress == 0) {
                progressButton.progressView.circularState = FFCircularStateStopSpinning;
            }
            else if (newProgress < 1) {
                progressButton.progressView.circularState = FFCircularStateStopProgress;
            }
            else {
                progressButton.progressView.circularState = FFCircularStateCompleted;
                setTimeout(0.1, ^{
                    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
                    [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                });
            }
        }
        else {
            progressButton.hidden = track.isAudioDownloaded;
            progressButton.progressView.progress = 0.0;
            progressButton.progressView.circularState = FFCircularStateIcon;
        }
    }
    else {
        progressButton.hidden = YES;
        progressButton.progressView.circularState = FFCircularStateStop;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    if (sectionInfo.tag == kSectionTagTool) {
        return 50;
    }
    
    return _tableView.rowHeight;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    return (sectionInfo.tag != kSectionTagTool);
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    
    if (_tableView.isEditing) {
        if (sectionInfo.tag == kSectionTagTracks) {
            Track *track = _tracks[indexPath.row];
            track.isSelected = !track.isSelected;
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
    else {
        if (_tracks.count > 0) {
            BOOL shuffled = (sectionInfo.tag == kSectionTagTool);
            NSInteger index = (shuffled ? arc4random() % _tracks.count : indexPath.row);
            [kGlobal enqueuTracks:_tracks andPlayTrackAtIndex:index shuffled:shuffled rootNavigationController:self.navigationController playlist:self.playlist];
        }
    }
}

#pragma mark - Handler
- (void)playerBarButtonItemHandler:(id)sender
{
    [self.navigationController pushViewController:kGlobal.playerViewController animated:YES];
}

- (IBAction)editButtonHandler:(id)sender {
    for (TrackCell *cell in [_tableView visibleCells]) {
        cell.isSelected = NO;
    }
    
    [_tableView setEditing:YES animated:YES];
    self.navigationItem.rightBarButtonItem = _endEditingBarButtonItem;
    _editButton.hidden = YES;
    
    UIView *bottomToolView = [self bottomToolView];
    bottomToolView.hidden = NO;
    bottomToolView.y = _bannerView.hidden ? _bannerView.bottom : _bannerView.y;
    [UIView animateWithDuration:0.2 animations:^{
        bottomToolView.y -= bottomToolView.height;
    } completion:^(BOOL finished) {
        //
    }];
}

- (void)endEditingBarButtonItemHandler:(UIBarButtonItem*)sender
{
    [_tableView setEditing:NO animated:YES];
    self.navigationItem.rightBarButtonItem = _playerBarButtonItem;
    _editButton.hidden = NO;
    
    [kGlobal setAllTracksUnselected];
    
    setTimeout(0.1, ^{
        UIView *bottomToolView = [self bottomToolView];
        
        [UIView animateWithDuration:0.2 animations:^{
            bottomToolView.y = _bannerView.y;
        } completion:^(BOOL finished) {
            bottomToolView.hidden = YES;
        }];
    });
}

- (IBAction)deleteButtonHandler:(id)sender {
    NSMutableArray *IDs = [NSMutableArray array];
    for (int i=(int)_tracks.count-1; i>=0; i--) {
        Track *track = _tracks[i];
        if (track.isSelected) {
            [IDs addObject:track.ID];
            [_tracks removeObjectAtIndex:i];
            
            if (isRegularType) {
                [track delete];
            }
        }
    }
    
    //从playlist中删除
    if (isRegularType) {
        for (Playlist *playlist in [Playlist allRecords]) {
            [playlist removeTracksWithIDs:IDs];
        }
    }
    else {
        [_playlist removeTracksWithIDs:IDs];
    }
    
    [_tableView reloadData];
    [self endEditingBarButtonItemHandler:_endEditingBarButtonItem];
}

- (IBAction)offlineSwitchValueChanged:(id)sender {
    FESetUserDefaults(kUDKeyOffline, @(_offlineSwitch.on));
    [self reloadTrackData];
}

- (void)progressButtonHandler:(id)sender
{
    TrackCell *cell = [kUtil findSuperViewWithClass:[TrackCell class] subview:sender];
    
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    Track *track = _tracks[indexPath.row];
    
    if (track.source == TrackSourceiTunesMusic) {
        if ([kDriveFileFetcher fetchingInfoForKey:track.remoteID] == nil) {
            [kDriveFileFetcher prepareToFetchFileWithKey:track.remoteID];
            
            MPMediaItem *item = [kiTunesMusicSource findMediaItemWithTrack:track];
            AVURLAsset *asset = [AVURLAsset assetWithURL:item.assetURL];
            AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
            exportSession.outputFileType = @"com.apple.quicktime-movie";
            
            // 扩展名
            NSString *extension = (__bridge  NSString *)UTTypeCopyPreferredTagWithClass((__bridge  CFStringRef)exportSession.outputFileType, kUTTagClassFilenameExtension);
            track.extension = extension;
            [track save];

            //
            exportSession.outputURL = [NSURL fileURLWithPath:track.audioLocalPath];
            
            [exportSession exportAsynchronouslyWithCompletionHandler:^{
                if (exportSession.status <= AVAssetExportSessionStatusExporting) {
                    return;
                }
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [kDriveFileFetcher cancelFetchingFileWithKey:track.remoteID];
                    
                    // TODO：完成效果
                    for (TrackCell *cell in _tableView.visibleCells) {
                        NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
                        if (indexPath.section == kSectionTagTracks && _tracks[indexPath.row] == track) {
                            [self updateAccessoryViewOfCell:cell withTrack:track];
                            break;
                        }
                    }
                });
            }];
        }
        else {
            // 暂时不支持取消
        }
    }
    else {
        if ([kDriveFileFetcher fetchingInfoForKey:track.remoteID] == nil) {
            [kDriveFileFetcher prepareToFetchFileWithKey:track.remoteID];
            [kGlobal fetchStreamRequestForTrack:track completionHandler:^(NSURLRequest *request) {
                [kDriveFileFetcher startFetchingFileWithRequest:request forKey:track.remoteID targetPath:track.audioLocalPath totalSize:track.size];
            }];
        }
        else {
            [kDriveFileFetcher cancelFetchingFileWithKey:track.remoteID];
        }
    }
    
    [self updateAccessoryViewOfCell:cell withTrack:track];
}

#pragma mark - Handler (_type == SongsViewControllerTypeRegular)
- (IBAction)addToPlaylistButtonHandler:(id)sender {
    if ([self selectedTracks].count == 0) {
        FEAlertController *alert = [[FEAlertController alloc] initWithTitle:@"" message:@"Please select at least one item" buttonTitles:nil cancelButtonTitle:@"OK" destructiveButtonTitle:nil preferredStyle:UIAlertControllerStyleAlert handler:nil];
        [alert showInController:self animated:YES completion:nil];
        return;
    }
    
    PlayListViewController *c = [[PlayListViewController alloc] initWithType:PlayListViewControllerTypeSelection];
    c.didCancelHandler = ^(PlayListViewController *c) {
        [self endEditingBarButtonItemHandler:_endEditingBarButtonItem];
        [c dismissViewControllerAnimated:YES completion:nil];
    };
    c.didSelectPlaylistsHandler = ^(NSArray *playlists, PlayListViewController *c) {
        NSArray *selectedTracks = [self selectedTracks];
        for (Playlist *playlist in playlists) {
            [playlist addTracks:selectedTracks];
        }

        [self endEditingBarButtonItemHandler:_endEditingBarButtonItem];
        [c dismissViewControllerAnimated:YES completion:nil];
    };
    
    UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentViewController:n animated:YES completion:nil];
}

#pragma mark - Handler (_type == SongsViewControllerTypePlaylistDetails)
- (IBAction)addSongsButtonHandler:(id)sender {
    SongsViewController *c = [[SongsViewController alloc] initWithType:SongsViewControllerTypeSelection];
    c.didCancelHandler = ^(SongsViewController *c) {
        [self endEditingBarButtonItemHandler:_endEditingBarButtonItem];
        [c dismissViewControllerAnimated:YES completion:nil];
    };
    c.didSelectTracksHandler = ^(NSArray* tracks, SongsViewController *c) {
        [_playlist addTracks:tracks];
        [self endEditingBarButtonItemHandler:_endEditingBarButtonItem];
        [c dismissViewControllerAnimated:YES completion:nil];
    };
    
    UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentViewController:n animated:YES completion:nil];
}

#pragma mark - Handler (_type == SongsViewControllerTypeSelection)
- (void)cancelButtonItemHandler:(UIBarButtonItem*)sender
{
    if (_didCancelHandler) {
        _didCancelHandler(self);
    }
}

- (void)finishSelectingTracksButtonItemHandler:(UIBarButtonItem*)sender
{
    if (_didSelectTracksHandler == nil) {
        return;
    }
    
    NSMutableArray *selectedTracks = [NSMutableArray array];
    for (Track *track in _tracks) {
        if (track.isSelected) {
            [selectedTracks addObject:track];
        }
    }
    _didSelectTracksHandler(selectedTracks, self);
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (_bannerView.hidden) {
        _bannerView.hidden = NO;
        [self.view bringSubviewToFront:_bannerView];
        [self.view setNeedsLayout];
    }
}

#pragma mark - Private
- (void)reloadTrackData
{
    _sectionInfoList = [NSMutableArray array];
    
    NSArray *tracks = nil;
    if (isRegularType || isSelectionType) {
        tracks = [Track allRecords];
        tracks = [tracks sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSDate *d1 = [(Track*)obj1 creationDate];
            NSDate *d2 = [(Track*)obj2 creationDate];
            return [d2 compare:d1];
        }];
    }
    else if (isPlaylistDetailsType) {
        tracks = [[_playlist tracks] mutableCopy];
    }
    
    //check for offline
    if (_offlineSwitch.on == NO) {
        _tracks = [tracks mutableCopy];
    }
    else {
        _tracks = [NSMutableArray array];
        for (Track *track in tracks) {
            if (track.isAudioDownloaded) {
                [_tracks addObject:track];
            }
        }
    }
    
    [_sectionInfoList addObject:newSectionInfo(nil, @[_toolCell], kSectionTagTool)];
    [_sectionInfoList addObject:newSectionInfo(nil, _tracks, kSectionTagTracks)];
    
    [_tableView reloadData];
}

- (NSArray*)selectedTracks
{
    NSMutableArray *r = [NSMutableArray array];
    for (Track *track in _tracks) {
        if (track.isSelected) {
            [r addObject:track];
        }
    }
    return r;
}

- (UIView*)bottomToolView
{
    UIView *r = nil;
    if (isRegularType) {
        r = _bottomToolViewForRegularType;
    }
    if (isPlaylistDetailsType) {
        r = _bottomToolViewForPlaylistDetailsType;
    }
    
    return r;
}

- (UIBarButtonItem*)createEndEditingBarButtonItem
{
    return [self barButtonItem:@"DONE" selector:@selector(endEditingBarButtonItemHandler:)];
}

- (UIBarButtonItem *)barButtonItem:(NSString *)title selector:(SEL)selector {
    float targetImageHeight = 26;
    UIView *container = [[UIView alloc] init];
    
    UILabel *label = [[UILabel alloc] init];
    [container addSubview:label];
    label.attributedText = [[NSAttributedString alloc] initWithString:title attributes:[kGlobal navigationBarItemAttribute]];
    [label sizeToFit];
    label.y = (targetImageHeight - label.height) / 2.0;
    
    container.frame = CGRectMake(0, 0, label.right, targetImageHeight);
    UIImage *image = [kUtil viewToImage:container scale:0];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:(id)self action:selector];
    item.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    return item;
}


- (void)updateOfflineButtonAndOfflineLabel
{
    BOOL hidesOffline = (FEUserDefaultsIntegerForKey(kUDKeyFLOWMode) == FLOWModeLimited);
    _offlineSwitch.hidden = hidesOffline;
    _offlineLabel.hidden = hidesOffline;
}

- (void)setupBannerAdIfNeeded
{
    _bannerView.hidden = YES;
    
    if ([kParamsManager boolForName:kParamNameShowsBannerAd] && isSelectionType == NO) {
        _bannerView.adUnitID = kGlobal.bannerUnitID;
        _bannerView.delegate = (id)self;
        _bannerView.rootViewController = self;
        _bannerView.autoloadEnabled = YES;
        [_bannerView loadRequest:[GADRequest request]];
    }
}

- (void)tintIcons {
    UIImage *shuffleIcon = _shuffleImageView.image;
    shuffleIcon = [kUtil tintImage:shuffleIcon withColor:kGlobal.tintColor];
    _shuffleImageView.image = shuffleIcon;
    
    CGFloat shadowRadius = 4.0;
    _editButton.layer.shadowRadius = shadowRadius;
    _editButton.layer.shadowColor = kGlobal.tintColor.CGColor;
    _editButton.layer.shadowOpacity = 0.7;
    _editButton.layer.cornerRadius = _editButton.halfHeight;
    [_editButton.layer setShadowPath:[[UIBezierPath bezierPathWithRoundedRect:CGRectMake(CGRectGetX(_editButton.bounds) - shadowRadius/2, CGRectGetY(_editButton.bounds) + shadowRadius/2, CGRectGetWidth(_editButton.bounds) + shadowRadius/2, CGRectGetHeight(_editButton.bounds) + shadowRadius/2) cornerRadius:_editButton.layer.cornerRadius] CGPath]];
}

@end

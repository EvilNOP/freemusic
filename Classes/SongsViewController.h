//
//  SongsViewController.h
//  Drive
//
//  Created by EvilNOP on 15/3/11.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    SongsViewControllerTypeRegular,
    SongsViewControllerTypePlaylistDetails,
    SongsViewControllerTypeSelection
} SongsViewControllerType;

@interface SongsViewController : UIViewController

- (id)initWithType:(SongsViewControllerType)type;

@property (nonatomic, readonly) SongsViewControllerType type;
@property (nonatomic, strong) Playlist *playlist;

// _type == SongsViewControllerTypeSelection
@property (nonatomic, strong) void (^didCancelHandler) (SongsViewController *c);
@property (nonatomic, strong) void (^didSelectTracksHandler) (NSArray* tracks, SongsViewController *c);

@end

//
//  Slider.m
//  Drive
//
//  Created by EvilNOP on 15/3/30.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "Slider.h"

@interface Slider () {
    UIButton *_handleButton;
    UIView *_trackView;
    UIView *_progressView;
    UIView *_loadedRangeView;
    NSRange _loadedRange;
    float _previousIconX;
}

@end

@implementation Slider

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.includeOutsideSubviewEvent = YES;
    
    _previousIconX = -1;
    _minValue = 0;
    _maxValue = 1.0;
    _draggable = YES;
    _trackHeight = 4;
    
    _trackView = [[UIView alloc] init];
    _trackView.backgroundColor = FEHexRGBA(0xe2e2e2, 1.0);
    [self addSubview:_trackView];
    
    _loadedRangeView = [[UIView alloc] init];
    _loadedRangeView.backgroundColor = FEHexRGBA(0xc2c2c2, 1.0);
    [self addSubview:_loadedRangeView];
    
    _progressView = [[UIView alloc] init];
    _progressView.backgroundColor = FEHexRGBA(0x2c7abd, 1.0);
    [self addSubview:_progressView];
    
    _handleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kSliderHandleButtonWidth, kSliderHeight)];
    UIView *v = [[UIView alloc] initWithFrame:_handleButton.bounds];
    v.backgroundColor = _progressView.backgroundColor;
    UIImage *icon = [kUtil viewToImage:v scale:0];
    [self setHandleIcon:icon];
    
    [_handleButton addTarget:(id)self action:@selector(handleButtonTouchDown:) forControlEvents:UIControlEventTouchDown];
    [_handleButton addTarget:(id)self action:@selector(handleButtonTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    [_handleButton addTarget:(id)self action:@selector(handleButtonTouchUp:) forControlEvents:UIControlEventTouchUpOutside];
    [_handleButton addTarget:(id)self action:@selector(handleButtonDrag:forEvent:) forControlEvents:UIControlEventTouchDragInside];
    [_handleButton addTarget:(id)self action:@selector(handleButtonDrag:forEvent:) forControlEvents:UIControlEventTouchDragOutside];
    [self addSubview:_handleButton];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (_dragging) {
        return;
    }

    [self updateUI];
}

#pragma mark - Overwrite
- (void)setFrame:(CGRect)frame
{
    frame.size.height = kSliderHeight;
    [super setFrame:frame];
}

#pragma mark - Handler
- (void)handleButtonTouchDown:(UIButton*)sender
{
    _dragging = YES;
}

- (void)handleButtonDrag:(UIButton*)sender forEvent:(UIEvent *)event
{
    if (_previousIconX < 0) { //拖动按钮后第一次调用
        _previousIconX = _progressView.width;
    }
    
    UITouch *touch = [[event touchesForView:_handleButton] anyObject];
    CGPoint location = [touch locationInView:self];

    float iconX = location.x - [self iconWidth]/2.0;
    if (iconX < 0) {
        iconX = 0;
    }
    else if (iconX > [self maxIconX]) {
        iconX = [self maxIconX];
    }
    [self updateHandleButtonAndProgressViewWithIconX:iconX];
    
    if (fabsf(_previousIconX - iconX) >= 2.0 ||
        (
         fabsf(_previousIconX - iconX) > 0 &&
         (iconX == 0 || iconX == [self maxIconX])
        )
    ) {
        _value = iconX / [self maxIconX] * _maxValue;
        if (_valueChangedhandler) {
            _valueChangedhandler(self);
        }
        _previousIconX = iconX;
    }
}

- (void)handleButtonTouchUp:(UIButton*)sender
{
    _dragging = NO;
    _value = (_handleButton.center.x - [self iconWidth]/2.0) / [self maxIconX] * _maxValue;
    
    if (_valueChangedhandler) {
        _valueChangedhandler(self);
    }
    
    _previousIconX = -1;
}

#pragma mark - Private
- (float)maxIconX
{
    return self.width - [self iconWidth];
}

- (void)updateUI
{
    _trackView.frame = CGRectMake(0, 0, self.width, _trackHeight);
    _trackView.center = CGPointMake(_trackView.center.x, self.height/2.0);
    
    [self updateLoadedRangeView];
    [self updateHandleButtonAndProgressView];
}

- (void)updateLoadedRangeView
{
    float viewY = (self.height - _trackHeight) / 2.0;
    float rate = [self maxIconX] / _maxValue;
    _loadedRangeView.frame = CGRectMake(_loadedRange.location*rate,
                                        viewY,
                                        ceilf(_loadedRange.length*rate),
                                        _trackHeight);
    if (_loadedRangeView.right >= [self maxIconX]) {
        _loadedRangeView.right = self.width;
    }
}

- (void)updateHandleButtonAndProgressView
{
    float iconX = ceilf(_value / _maxValue * [self maxIconX]);
    [self updateHandleButtonAndProgressViewWithIconX:iconX];
}

- (void)updateHandleButtonAndProgressViewWithIconX:(float)iconX
{
    _handleButton.center = CGPointMake(iconX + [self iconWidth]/2.0, self.height/2.0);
 
    _progressView.frame = CGRectMake(0, 0, _handleButton.center.x, _trackHeight);
    _progressView.center = CGPointMake(_progressView.center.x, self.height/2.0);
}

- (float)iconWidth
{
    return _handleButton.imageView.image.size.width;
}

#pragma mark - Public
- (void)setDraggable:(BOOL)draggable
{
    _draggable = draggable;
    _handleButton.enabled = _draggable;
}

- (void)setTrackColor:(UIColor *)color
{
    _trackView.backgroundColor = color;
}

- (void)setProgressColor:(UIColor *)color
{
    _progressView.backgroundColor = color;
}

- (void)setLoadedRangeColor:(UIColor *)color
{
    _loadedRangeView.backgroundColor = color;
}

- (void)setLoadedRange:(NSRange)range
{
    _loadedRange = range;
    [self updateUI];
}

- (void)setValue:(float)value
{
    _value = value;
    [self updateUI];
}

- (void)setHandleIcon:(UIImage *)icon
{
    assert(icon.size.height > 0 &&
           icon.size.height <= kSliderHeight &&
           icon.size.width > 0 &&
           icon.size.width <= kSliderHandleButtonWidth);
    
    [_handleButton setImage:icon forState:UIControlStateNormal];
    [_handleButton setImage:icon forState:UIControlStateHighlighted];
    [self updateUI];
}

@end

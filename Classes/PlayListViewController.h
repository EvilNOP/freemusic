//
//  PlayListViewController.h
//  Drive
//
//  Created by EvilNOP on 15/3/11.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    PlayListViewControllerTypeRegular,
    PlayListViewControllerTypeSelection
} PlayListViewControllerType;

@interface PlayListViewController : UIViewController

- (id)initWithType:(PlayListViewControllerType)type;

@property (nonatomic, readonly) PlayListViewControllerType type;
@property (nonatomic, strong) void (^didCancelHandler) (PlayListViewController *c);
@property (nonatomic, strong) void (^didSelectPlaylistsHandler) (NSArray *playlists, PlayListViewController *c);

@end

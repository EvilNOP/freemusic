//
//  Model.h
//  Drive
//
//  Created by EvilNOP on 15/3/12.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Model : NSObject <NSCoding>

@property (strong, nonatomic) NSString *ID;
@property (readonly, nonatomic) NSDate *creationDate;
@property (readonly, nonatomic) NSDate *modificationDate;

- (void)save;
- (void)delete;

+ (void)setPersistantStorePath:(NSString*)path;

+ (id)findByID:(NSString*)ID;
+ (NSArray*)allRecords;
+ (BOOL)existedWithID:(NSString*)ID;

@end
 

//
//  ScreenShotScrollView.m
//  helo
//
//  Created by EvilNOP on 14-1-5.
//  Copyright (c) 2014年 Fire. All rights reserved.
//

#import "ScreenShotScrollView.h"

@implementation ScreenShotScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    float marginLeft = 8, marginRight = 8, marginTop = 8, marginBottom = 2, interval = 8;
    float imageViewHeight = self.bounds.size.height - marginTop - marginBottom;
    float x = marginLeft;
    for (UIImageView *v in _imageViewList) {
        float imageViewWidth = v.image.size.width / v.image.size.height * imageViewHeight;
        v.frame = CGRectMake(x,
                             marginTop,
                             imageViewWidth,
                             imageViewHeight);
        x += (imageViewWidth + interval);
    }
    
    self.contentSize = CGSizeMake(x-interval+marginRight, 0);
}

- (void)loadData {
    [self unloadData];
    
    int number = [_datasource numberOfImagesForScreenShotScrollView:self];
    _imageViewList = [NSMutableArray array];
    for (int i=0; i<number; i++) {
        UIImage *image = [_datasource screenShotScrollView:self imageForCellAtIndex:i];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.layer.borderWidth = 0.5;
        imageView.layer.borderColor = FEHexRGBA(0xcccccc, 1.0).CGColor;
        imageView.layer.masksToBounds = YES;
        [self addSubview:imageView];
        [_imageViewList addObject:imageView];
    }
    
    [self layoutSubviews];
}

- (void)unloadData
{
    for (UIImageView *v in _imageViewList) {
        [v removeFromSuperview];
    }
    _imageViewList = nil;
}

@end

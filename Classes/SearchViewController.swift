//
//  SearchViewController.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/1/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit

@objc class SearchViewController: UIViewController {
    
    private var topContentView: UIView!
    
    private var googleDriveButton: UIButton!
    private var dropboxButton: UIButton!
    private var oneDriveButton: UIButton!
    
    private var indicator: UIImageView!
    
    private var googleDriveListView: GoogleDriveListView!
    private var dropboxListView: DropboxListView!
    private var oneDriveListView: OneDriveListView!
    
    private var ignoresLayoutSubview = false
    
    private var buttons: [UIButton] = []
    private var availableDrives: [Int] = []
    
    private var selectedDriveIndex:Int = 1 {
        didSet {
            let index = self.selectedDriveIndex
            
            self.googleDriveButton.selected = (index == 1)
            self.dropboxButton.selected = (index == 2)
            self.oneDriveButton.selected = (index == 3)
            
            switch index {
            case 0:
                break
            case 1:
                self.view.bringSubviewToFront(self.googleDriveListView)
            case 2:
                self.view.bringSubviewToFront(self.dropboxListView)
            case 3:
                self.view.bringSubviewToFront(self.oneDriveListView)
            default:
                break
            }
        }
    }

    private let iconR:CGFloat = 40.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Global.sharedInstance().navigationItem(navigationItem, customTitle: "SEARCH")
        navigationItem.rightBarButtonItem = Global.sharedInstance().createPlayerBarButtonItem()
        
        edgesForExtendedLayout = .None
        
        topContentView = UIView()
        topContentView.backgroundColor = UIColor.whiteColor()
        
        googleDriveButton = UIButton()
        googleDriveButton.setImage(kUtil.tintImage(UIImage(named: "GoogleDrivePre"), withColor: colorForKey("primaryColor")), forState: .Normal)
        googleDriveButton.setImage(UIImage(named: "SearchGoogleDrive"), forState: .Selected)
        googleDriveButton.addTarget(self, action: #selector(googleDriveButtonAction), forControlEvents: .TouchUpInside)
        
        dropboxButton = UIButton()
        dropboxButton.setImage(kUtil.tintImage(UIImage(named: "DropboxPre"), withColor: colorForKey("primaryColor")), forState: .Normal)
        dropboxButton.setImage(UIImage(named: "SearchDropbox"), forState: .Selected)
        dropboxButton.addTarget(self, action: #selector(dropboxButtonAction), forControlEvents: .TouchUpInside)
        
        oneDriveButton = UIButton()
        oneDriveButton.setImage(kUtil.tintImage(UIImage(named: "OneDrivePre"), withColor: colorForKey("primaryColor")), forState: .Normal)
        oneDriveButton.setImage(UIImage(named: "OneDrivePreSearching"), forState: .Selected)
        oneDriveButton.addTarget(self, action: #selector(oneDriveButtonAction), forControlEvents: .TouchUpInside)
        
        self.buttons = [self.googleDriveButton, self.dropboxButton, self.oneDriveButton]
        
        indicator = UIImageView(image: UIImage(named: "SearchIndicator"))
        indicator.contentMode = .ScaleAspectFit
        
        googleDriveListView = GoogleDriveListView(controller: self, searching: true)
        dropboxListView = DropboxListView(controller: self, searching: true)
        oneDriveListView = OneDriveListView(controller: self, searching: true)

        self.topContentView.addSubview(self.googleDriveButton)
        self.topContentView.addSubview(self.dropboxButton)
        self.topContentView.addSubview(self.oneDriveButton)
        
        self.topContentView.addSubview(indicator)
        
        self.view.addSubview(self.oneDriveListView)
        self.view.addSubview(self.dropboxListView)
        self.view.addSubview(self.googleDriveListView)
        
        view.addSubview(topContentView)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(paramsManagerDidUpdateParams(_:)), name: kParamsManagerDidUpdateParamsNotification, object: nil)
        
        self.selectedDriveIndex = 1
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.regulationDriveCount()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if ignoresLayoutSubview {
            return
        }
        
        if self.availableDrives.contains(0) {
            self.availableDrives.removeAtIndex(self.availableDrives.indexOf(0)!)
        }
        
        topContentView.frame = CGRectMake(0, 0, view.width, 74)
        
        let interval = (self.view.width - CGFloat(self.availableDrives.count) * self.iconR) /  CGFloat(self.availableDrives.count + 1)
        
        googleDriveButton.frame = CGRectMake(0, 10, iconR, iconR)
        dropboxButton.frame = CGRectMake(0, 10, iconR, iconR)
        oneDriveButton.frame = CGRectMake(0, 10, iconR, iconR)
    
        
        print(self.availableDrives)
        
        for position in self.availableDrives {
            let index = CGFloat(self.availableDrives.indexOf(position)!)
            let x = interval * (index + 1) + index * self.iconR
            if position == 0 {
                
            } else if position == 1 {
                self.googleDriveButton.x = x
            } else if position == 2 {
                self.dropboxButton.x = x
            } else if position == 3 {
                self.oneDriveButton.x = x
            }
        }
        
        self.indicator.centerX = self.selectedButton()?.centerX ?? 0
        self.indicator.bottom = self.topContentView.height + 1
        
        let listViewFrmae = CGRectMake(0, topContentView.bottom, view.width, view.height - topContentView.height)
        googleDriveListView.frame = listViewFrmae
        dropboxListView.frame = listViewFrmae
        oneDriveListView.frame = listViewFrmae
    }
}

// MARK: - Notification
extension SearchViewController {
    func paramsManagerDidUpdateParams(notification: NSNotification) {
        self.regulationDriveCount()
    }
}

// MARK: - Button action
extension SearchViewController {
    
    func googleDriveButtonAction() {
        self.selectedDriveIndex = 1
    }
    
    func dropboxButtonAction() {
        self.selectedDriveIndex = 2
    }
    
    func oneDriveButtonAction() {
        self.selectedDriveIndex = 3
    }
}

// MARK: - Util
extension SearchViewController {
    
    func changeIndicatorPositionWithButton(button: UIButton) {
        ignoresLayoutSubview = true
        
        UIView.animateWithDuration(0.5, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 10, options: [], animations: { 
            self.indicator.centerX = button.centerX
            }) { (finish) in
                if finish {
                    self.ignoresLayoutSubview = false
                }
        }
    }
}

// MARK: - Private
extension SearchViewController {
    
    func selectedButton() -> UIButton? {
        for button in [self.googleDriveButton, self.dropboxButton, self.oneDriveButton] {
            if button.selected {
                return button
            }
        }
        
        return nil
    }
    
    func regulationDriveCount() {
        let drives = kOnlineParamsManager.valueForName(kParamNameAvailableDrives) as? [Int]
        if drives == nil || drives?.count == 0 {
            self.availableDrives = [1, 2, 3]
        }
        else {
            self.availableDrives = drives!
        }

        if self.availableDrives.contains(self.selectedDriveIndex) == false {
            
            if self.availableDrives.contains(0) {
                self.availableDrives.removeAtIndex(self.availableDrives.indexOf(0)!)
            }
            
            self.selectedDriveIndex = self.availableDrives.first ?? 1
        }
        
        self.regulationGoogleDrive()
        self.regulationDropboxDrive()
        self.regulationOneDrive()
        
        self.view.setNeedsLayout()
        
        self.indicator.hidden = (self.availableDrives.count == 1 && self.availableDrives.contains(0) || self.availableDrives.count == 0)
    }
    
    func regulationGoogleDrive() {
        if self.availableDrives.contains(1) {
            self.googleDriveButton.hidden = false
            self.googleDriveListView.hidden = false
        } else {
            self.googleDriveButton.hidden = true
            self.googleDriveListView.hidden = true
        }
    }
    
    func regulationDropboxDrive() {
        if self.availableDrives.contains(2) {
            self.dropboxButton.hidden = false
            self.dropboxListView.hidden = false
        } else {
            self.dropboxButton.hidden = true
            self.dropboxListView.hidden = true
        }
    }
    
    func regulationOneDrive() {
        if self.availableDrives.contains(3) {
            self.oneDriveButton.hidden = false
            self.oneDriveListView.hidden = false
        } else {
            self.oneDriveButton.hidden = true
            self.oneDriveListView.hidden = true
        }
    }

}

//
//  GoogleDriveBrowserViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "GoogleDriveBrowserViewController.h"
#import "FreeMusic-Swift.h"

@interface GoogleDriveBrowserViewController()

@property (nonatomic, strong) GoogleDriveListView *googleDriveListView;

@end

@implementation GoogleDriveBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    if (self.file == nil) {
        [kGlobal navigationItem:self.navigationItem customTitle:@"Google Drive"];
    } else {
        [kGlobal navigationItem:self.navigationItem customTitle:self.file.name];
    }
    
    self.googleDriveListView = [[GoogleDriveListView alloc] initWithController:self searching:NO];

    if ([self.googleDriveListView isVerified]) {
        [self.googleDriveListView fetchFiles: self.file];
    } else {
        [self.googleDriveListView verify];
    }
    
    __weak GoogleDriveBrowserViewController *weakSelf = self;
    self.googleDriveListView.cellSelectedHandler = ^(NSInteger index, BOOL isFolder, GTLDriveFile *file) {
        if (isFolder) {
            GoogleDriveBrowserViewController *googleDriveBrowser = [[GoogleDriveBrowserViewController alloc] init];
            googleDriveBrowser.file = file;
            [weakSelf.navigationController pushViewController:googleDriveBrowser animated:true];
        }
    };
    
    self.googleDriveListView.cancelSignInHandler = ^() {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    [self.view addSubview:self.googleDriveListView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.googleDriveListView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}

@end

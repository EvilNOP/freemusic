//
//  SourceTableViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/1/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "SourceTableViewController.h"
#import "GoogleDriveBrowserViewController.h"
#import "DropboxBrowserViewController.h"
#import "OneDriveBrowserViewController.h"
#import "iTunesMusicViewController.h"
#import "iTunesFileSharingTableViewController.h"
#import "WifiTransferTableViewController.h"

typedef NS_ENUM(NSUInteger, SourceType) {
    CloudSource = 0,
    Others
};

typedef NS_ENUM(NSUInteger, CloudType) {
    CloudTypeGoogleDrive,
    CloudTypeDropbox,
    CloudTypeOneDrive
};

typedef NS_ENUM(NSUInteger, OthersType) {
    iTunesMusic = 0,
    iTunesFileSharing,
    WifiTransfer
};

@interface SourceTableViewController()

@property (weak, nonatomic) IBOutlet UIImageView *soundCloundIcon;
@property (weak, nonatomic) IBOutlet UIImageView *googleDriveIcon;
@property (weak, nonatomic) IBOutlet UIImageView *dropboxIcon;
@property (weak, nonatomic) IBOutlet UIImageView *oneDriveIcon;
@property (weak, nonatomic) IBOutlet UIImageView *iTunesMusicIcon;
@property (weak, nonatomic) IBOutlet UIImageView *iTunesFileSharingIcon;
@property (weak, nonatomic) IBOutlet UIImageView *wifiTransferIcon;

@end

@implementation SourceTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.googleDriveIcon.image = [kUtil tintImage:[UIImage imageNamed:@"GoogleDrive"] withColor:colorForKey(@"primaryColor")];
    self.dropboxIcon.image = [kUtil tintImage:[UIImage imageNamed:@"Dropbox"] withColor:colorForKey(@"primaryColor")];
    self.oneDriveIcon.image = [kUtil tintImage:[UIImage imageNamed:@"OneDrive"] withColor:colorForKey(@"primaryColor")];
    self.iTunesMusicIcon.image = [kUtil tintImage:[UIImage imageNamed:@"iTunesMusic"] withColor:colorForKey(@"primaryColor")];
    self.iTunesFileSharingIcon.image = [kUtil tintImage:[UIImage imageNamed:@"iTunesFileSharing"] withColor:colorForKey(@"primaryColor")];
    self.wifiTransferIcon.image = [kUtil tintImage:[UIImage imageNamed:@"WiFiTransfer"] withColor:colorForKey(@"primaryColor")];
    
    FEAddObserver(self, kParamsManagerDidUpdateParamsNotification, @selector(paramsManagerDidUpdateParams:));
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Notification
- (void)paramsManagerDidUpdateParams:(NSNotification*)notification {
    [self.tableView reloadData];
}

#pragma mark - Table View delegate & data source

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.clipsToBounds = YES;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == CloudSource) {

        if (indexPath.row == CloudTypeGoogleDrive) {
            GoogleDriveBrowserViewController *googleDriveBrowser = [[GoogleDriveBrowserViewController alloc] init];
            [self.navigationController pushViewController:googleDriveBrowser animated:true];
            
        } else if (indexPath.row == CloudTypeDropbox) {
            DropboxBrowserViewController *dropboxBrowser = [[DropboxBrowserViewController alloc] init];
            [self.navigationController pushViewController:dropboxBrowser animated:true];
            
        } else if (indexPath.row == CloudTypeOneDrive) {
            OneDriveBrowserViewController *oneDriveBrowser = [[OneDriveBrowserViewController alloc] init];
            [self.navigationController pushViewController:oneDriveBrowser animated:true];
        }
        
    } else if (indexPath.section == Others) {
        
        if (indexPath.row == iTunesMusic) {
            iTunesMusicViewController *iTunesMusic = [[iTunesMusicViewController alloc] init];
            [self.navigationController pushViewController:iTunesMusic animated:YES];
            
        } else if (indexPath.row == iTunesFileSharing) {
            UIStoryboard *sourceStoryboard = [UIStoryboard storyboardWithName:@"iTunesFileSharing" bundle:[NSBundle mainBundle]];
            iTunesFileSharingTableViewController *iTunesFileSharing = (iTunesFileSharingTableViewController *)[sourceStoryboard instantiateViewControllerWithIdentifier:@"iTunesFileSharingTableViewController"];
            [self.navigationController pushViewController:iTunesFileSharing animated:YES];
            
        } else if (indexPath.row == WifiTransfer) {
            UIStoryboard *sourceStoryboard = [UIStoryboard storyboardWithName:@"WifiTransfer" bundle:[NSBundle mainBundle]];
            WifiTransferTableViewController *wifiTransfer = (WifiTransferTableViewController *)[sourceStoryboard instantiateViewControllerWithIdentifier:@"WifiTransferTableViewController"];
            [self.navigationController pushViewController:wifiTransfer animated:YES];
        }
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 55.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *availableDrivces = [kParamsManager valueForName:kParamNameAvailableDrives];
    if ([availableDrivces isKindOfClass:[NSArray class]] == NO || availableDrivces.count == 0) {
        availableDrivces = @[@1, @2, @3];
    }
    
    CGFloat rowHeight = 50;
    
    if (indexPath.section == 0) {
        if ([availableDrivces containsObject:@(indexPath.row + 1)] == NO) {
            rowHeight = 0;
        }
    }

    return rowHeight;
}

#pragma mark - Private

@end

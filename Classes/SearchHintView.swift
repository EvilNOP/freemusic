//
//  NoLoginView.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit

class NoLoginView: UIView {
    
    private let loginButton = UIButton()
    private let loginLabel = UILabel()
    
    var loginAction: (() -> Void)?
    
    convenience init(loginHandler: (() -> Void)?) {
        self.init()
        
        self.backgroundColor = Global.sharedInstance().colorForKey("bigBackgroundColor")
        loginButton.setImage(UIImage(named: "LoginRequired"), forState: .Normal)
        loginButton.addTarget(self, action: #selector(NoLoginView.loging), forControlEvents: .TouchUpInside)
        
        loginLabel.text = "Login Required"
        loginLabel.textColor = UIColor(red: 199/255, green: 199/255, blue: 204/255, alpha: 1.0)
        loginLabel.textAlignment = .Center
        
        loginAction = loginHandler
        
        self.addSubview(loginButton)
        self.addSubview(loginLabel)
    }
    
    init() {
        super.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        
        loginButton.frame = CGRectMake(0, 20, 90, 90)
        loginButton.centerX = self.halfWidth
        
        loginLabel.frame = CGRectMake(0, loginButton.bottom + 8, self.width, 20)
        loginButton.centerX = self.halfWidth
    }
}

// MARK: - Button action
extension NoLoginView {
    
    func loging() {
        if let handler = loginAction {
            handler()
        }
    }
}

class NoSearchingView: UIView {
    
    private let noSearchingImageView = UIImageView(image: UIImage(named: "NoResults"))
    private let noSearchingLabel = UILabel()
    
    init() {
        super.init(frame: CGRectZero)
        
        noSearchingLabel.text = "No Results"
        noSearchingLabel.textColor = UIColor(red: 199/255, green: 199/255, blue: 204/255, alpha: 1.0)
        noSearchingLabel.textAlignment = .Center
        
        self.addSubview(noSearchingImageView)
        self.addSubview(noSearchingLabel)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
    
        noSearchingImageView.y = 20
        noSearchingImageView.centerX = self.halfWidth
        
        noSearchingLabel.frame = CGRectMake(0, noSearchingImageView.bottom + 8, self.width, 20)
        noSearchingImageView.centerX = self.halfWidth
    }
}

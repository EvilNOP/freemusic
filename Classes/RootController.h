//
//  RootViewController.h
//  Drive
//
//  Created by EvilNOP on 15/3/10.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kRootControllerDidSelectViewControllerNotification = @"kRootControllerDidSelectViewControllerNotification";

@interface RootController : UITabBarController

- (void)setRedDotNumber:(NSUInteger)number;

@end

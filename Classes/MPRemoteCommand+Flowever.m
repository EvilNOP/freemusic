//
//  MPRemoteCommandCenter+Flowever.m
//  FreeMusic
//
//  Created by EvilNOP on 15/8/26.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "MPRemoteCommand+Flowever.h"
#import <objc/runtime.h>

@implementation MPRemoteCommand (Flowever)

+ (void)load {
    Method m1;
    Method m2;
    
    m1 = class_getInstanceMethod(self, @selector(addTargetWithHandler:));
    m2 = class_getInstanceMethod(self, @selector(newAddTargetWithHandler:));
    method_exchangeImplementations(m1, m2);
    
    m1 = class_getInstanceMethod(self, @selector(addTarget:action:));
    m2 = class_getInstanceMethod(self, @selector(newAddTarget:action:));
    method_exchangeImplementations(m1, m2);
    
    m1 = class_getInstanceMethod(self, @selector(addTarget:action:forControlEvents:));
    m2 = class_getInstanceMethod(self, @selector(newAddTarget:action:forControlEvents:));
    method_exchangeImplementations(m1, m2);
}

- (id)newAddTargetWithHandler:(MPRemoteCommandHandlerStatus(^)(MPRemoteCommandEvent *event))handler {
    if (MPRemoteCommandCenterUpdatable) {
        return [self newAddTargetWithHandler:handler];
    }
    
    NSLog(@"%s fu", __func__);
    return nil;
}

- (void)newAddTarget:(id)target action:(SEL)action {
    if (MPRemoteCommandCenterUpdatable) {
        [self newAddTarget:target action:action];
    } else {
        NSLog(@"%s fu", __func__);
    }
}

- (void)newAddTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents {
    if (MPRemoteCommandCenterUpdatable) {
        [self newAddTarget:target action:action forControlEvents:controlEvents];
    } else {
        NSLog(@"%s fu", __func__);
    }
}

@end

//
//  TrackTableView.m
//  Drive
//
//  Created by EvilNOP on 15/3/10.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "TrackTableView.h"

#define kHeightOfLoadingArea 40

@interface TrackTableView () {
    UIActivityIndicatorView *_loadMoreIndicator;
}

@end

@implementation TrackTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeTrackTableView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeTrackTableView];
    }
    return self;
}

- (void)initializeTrackTableView
{
    self.rowHeight = 70;
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _loadMoreIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _loadMoreIndicator.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [_loadMoreIndicator startAnimating];
    [self addSubview:_loadMoreIndicator];
    
    [self setHidesLoadMoreIndicator:NO];
}

#pragma mark - Overwrite
- (void)layoutSubviews
{
    [super layoutSubviews];
    _loadMoreIndicator.center = CGPointMake(self.width/2.0,
                                            self.contentSize.height+kHeightOfLoadingArea/2.0);
}

#pragma mark - Public
- (void)setHidesLoadMoreIndicator:(BOOL)hidesIndicatorView
{
    if (hidesIndicatorView) {
        [_loadMoreIndicator stopAnimating];
        _loadMoreIndicator.hidden = YES;
        self.contentInset = UIEdgeInsetsMake(self.contentInset.top,
                                             self.contentInset.left,
                                             0,
                                             self.contentInset.right);
    }
    else {
        [_loadMoreIndicator startAnimating];
        _loadMoreIndicator.hidden = NO;
        self.contentInset = UIEdgeInsetsMake(self.contentInset.top,
                                             self.contentInset.left,
                                             kHeightOfLoadingArea,
                                             self.contentInset.right);
    }
}

- (BOOL)hidesLoadMoreIndicator
{
    return _loadMoreIndicator.hidden;
}

@end

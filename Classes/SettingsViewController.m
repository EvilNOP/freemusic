//
//  SettingsViewController.m
//  Drive
//
//  Created by EvilNOP on 15/3/11.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "SettingsViewController.h"
#import "ProductCell.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

enum {
    kSectionTagShare,
    kSectionTagMoreApps
};

@interface SettingsViewController () <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate> {
    
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet GADBannerView *_bannerView;
    
    NSArray *_moreappsInfoList;
    NSIndexPath *_selectedProductCellIndexPath;
    NSMutableArray *_sectionInfoList;
    
    UITableViewCell *_messageCell;
    UITableViewCell *_emailCell;
    UITableViewCell *_twitterCell;
    UITableViewCell *_facebookCell;
}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.title = @"More";
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    
    _moreappsInfoList = [ADManager moreAppsInfoList];
    [self initCells];
    
    _tableView.delegate = (id)self;
    _tableView.dataSource = (id)self;
    
    [self setupBannerAdIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadSettingsData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _bannerView.frame = CGRectMake(0, 0, self.view.width, 50);
    _bannerView.bottom = self.view.height;
    
    CGFloat tableViewHeight = self.view.height - (_bannerView.hidden ? 0 : _bannerView.height);
    _tableView.frame = CGRectMake(0, 0, self.view.width, tableViewHeight);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _sectionInfoList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SectionInfo *sectionInfo = _sectionInfoList[section];
    return sectionInfo.items.count;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    SectionInfo *sectionInfo = _sectionInfoList[section];
    return sectionInfo.title;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    
    if (sectionInfo.tag == kSectionTagShare) {
        return sectionInfo.items[indexPath.row];
    }
    else if (sectionInfo.tag == kSectionTagMoreApps) {
        static NSString *kIdentifier = @"I";
        ProductCell *cell = [_tableView dequeueReusableCellWithIdentifier:kIdentifier];
        if (cell == nil) {
            cell = [[ProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kIdentifier];
            cell.screenShotScrollViewDatasource = (id)self;
            [cell.downloadButton addTarget:(id)self action:@selector(productCellDownloadButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        ADMoreAppsInfo *info = _moreappsInfoList[indexPath.row];
        
        cell.titleLabel.text = info.app.name;
        cell.detailsLabel.text = info.app.details;
        cell.descriptionLabel.text = info.app.descriptions;
        
        NSString *price = info.app.price;
        [cell.downloadButton setTitle:price forState:UIControlStateNormal];
        
        NSString *iconPath = info.app.iconPath;
        cell.iconView.image = [UIImage imageWithContentsOfFile:iconPath];
        
        float rating = [info.app.rating floatValue];
        int numberOfReviews = [info.app.numberOfReviews intValue];
        if (rating == 0) rating = 5.0;
        [cell setRating:rating numberOfReviews:numberOfReviews];
        
        if ([indexPath isEqual:_selectedProductCellIndexPath]) {
            [cell expand];
        }
        else {
            [cell shrink];
        }
        return cell;
    }

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    if (sectionInfo.tag == kSectionTagShare) {
        return 44;
    }
    else if (sectionInfo.tag == kSectionTagMoreApps) {
        return [indexPath isEqual:_selectedProductCellIndexPath] ? kProductCellExpandedHeight : kProductCellShrinkedHeight;
    }

    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    
    if (sectionInfo.tag == kSectionTagShare) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        if (cell == _twitterCell) {
            NSString *content = FEStringWithFormat(@"Highly recommend a cool app which you can discover and listen free music. https://itunes.apple.com/app/%@", kAppID);
            SLComposeViewController *controller = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [controller setInitialText:content];
            [controller addImage:[UIImage imageNamed:@"iTunesArtwork"]];
            [self presentViewController:controller animated:YES completion:nil];
        }
        else {
            NSString *content = FEStringWithFormat(@"I’m using a cool app that you can discover and listen free music. Highly recommended! You can download it for free via the following link: https://itunes.apple.com/app/id%@", kAppID);
            
            if (cell == _messageCell) {
                MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
                if([MFMessageComposeViewController canSendText]) {
                    controller.body = content;
                    controller.messageComposeDelegate = self;
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }
            
            else if (cell == _emailCell) {
                MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
                if([MFMailComposeViewController canSendMail]) {
                    [controller setSubject:kAppName];
                    [controller setMessageBody:content isHTML:NO];
                    controller.mailComposeDelegate = (id)self;
                    [self presentViewController:controller animated:YES completion:nil];
                }
            }

            else if (cell == _facebookCell) {
                SLComposeViewController *controller = [SLComposeViewController
                                                       composeViewControllerForServiceType:SLServiceTypeFacebook];

                [controller addImage:[UIImage imageNamed:@"iTunesArtwork"]];
                [controller setInitialText:content];
                NSString *urlString = FEStringWithFormat(@"https://itunes.apple.com/app/id%@", kAppID);
                [controller addURL:[NSURL URLWithString:urlString]];
                [self presentViewController:controller animated:YES completion:nil];
            }

        }
    }
    else if (sectionInfo.tag == kSectionTagMoreApps) {
        if ([_selectedProductCellIndexPath isEqual:indexPath] == NO) {
            if (_selectedProductCellIndexPath == nil) {
                _selectedProductCellIndexPath = indexPath;
                [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            else {
                NSIndexPath *backup = _selectedProductCellIndexPath;
                _selectedProductCellIndexPath = indexPath;
                [_tableView reloadRowsAtIndexPaths:@[backup, _selectedProductCellIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            
            [_tableView scrollToRowAtIndexPath:_selectedProductCellIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        else { //to shrink
            _selectedProductCellIndexPath = nil;
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

#pragma mark - ProductCell screenShotScrollViewDatasource (more apps)
- (NSInteger)numberOfImagesForScreenShotScrollView:(ScreenShotScrollView *)sv
{
    NSInteger productIndex = _selectedProductCellIndexPath.row;
    ADMoreAppsInfo *info = _moreappsInfoList[productIndex];
    return info.screenshotPathList.count;
}

- (UIImage*)screenShotScrollView:(ScreenShotScrollView *)sv imageForCellAtIndex:(int)index
{
    NSInteger productIndex = _selectedProductCellIndexPath.row;
    ADMoreAppsInfo *info = _moreappsInfoList[productIndex];
    NSString *path = info.screenshotPathList[index];
    return [UIImage imageWithContentsOfFile:path];
}

#pragma mark - Handler (more apps)
- (void)productCellDownloadButtonHandler:(id)sender
{
    ProductCell *cell = [kUtil findSuperViewWithClass:[ProductCell class] subview:sender];

    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    ADMoreAppsInfo *info = _moreappsInfoList[indexPath.row];
    NSString *appID = info.app.appID;
    
    SKStoreProductViewController *c = [[SKStoreProductViewController alloc] init];
    c.delegate = (id)self;
    NSDictionary *dict = @{SKStoreProductParameterITunesItemIdentifier: appID};
    [c loadProductWithParameters:dict completionBlock:nil];
    [self presentViewController:c animated:YES completion:nil];
}

#pragma mark - SKStoreProductViewController delegate (more apps)
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate & MFMessageComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (_bannerView.hidden) {
        _bannerView.hidden = NO;
        [self.view setNeedsLayout];
    }
}

#pragma mark - Private
- (void)reloadSettingsData
{
    _sectionInfoList = [NSMutableArray array];
    
    [_sectionInfoList addObject:newSectionInfo(@"Share", @[_messageCell, _emailCell, _twitterCell, _facebookCell], kSectionTagShare)];
    if ([kParamsManager boolForName:kParamNameShowsMoreApps] && _moreappsInfoList.count > 0) {
        [_sectionInfoList addObject:newSectionInfo(@"Featured", _moreappsInfoList, kSectionTagMoreApps)];
    }
    
    [_tableView reloadData];
}

- (void)setupBannerAdIfNeeded
{
    _bannerView.hidden = YES;

    if ([kParamsManager boolForName:kParamNameShowsBannerAd]) {
        _bannerView.adUnitID = kGlobal.bannerUnitID;
        _bannerView.delegate = (id)self;
        _bannerView.rootViewController = self;
        _bannerView.autoloadEnabled = YES;
        [_bannerView loadRequest:[GADRequest request]];
    }
    else {
        _bannerView.autoloadEnabled = NO;
    }
}

- (void)initCells
{
    _messageCell = [self createCellWithTitle:@"Message"];
    _emailCell = [self createCellWithTitle:@"Email"];
    _twitterCell = [self createCellWithTitle:@"Twitter"];
    _facebookCell = [self createCellWithTitle:@"Facebook"];
}

- (UITableViewCell*)createCellWithTitle:(NSString*)title
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textLabel.text = title;
    return cell;
}

@end

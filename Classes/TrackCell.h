//
//  TrackCell.h
//  Drive
//
//  Created by EvilNOP on 15/3/6.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "ImageResizableCell.h"
#import "ProgressButton.h"

@interface TrackCell : ImageResizableCell

@property (nonatomic, strong) void (^didLayoutSubviews)(TrackCell *cell);

@property (nonatomic) BOOL hidesSelectionBox;
@property (nonatomic) BOOL isSelected;

@property (nonatomic, readonly) RhythmView *rhythmView;
@property (nonatomic) BOOL showsRhythmView;

@property (nonatomic, readonly) UIImageView *playingIconView;
@property (nonatomic) BOOL showsPlayingIcon;

- (void)setArtwork:(UIImage *)icon;

@end

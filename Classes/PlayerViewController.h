//
//  PlayerViewController.h
//  Drive
//
//  Created by EvilNOP on 15/3/15.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const PlayerViewControllerPlayingStateChangedNotification = @"PlayerViewControllerPlayingStateChangedNotification";

@interface PlayerViewController : UIViewController

@property (nonatomic, readonly) NSArray *tracks;
@property (nonatomic, readonly) Track *currentTrack;
@property (nonatomic, readonly) BOOL isPlaying;
@property (nonatomic) BOOL isShuffled;

- (void)setTracks:(NSArray*)tracks currentIndex:(NSUInteger)currentIndex;
- (void)play;
- (void)pause;
- (void)playNextTrack;
- (void)playPreviousTrack;

@end

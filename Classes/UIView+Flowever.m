//
//  UIView+IgnoreSubviewEvent.m
//  OnePass
//
//  Created by EvilNOP on 13-9-20.
//  Copyright (c) 2013年 Elite. All rights reserved.
//

#import "UIView+Flowever.h"
#import <objc/runtime.h>

@implementation UIView (Flowever)

+ (void)load
{
    Method originMethod = class_getInstanceMethod(self, @selector(hitTest:withEvent:));
    Method newMethod = class_getInstanceMethod(self, @selector(customHitTest:withEvent:));
    method_exchangeImplementations(originMethod, newMethod);
    
    Method originMethod1 = class_getInstanceMethod(self, @selector(pointInside:withEvent:));
    Method newMethod1 = class_getInstanceMethod(self, @selector(customPointInside:withEvent:));
    method_exchangeImplementations(originMethod1, newMethod1);
}

- (UIView*)customHitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *view = [self customHitTest:point withEvent:event]; //调用原始的hitTest:withEvent:
    
    if (view && self.ignoreSubviewEvent) {
        return self;
    }
    
    return view;
}

- (BOOL)customPointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    if (self.includeOutsideSubviewEvent == NO) {
        return [self customPointInside:point withEvent:event]; //调用原始的pointInside:withEvent:
    }
    else {
        if (CGRectContainsPoint(self.bounds, point)) {
            return YES;
        }
        else {
            for (UIView *subview in self.subviews) {
                if ([subview pointInside:[self convertPoint:point toView:subview] withEvent:event]) {
                    return YES;
                }
            }
        }
    }
    
    return NO;
}

- (void)setIgnoreSubviewEvent:(BOOL)ignoreSubviewEvent
{
    objc_setAssociatedObject(self, @"ignoreSubviewEvent", @(ignoreSubviewEvent), OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)ignoreSubviewEvent
{
    NSNumber *o = objc_getAssociatedObject(self, @"ignoreSubviewEvent");
    return [o boolValue];
}

- (void)setIncludeOutsideSubviewEvent:(BOOL)includeOutsideSubviewEvent
{
    objc_setAssociatedObject(self, @"includeOutsideSubviewEvent", @(includeOutsideSubviewEvent), OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)includeOutsideSubviewEvent
{
    NSNumber *o = objc_getAssociatedObject(self, @"includeOutsideSubviewEvent");
    return [o boolValue];
}

@end

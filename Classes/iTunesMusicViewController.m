//
//  iTunesMusicViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/5/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "iTunesMusicViewController.h"
#import "Track.h"
#import "FreeMusic-Swift.h"
#import "FlagButton.h"
@import MediaPlayer;

enum {
    kFlagAdd,
    kFlagAdded
};

@interface iTunesMusicViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSMutableArray *itemsName;
@property (nonatomic, strong) NSMutableArray *searchItems;
@property (nonatomic, strong) NSMutableArray *tracks;

@property (nonatomic) BOOL isSearching;

@end

@implementation iTunesMusicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [kGlobal navigationItem:self.navigationItem customTitle:@"iTunes Music"];
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    self.itemsName = [NSMutableArray array];
    self.searchItems = [NSMutableArray array];
    self.isSearching = NO;
    
    self.searchBar = [[UISearchBar alloc] init];
    self.searchBar.barStyle = UISearchBarStyleDefault;
    self.searchBar.returnKeyType = UIReturnKeySearch;
    self.searchBar.placeholder = @"Search";
    self.searchBar.delegate = self;
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = colorForKey(@"bigBackgroundColor");
    self.tableView.rowHeight = 70;
    
    [self.view addSubview:self.searchBar];
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.items = [self songItems];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.searchBar.frame = CGRectMake(0, 0, self.view.width, 44);
    self.tableView.frame = CGRectMake(0, 44, self.view.width, self.view.height - self.searchBar.height);
}

#pragma mark - Handler
- (void)addButtonHandler:(FlagButton*)sender {
    FESetUserDefaults(kUDKeyOffline, @NO);
    
    TrackCell *cell = [kUtil findSuperViewWithClass:[UITableViewCell class] subview:sender];
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    
    MPMediaItem *item;
    
    if (self.isSearching) {
        item = self.searchItems[indexPath.row];
    } else {
        item = self.items[indexPath.row];
    }
    
    NSString *identifier = [NSString stringWithFormat:@"%ld", (long)item.persistentID];
    if ([Track existedWithID:identifier] == NO) {
        
        Track *track = [kiTunesMusicSource createTrackWithItem:item];
        [track save];
        sender.flag = kFlagAdded;
    }
}

#pragma mark - TableView delegate & data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearching) {
        return self.searchItems.count;
    } else {
        return self.items.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *kIdentifier = @"C";
    TrackCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifier];
    
    if (cell == nil) {
        static UIImage *addIcon = nil;
        if (addIcon == nil) {
            addIcon = [kUtil tintImage:[UIImage imageNamed:@"Add"] withColor:kGlobal.tintColor];
        }
        
        cell = [[TrackCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kIdentifier];
        
        FlagButton *accessoryButton = [[FlagButton alloc] init];
        accessoryButton.frame = CGRectMake(0, 0, 40, _tableView.rowHeight);
        
        [accessoryButton addTarget:(id)self action:@selector(addButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
        accessoryButton.autoChangeFlag = NO;
        [accessoryButton addFlag:kFlagAdd icon:addIcon];
        [accessoryButton addFlag:kFlagAdded icon:[UIImage imageNamed:@"Added"]];
        
        cell.accessoryView = accessoryButton;
        
        [cell setDidLayoutSubviews:^(TrackCell *cell) {
            cell.accessoryView.x += 10;
        }];
    }

    MPMediaItem *item;
    
    if (self.isSearching) {
        item = self.searchItems[indexPath.row];
    } else {
        item = self.items[indexPath.row];
    }
    
    FlagButton *accessoryButton = (FlagButton*)cell.accessoryView;
    NSString *identifier = [NSString stringWithFormat:@"%ld", (long)item.persistentID];
    accessoryButton.flag = ([Track existedWithID:identifier] ? kFlagAdded : kFlagAdd);
    
    cell.textLabel.text = item.title;
    cell.detailTextLabel.text = [kGlobal formatSeconds: item.playbackDuration];
    
    cell.imageView.image = kDefaultTrackArtwork;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [item.artwork imageWithSize:CGSizeMake(50, 50)];
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            if (image && [tableView indexPathForCell:cell].row == indexPath.row) {
                cell.imageView.image = image;
            }
        });
        
    });

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.searchBar isFirstResponder]) {
        [self.searchBar resignFirstResponder];
    }
    
    NSArray *results = nil;
    if (self.isSearching) {
        results = self.searchItems;
    } else {
        results = self.items;
    }
    
    NSMutableArray *tracks = [NSMutableArray array];
    for (MPMediaItem *item in results) {
        Track *track = [kiTunesMusicSource createTrackWithItem:item];
        [tracks addObject:track];
    }
    
    [kGlobal enqueuTracks:tracks andPlayTrackAtIndex:indexPath.row shuffled:NO rootNavigationController:self.navigationController playlist:nil];
}

#pragma mark - Search bar delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
    self.isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.searchItems = [self filterItemsWithText:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    self.isSearching = NO;
    [self.tableView reloadData];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

#pragma mark - Scroll view delegate 

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.searchBar isFirstResponder]) {
        [self.searchBar resignFirstResponder];
    }
}

#pragma mark - Private

- (NSArray *)songItems {
    MPMediaQuery *query = [MPMediaQuery songsQuery];
    [query addFilterPredicate:[MPMediaPropertyPredicate predicateWithValue:[NSNumber numberWithBool:NO] forProperty:MPMediaItemPropertyIsCloudItem]];
    
    NSMutableArray *tmp = [NSMutableArray array];
    for (MPMediaItem *item in [query items]) {
        
        [tmp addObject:item];
    }
    
    return tmp;
}

- (NSMutableArray *)filterItemsWithText:(NSString *)text {
    NSString *regx = [kGlobal regxWithText:text];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF LIKE[cd] %@", regx];
    NSArray *filterNames = [self.itemsName filteredArrayUsingPredicate:predicate];
    NSMutableArray *tmpArray = [NSMutableArray array];
    
    for (MPMediaItem *item in self.items) {
        NSString *name = [NSString stringWithFormat:@"%@", item.title];
        if ([filterNames containsObject:name]) {
            [tmpArray addObject:item];
        }
    }
    return tmpArray;
}

@end

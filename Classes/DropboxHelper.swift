//
//  DropboxHelper.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/3/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit

@objc class DropboxHelper: NSObject {
    
    class func handleRedirectURL(url: NSURL) -> Bool {
        if let authResult = Dropbox.handleRedirectURL(url) {
            switch authResult {
            case .Success(let token):
                print("Success! User is logged into Dropbox with token: \(token)")
                NSUserDefaults.standardUserDefaults().setObject(token.accessToken, forKey: "dropboxToken")
            case .Error(let error, let description):
                print("Error \(error): \(description)")
            }
        }
        return false
    }
    
    class func isVerified() -> Bool {
        if let _ = Dropbox.authorizedClient {
            return true
        } else {
            return false
        }
    }
    
    class func removeAuthorizer() {
        Dropbox.unlinkClient()
    }
}

//
//  FileDetector.h
//  Coolplayer
//
//  Created by EvilNOP on 6/16/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FileDetector;

typedef void (^FileDetectorDetectingHandler) (FileDetector *detector, Boolean changed, NSDictionary *subPathToAttributes);
typedef BOOL (^FileDetectorIgnoreFilesBlock) (FileDetector *detector, NSString *subPath);

@interface FileDetector : NSObject

+ (id)detectFilesAtPath:(NSString*)path detectingHandler:(FileDetectorDetectingHandler)handler;

@property (nonatomic) NSTimeInterval detectingInterval;
@property (nonatomic) FileDetectorIgnoreFilesBlock ignoreFilesBlock;

- (void)run;
- (void)stop;

@end

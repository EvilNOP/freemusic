//
//  iTunesMusicSource.h
//  FreeMusic
//
//  Created by EvilNOP on 16/8/19.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

@interface iTunesMusicSource : FESingleton

- (Track*)createTrackWithItem:(MPMediaItem*)item;
- (MPMediaItem*)findMediaItemWithTrack:(Track*)track;

@end

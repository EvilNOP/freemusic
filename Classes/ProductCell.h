//
//  ProductCell.h
//  helo
//
//  Created by EvilNOP on 14-1-5.
//  Copyright (c) 2014年 Fire. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScreenShotScrollView.h"

//缩小状态高度
#define kProductCellShrinkedHeight 66

//展开状态Screenshot高度
#define kProductCellExpandedScreenshotScrollViewHeight 300

//展开状态descriptionLabel的高度
#define kProductCellExpandedDescriptionHeight 60

//展开状态高度
#define kProductCellExpandedHeight (kProductCellShrinkedHeight + kProductCellExpandedScreenshotScrollViewHeight + kProductCellExpandedDescriptionHeight)

typedef enum {
    kProductCellStatusShrink,
    kProductCellStatusExpand
} kProductCellStatus;

@interface ProductCell : UITableViewCell {
    UIView *_headerBackgroundView;
    CAGradientLayer *_topShadow;
}

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *detailsLabel;
@property (strong, nonatomic) UILabel *descriptionLabel;
@property (strong, nonatomic) UIButton *downloadButton;
@property (strong, nonatomic) UIImageView *ratingImageView;
@property (strong, nonatomic) ScreenShotScrollView *screenShotScrollView;
@property (strong, nonatomic) UIImageView *iconView;

@property (nonatomic, readonly) kProductCellStatus status;

- (void)shrink;
- (void)expand;

- (void)setRating:(float)rating numberOfReviews:(int)numberOfReviews;
- (void)setScreenShotScrollViewDatasource:(id<ScreenShotScrollViewDatasource>)datasource;

@end

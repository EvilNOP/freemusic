//
//  MusicListCell.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/1/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit

class MusicListCell: UITableViewCell {
    
    // TODO: 命名
    var thumbnail: UIImageView!
    var fileName: UILabel!
    var duration: UILabel!
    var stateButton: UIButton!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        thumbnail = UIImageView()
        thumbnail.contentMode = .ScaleAspectFit
        thumbnail.clipsToBounds = true
        self.contentView.addSubview(self.thumbnail)
        
        fileName = UILabel()
        fileName.lineBreakMode = .ByTruncatingMiddle
        contentView.addSubview(fileName)
        
        duration = UILabel()
        duration.font = UIFont.systemFontOfSize(12.0)
        duration.textColor = Global.sharedInstance().colorForKey("musicListCellDurationColor")
        contentView.addSubview(duration)
        
        stateButton = UIButton()
        contentView.addSubview(self.stateButton)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        thumbnail.frame = CGRectMake(15, 0, 56, 56)
        thumbnail.centerY = self.halfHeight
        
        fileName.frame = CGRectMake(thumbnail.right + 10, 15, self.width - 2 * 15 - 56 - 10 - 22 - 10, 20)
        
        duration.frame = CGRectMake(0, 0, 100, 20)
        duration.x = fileName.x
        duration.bottom = self.height - 16
        
        stateButton.frame = CGRectMake(0, 0, 22, 22)
        stateButton.right = self.width - 15
        stateButton.centerY = self.halfHeight
    }
}

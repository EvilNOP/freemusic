//
//  MoreViewController.h
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, SettingSection) {
    none,
    cloundServices,
    share
};

typedef NS_ENUM(NSUInteger, CloundServices) {
    googleDrive,
    dropbox,
    oneDrive
};

typedef NS_ENUM(NSUInteger, Share) {
    message,
    email,
    twitter,
    facebook
};

@interface MoreViewController : UITableViewController

@end

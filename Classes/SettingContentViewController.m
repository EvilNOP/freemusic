//
//  SettingContentViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/11/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "SettingContentViewController.h"

@interface SettingContentViewController ()<GADBannerViewDelegate>

@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerBottomConstraint;

@end

@implementation SettingContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [kGlobal navigationItem:self.navigationItem customTitle:@"MORE"];
    self.bannerView.backgroundColor = [UIColor clearColor];
    [self setupBannerAdIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupBannerAdIfNeeded
{
    self.bannerView.hidden = YES;
    
    if ([kParamsManager boolForName:kParamNameShowsBannerAd]) {
        self.bannerView.adUnitID = kGlobal.bannerUnitID;
        self.bannerView.delegate = self;
        self.bannerView.rootViewController = self;
        self.bannerView.autoloadEnabled = YES;
        [self.bannerView loadRequest:[GADRequest request]];
    }
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (self.self.bannerView.hidden) {
        self.bannerView.hidden = NO;
        self.bannerBottomConstraint.constant = 0;
        [self.view bringSubviewToFront:self.bannerView];
        [self.view setNeedsLayout];
    }
}



@end

//
//  ScreenShotScrollView.h
//  helo
//
//  Created by EvilNOP on 14-1-5.
//  Copyright (c) 2014年 Fire. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScreenShotScrollViewDatasource;

@interface ScreenShotScrollView : UIScrollView {
    NSMutableArray *_imageViewList;
}

@property (weak) id<ScreenShotScrollViewDatasource> datasource;

- (void)loadData;
- (void)unloadData;

@end



@protocol ScreenShotScrollViewDatasource <NSObject>

- (int)numberOfImagesForScreenShotScrollView:(ScreenShotScrollView*)sv;
- (UIImage*)screenShotScrollView:(ScreenShotScrollView*)sv imageForCellAtIndex:(int)index;

@end

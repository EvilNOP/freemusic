//
//  Playlist.m
//  Drive
//
//  Created by EvilNOP on 15/3/12.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "Playlist.h"

static NSString * const kKeyName = @"name";
static NSString * const kKeyTrackIDList = @"trackIDList";
static NSString * const kKeyOrder = @"order";

@interface Playlist () <NSCoding> {
    NSMutableArray *_trackIDList;
}

@end

@implementation Playlist

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [super encodeWithCoder:aCoder];
    if (_name) [aCoder encodeObject:_name forKey:kKeyName];
    if (_trackIDList) [aCoder encodeObject:_trackIDList forKey:kKeyTrackIDList];
    [aCoder encodeObject:@(_order) forKey:kKeyOrder];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _name = [aDecoder decodeObjectForKey:kKeyName];
        _trackIDList = [aDecoder decodeObjectForKey:kKeyTrackIDList];
        _order = [[aDecoder decodeObjectForKey:kKeyOrder] longValue];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        _trackIDList = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Overwrite
- (void)willSave
{
    if (self.ID == nil) {
        _order = [self.class maxOrder] + 1;
    }
}

#pragma mark - Private
- (BOOL)containsTrackWithID:(NSString*)trackID
{
    return [_trackIDList containsObject:trackID];
}

#pragma mark - Public
- (int)numberOfTracks
{
    return _trackIDList.count;
}

- (void)addTrack:(Track *)track
{
    if (track.ID && [self containsTrackWithID:track.ID] == NO) {
        [_trackIDList addObject:track.ID];
        [self save];
    }
}

- (void)addTracks:(NSArray *)tracks
{
    int count = [_trackIDList count];
    for (Track *track in tracks) {
        if (track.ID && [self containsTrackWithID:track.ID] == NO) {
            [_trackIDList addObject:track.ID];
        }
    }
    if (count != _trackIDList.count) {
        [self save];
    }
}

- (void)removeTrackWithID:(NSString *)trackID
{
    int index = [_trackIDList indexOfObject:trackID];
    if (index != NSNotFound) {
        [_trackIDList removeObjectAtIndex:index];
        [self save];
    }
}

- (void)removeTracksWithIDs:(NSArray *)trackIDs
{
    NSUInteger count = [_trackIDList count];
    for (NSString *trackID in trackIDs) {
        NSUInteger index = [_trackIDList indexOfObject:trackID];
        if (index != NSNotFound) {
            [_trackIDList removeObjectAtIndex:index];
        }
    }
    if (count != _trackIDList.count) {
        [self save];
    }
}

- (NSArray*)tracks
{
    NSMutableArray *tracks = [NSMutableArray array];
    NSUInteger count = [_trackIDList count];
    for (int i=(int)_trackIDList.count-1; i>=0; i--) {
        NSString *trackID = _trackIDList[i];
        Track *track = [Track findByID:trackID];
        if (track) {
            [tracks insertObject:track atIndex:0];
        }
        else {
            [_trackIDList removeObjectAtIndex:i];
        }
    }
    
    if (count != _trackIDList.count) {
        [self save];
    }
    
    return tracks;
}

- (Track*)firstTrack
{
    NSString *trackID = [_trackIDList firstObject];
    return [Track findByID:trackID];
}

- (Track*)lastTrack
{
    NSString *trackID = [_trackIDList lastObject];
    return [Track findByID:trackID];
}


#pragma mark - Private class method
+ (NSInteger)maxOrder
{
    NSInteger maxOrder = 0;
    for (Playlist *playlist in [self allRecords]) {
        if (playlist.order > maxOrder) {
            maxOrder = playlist.order;
        }
    }
    return maxOrder;
}

@end

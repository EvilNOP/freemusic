//
//  Model.m
//  Drive
//
//  Created by EvilNOP on 15/3/12.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "Model.h"

static NSString * const kKeyID = @"#id";
static NSString * const kKeyCreationDate = @"#creation_date";
static NSString * const kKeyModificationDate = @"#modification_date";

static NSMutableDictionary *caches;
static NSString *databasePath;

@implementation Model

+ (void)load
{
    caches = [NSMutableDictionary dictionary];
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    if (self.ID) [aCoder encodeObject:self.ID forKey:kKeyID];
    if (self.creationDate) [aCoder encodeObject:self.creationDate forKey:kKeyCreationDate];
    if (self.modificationDate) [aCoder encodeObject:self.modificationDate forKey:kKeyModificationDate];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    _ID = [aDecoder decodeObjectForKey:kKeyID];
    _creationDate = [aDecoder decodeObjectForKey:kKeyCreationDate];
    _modificationDate = [aDecoder decodeObjectForKey:kKeyModificationDate];
    return self;
}

#pragma mark - Private
- (NSString*)modelName
{
    return [self.class modelName];
}

#pragma mark - Hook
- (void)willSave
{
    
}

- (void)didSave
{
    
}

- (void)willDelete
{
    
}

- (void)didDelete
{
    
}

#pragma mark - Public
- (void)save
{
    if (databasePath == nil) {
        [NSException raise:@"ModelInitializationError" format:@"先调用 + [Model setPersistantStorePath:]方法设置数据库路径"];
    }
    
    [self willSave];
    
    if (_ID == nil) _ID = [kGlobal generateRandomID];
    if (_modificationDate == nil) _modificationDate = [NSDate new];
    if (_creationDate == nil) _creationDate = [NSDate new];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    NSString *dataFilePath = [self.class dataFilePathForRecordWithID:self.ID];
    [kFM removeItemAtPath:dataFilePath error:nil];
    
    BOOL saved = NO;
    if ([data writeToFile:dataFilePath atomically:YES] == NO) {
        
        NSString *dataFileRoot = [self.class dataFileRoot];
        if ([kFM fileExistsAtPath:dataFileRoot] == NO) {
            [kFM createDirectoryAtPath:dataFileRoot withIntermediateDirectories:YES attributes:nil error:nil];
            saved = [data writeToFile:dataFilePath atomically:YES];
        }
    }
    else {
        saved = YES;
    }
    
    if (saved) {
        [self.class cacheRecord:self];
        [self didSave];
    }
}

- (void)delete
{
    [self willDelete];
    
    NSString *path = [self.class dataFilePathForRecordWithID:_ID];
    [kFM removeItemAtPath:path error:nil];
    [self.class deleteRecordFromCaches:self];
    
    [self didDelete];
}

#pragma mark - Private class method
+ (NSString*)dataFileRoot
{
    return [kUtil join:databasePath, [self modelName], nil];
}

+ (NSString*)dataFilePathForRecordWithID:(NSString*)ID
{
    if (ID == nil) {
        return nil;
    }
    return [kUtil join:[self dataFileRoot], ID, nil];
}

+ (NSString*)modelName
{
    return [self description];
}

+ (id)findCachedRecordByID:(NSString*)ID
{
    id record = nil;
    NSString *modelName = [self modelName];
    if (caches[modelName]) {
         record = caches[modelName][ID];
    }
    return record;
}

+ (void)cacheRecord:(Model*)record
{
    if (record.ID == nil) {
        return;
    }
    
    NSString *modelName = [self modelName];
    if (caches[modelName] == nil) {
        caches[modelName] = [NSMutableDictionary dictionary];
    }
    caches[modelName][record.ID] = record;
}

+ (void)deleteRecordFromCaches:(Model*)record
{
    if (record.ID == nil) {
        return;
    }
    
    NSString *modelName = [self modelName];
    if (caches[modelName]) {
        [caches[modelName] removeObjectForKey:record.ID];
    }
}

#pragma mark - Public class method
+ (void)setPersistantStorePath:(NSString *)path
{
    databasePath = [path copy];
    [kFM createDirectoryAtPath:databasePath withIntermediateDirectories:YES attributes:nil error:nil];
}

+ (id)findByID:(NSString*)ID
{
    if (ID == nil) {
        return nil;
    }
    
    id record = [self findCachedRecordByID:ID];
    
    if (record == nil) {
        NSString *path = [self dataFilePathForRecordWithID:ID];
        record = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        [self cacheRecord:record];
    }
    
    return record;
}

+ (NSArray*)allRecords
{
    NSArray *filenameList = [kFM contentsOfDirectoryAtPath:[self dataFileRoot] error:nil];
    NSMutableArray *records = [NSMutableArray array];
    for (NSString *ID in filenameList) {
        id record = [self findByID:ID];
        if (record) {
            [records addObject:record];
        }
    }
    
    return records;
}

+ (BOOL)existedWithID:(NSString*)ID
{
    if (ID == nil) {
        return NO;
    }
    NSString *path = [self dataFilePathForRecordWithID:ID];
    return [kFM fileExistsAtPath:path];
}

@end

//
//  DropboxListView.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/3/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import MJRefresh
import Underscore_m

class DropboxListView: UIView {
    
    var parentsViewController: UIViewController!
    var cellSelectedHanlder: ((index: NSInteger, isFolder: Bool, file: [String: AnyObject]) -> Void)?
    var cancelSignInHandler: (() -> Void)?
    
    private let tableView = UITableView(frame: CGRectZero, style: .Plain)
    private let searchBar = UISearchBar()
    private var noLoginView: NoLoginView?
    private let noSearchingView = NoSearchingView()
    
    private let folder = "folder"
    private let file = "file"
    private var isSearchedViewController = false
    private var nextPage: (hasMore: Bool, cursor: String) = (false, "")
    private let searchURL = "https://api.dropboxapi.com/2/files/search"
    private let nextPageURL = "https://api.dropboxapi.com/2/files/list_folder/continue"
    private let searchPageSize:UInt64 = 1000    // 最大只能1000
    private var searchNextPageInfo: (more: Bool, start: Int) = (false, 0)
    
    enum ButtonFlag: Int32 {
        case Add, Added
    }
    
    private var items: [ [String: AnyObject] ] = [] {
        didSet {
            print(items.count)
            tableView.reloadData()
        }
    }
    
    private var searchItems: [ [String: AnyObject] ]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    func isVerified() -> Bool {
        if let _ = Dropbox.authorizedClient {
            return true
        } else {
            return false
        }
    }
    
    func verify() {
        guard parentsViewController != nil else { return }
        Dropbox.authorizeFromController(
            parentsViewController,
            completion: {
                if !self.isSearchedViewController {
                    self.fetchFile("")
                } else {
                    self.tableView.tableFooterView = self.noSearchingView
                }
            },
            cancel: {
                if let handler = self.cancelSignInHandler {
                    handler()
                }
        })
    }
    
    class func remoeAuthorizer() {
        Dropbox.unlinkClient()
    }
    
    func fetchFile(path: String) {
        MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)
        if let client = Dropbox.authorizedClient {
            client.files.listFolder(path: path, recursive: false, includeMediaInfo: true).customResponse({ (response, error) in
                if let data = response {
                    if let dic = self.convertDataToDictionary(data) {
                        self.items = dic["entries"] as! [ [String: AnyObject] ]

                        self.items = self.items.filter { (item) -> Bool in
                            if self.isFolder(item) {
                                return true
                            }
                            return Global.sharedInstance().isSongFileWithName(item["name"] as! String)
                            
                        }
                        self.nextPage.hasMore = dic["has_more"] as! Bool
                        self.nextPage.cursor = dic["cursor"] as! String
                        
                        self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(self.queryNextPage))
                        self.tableView.mj_footer.hidden = !self.nextPage.hasMore
                        
                        MBProgressHUD.hideHUDForView(self.tableView, animated: true)
                    }
                }
            })
        } else {
            showAlert("", message: "Please Sign In To Assess Files")
        }
    }
    
    convenience init(controller: UIViewController, searching: Bool) {
        self.init()
        parentsViewController = controller
        isSearchedViewController = searching
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.rowHeight = 70
        
        searchBar.barStyle = .Default
        searchBar.returnKeyType = .Search
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
        noLoginView = NoLoginView(loginHandler: {
            self.verify()
        })
        
        if searching {
            if isVerified() {
                tableView.tableFooterView = noSearchingView
                tableView.scrollEnabled = false
            } else {
                tableView.tableFooterView = noLoginView
                tableView.scrollEnabled = false
            }
        } else {
            tableView.tableFooterView = UIView()
            tableView.scrollEnabled = true
            if isVerified() {
            }
        }
        
        self.addSubview(searchBar)
        self.addSubview(tableView)
    }
    
    init () {
        super.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        searchBar.frame = CGRectMake(0, 0, self.width, 44)
        tableView.frame = CGRectMake(0, searchBar.height, self.width, self.height - searchBar.height)
        
        noLoginView!.frame = CGRectMake(0, 0, self.width, self.height - searchBar.height)
        noSearchingView.frame = CGRectMake(0, 0, self.width, self.height - searchBar.height)
    }
}

extension DropboxListView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showsSearchItems() {
            return searchItems!.count
        } else {
            return items.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier: String = "Cell"
        
        var optionalCell = tableView.dequeueReusableCellWithIdentifier(identifier) as? TrackCell
        if optionalCell == nil {
            optionalCell = TrackCell(style: .Default, reuseIdentifier: identifier)
            
            let accessoryButton = FlagButton()
            accessoryButton.autoChangeFlag = false
            accessoryButton.size = CGSize(width: 40, height: self.tableView.rowHeight)
            accessoryButton.addFlag(ButtonFlag.Add.rawValue, icon: UIImage(named: "Add"))
            accessoryButton.addFlag(ButtonFlag.Added.rawValue, icon: UIImage(named: "Added"))
            accessoryButton.addTarget(self, action: #selector(addButtonTapped(_:)), forControlEvents: .TouchUpInside)
            
            optionalCell?.accessoryView = accessoryButton
        }
        
        let cell = optionalCell!
        
        let dic: [String: AnyObject]
        
        if showsSearchItems() {
            dic = searchItems![indexPath.row]
        } else {
            dic = items[indexPath.row]
        }
        
        if isFolder(dic) {
            cell.accessoryType = .DisclosureIndicator
            cell.imageView?.image = UIImage(named: "File")
            cell.accessoryView?.hidden = true
        } else {
            cell.accessoryType = .None
            cell.imageView?.image = UIImage(named: "Music")
            
            let accessoryButton = cell.accessoryView as! FlagButton
            accessoryButton.hidden = false
            if let id = dic["id"] as? String {
                accessoryButton.flag = (Track.existedWithID(id) ? ButtonFlag.Added.rawValue : ButtonFlag.Add.rawValue)
            }
        }
        
        let name = dic["name"] as? String
        cell.textLabel?.attributedText = NSAttributedString(string: (name ?? ""), attributes: Global.sharedInstance().thirdFontAttribute() as? [String: AnyObject])
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let dict: [String: AnyObject]
        
        if showsSearchItems() {
            dict = searchItems![indexPath.row]
        } else {
            dict = items[indexPath.row]
        }
        
        if isFolder(dict) {
            if isSearchedViewController == false {
                if let handler = cellSelectedHanlder {
                    handler(index: indexPath.row, isFolder: isFolder(dict), file: dict)
                }
            }
        }
        else {
            var tracks = [Track]()
            var index = 0
            for item in (self.searchItems ?? self.items) {
                if (self.isFolder(item)) {
                    continue
                }
                
                let track = kDropboxSource.createTrackWithItem(item)
                tracks.append(track)
                
                if (item["id"] as? String) == (dict["id"] as? String) {
                    index = tracks.count - 1
                }
            }
            
            kGlobal.enqueuTracks(tracks, andPlayTrackAtIndex: index, shuffled: false, rootNavigationController: self.parentsViewController.navigationController, playlist: nil)
        }
    }
}

// MARK: - Searchbar delegate
extension DropboxListView: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        if isVerified() {
            return true
        }
        
        self.tableView.tableFooterView = self.noLoginView
        showAlert("", message: "Please login")
        return false
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        
        tableView.tableFooterView = UIView()
        tableView.scrollEnabled = true
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if isSearchedViewController == false {
            if searchBar.text != "" {
                let queryText = searchBar.text!
                if isVerified() {
                    NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(querylocalItemWithText), object: queryText)
                    performSelector(#selector(querylocalItemWithText), withObject: queryText, afterDelay: 0.5)
                }
            }
        } else if searchBar.text == "" {
            self.searchItems = []
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if searchBar.text != "" {
            let queryText = searchBar.text!
            if isVerified() {
                if isSearchedViewController {
                    searchFile(queryText)
                } else {
                    querylocalItemWithText(queryText)
                }
            }
        }
        
        kGlobal.increaseSearchTimesAndProcess()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchItems = nil
        
        if isSearchedViewController {
            tableView.tableFooterView = noSearchingView
            tableView.scrollEnabled = false
        } else {
            tableView.tableFooterView = UIView()
            tableView.scrollEnabled = true
        }
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        
        if let files = searchItems where files.count == 0 {
            tableView.tableFooterView = noSearchingView
            tableView.scrollEnabled = false
        }
    }
}

extension DropboxListView {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.searchBar.isFirstResponder() {
            self.searchBar.resignFirstResponder()
        }
    }
}

extension DropboxListView {
    @objc private func addButtonTapped(sender: FlagButton) {
        let cell = kUtil.findSuperViewWithClass(TrackCell.self, subview: sender) as! TrackCell
        let indexPath = self.tableView.indexPathForCell(cell)
        let file = (self.searchItems ?? self.items)[indexPath!.row]
        
        if let id = file["id"] as? String {
            if Track.existedWithID(id) == false {
                let track = kDropboxSource.createTrackWithItem(file)
                track.save()
                sender.flag = ButtonFlag.Added.rawValue
            }
        }
    }
}

// MARK: - Util
extension DropboxListView {
    
    func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .Alert
        )
        
        let ok = UIAlertAction(
            title: "OK",
            style: .Default,
            handler: nil
        )
        
        alert.addAction(ok)
        parentsViewController.presentViewController(alert, animated: true, completion: nil)
    }
    
    func convertDataToDictionary(data: NSData?) -> [String: AnyObject]? {
        if let data = data {
            do {
                return try NSJSONSerialization.JSONObjectWithData(data, options: []) as? [String: AnyObject]
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func isFolder(dic: [String: AnyObject]) -> Bool {
        let tag = dic[".tag"] as? String
        return tag == folder
    }
    
    
    
    func searchFile(query: String) {
        MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)
        if let client = Dropbox.authorizedClient {
            client.files.search(path: "", query: query, start: 0, maxResults: self.searchPageSize, mode: .Filename).customResponse { (response, error) in
                MBProgressHUD.hideHUDForView(self.tableView, animated: true)
                if let data = response {
                    if let dic = self.convertDataToDictionary(data) {
                        if let error  = dic["error"] {
                            print(error)
                        } else {
                            self.operatorAfterSearching(dic)
                        }
                    }
                }
            }
        }
    }
    
    func querylocalItemWithText(queryText: String) {
        
        if self.tableView.mj_footer != nil {
            self.tableView.mj_footer.hidden = true
        }
        
        if items.count != 0 {
            let regx = Global.sharedInstance().regxWithText(queryText)
            let predicate = NSPredicate(format: "SELF LIKE[cd] %@", regx)
            searchItems = items.filter {
                let name = ($0)["name"] as! String
                return predicate.evaluateWithObject(name)
            }
        }
    }
    
    func queryNextPage() {
        if self.nextPage.hasMore && self.nextPage.cursor != "" {
            if let client = Dropbox.authorizedClient {
                client.files.listFolderContinue(cursor: self.nextPage.cursor).customResponse({ (response, error) in
                    if error == nil {
                        if let data = response, dic = self.convertDataToDictionary(data) {
                            let tmp = dic["entries"] as! [String: AnyObject]
                            if tmp.isEmpty == false {
                                self.items.append(tmp)
                            }
                            self.nextPage.hasMore = dic["has_more"] as! Bool
                            self.nextPage.cursor = dic["cursor"] as! String
                            
                            if self.tableView.mj_footer != nil && self.tableView.mj_footer.isRefreshing() {
                                self.tableView.mj_footer.endRefreshing()
                            }
                        }
                    }
                })
            }
        } else {
            if self.tableView.mj_footer != nil && self.tableView.mj_footer.isRefreshing() {
                self.tableView.mj_footer.hidden = true
            }
        }
    }
    
    func searchNextPage() {
        if self.searchNextPageInfo.more {
            if let client = Dropbox.authorizedClient where self.searchBar.text != nil {
                client.files.search(path: "", query: self.searchBar.text!, start: UInt64(self.searchNextPageInfo.start), maxResults: self.searchPageSize, mode: .Filename).customResponse({ (response, error) in
                    if let data = response {
                        if let dic = self.convertDataToDictionary(data) {
                            if let error  = dic["error"] {
                                print(error)
                            } else {
                                self.operatorAfterSearching(dic)
                            }
                        }
                    }
                })
            }
        }
    }

    func operatorAfterSearching(dic: [String: AnyObject]) {
        print("dic: \(dic)")
        if let matches = dic["matches"] as? [[String: AnyObject]]  {
            self.searchItems = []
            
            for match in matches {
                if let metadata = match["metadata"] as? [String: AnyObject] {
                    self.searchItems?.append(metadata)
                }
            }
            
            let tmp = self.searchItems?.filter { item -> Bool in
                if self.isFolder(item) {
                    return false
                }
                return Global.sharedInstance().isSongFileWithName(item["name"] as? String)
            }
            
            
            self.searchItems = tmp!.sort { item1, _ -> Bool in
                return self.isFolder(item1)
            }
        }
        if let start = dic["start"] as? Int {
            self.searchNextPageInfo.start = start
        }
        if let more = dic["more"] as? Bool {
            self.searchNextPageInfo.more = more
        }
    }
    
    func showsSearchItems() -> Bool {
        return self.searchItems != nil
    }
}

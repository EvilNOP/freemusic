//
//  DriveFileFetcher.h
//  FreeMusic
//
//  Created by EvilNOP on 16/8/11.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const DriveFileFetcherNumberOfFetchingFilesChangedNotification = @"DriveFileFetcherNumberOfFetchingFilesChangedNotification";
static NSString * const DriveFileFetcherFileFetchingProgressUpdatedNotification = @"DriveFileFetcherFileFetchingProgressUpdatedNotification";

@interface DriveFileFetcher : FESingleton

- (void)prepareToFetchFileWithKey:(NSString*)key;

- (void)startFetchingFileWithRequest:(NSURLRequest*)request forKey:(NSString*)key targetPath:(NSString*)targetPath;
- (void)startFetchingFileWithRequest:(NSURLRequest*)request forKey:(NSString*)key targetPath:(NSString*)targetPath totalSize:(NSInteger)totalSize; // Google drive由于progress.totalUnitCount值为-1，暂时通过传入totalSize解决

- (NSDictionary*)fetchingInfoForKey:(NSString*)key;
- (void)cancelFetchingFileWithKey:(NSString*)key;

@end

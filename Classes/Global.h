//
//  Global.h
//  Drive
//
//  Created by EvilNOP on 15/3/5.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#define kTargetFreeMusicNotUsed 1
#define kTargetiMusic2 2
#define kTargetCloudMusic 3
#define kTargetFreeMusic 4
#define kTargetFreeMusic2 5
#define kTargetFreeMusic3 6
#define kTargetFreeMusic5 7

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "UIView+Flowever.h"
#import "NSDictionary+NestedKey.h"

#import "GTLDrive.h"
#import "GTMOAuth2ViewControllerTouch.h"

#import <FECommon/FECommon.h>
#import "OnlineParamsManager.h"
#import "AFNetworking.h"
#import "SectionInfo.h"
#import "Model.h"
#import "RhythmView.h"
#import "TrackTableView.h"
#import "TrackCell.h"
#import "RootController.h"
#import "Track.h"
#import "Playlist.h"
#import "PlayerViewController.h"
#import "SongsViewController.h"
#import "PlayerUserInfo.h"
#import "MobClick.h"
#import "MobClick+Flowever.h"
#import <GoogleMobileAds/GADInterstitial.h>
#import "OnlineDataManager.h"
#import "MPNowPlayingInfoCenter+Flowever.h"
#import "MPRemoteCommand+Flowever.h"
#import "WiFiTransfer.h"

@import MBProgressHUD;
@import SDWebImage;

#import "iTunesMusicSource.h"
#import "DriveFileFetcher.h"

#define kGlobal ((Global*)[Global sharedInstance])
#define kUtil ((FEUtil*)[FEUtil sharedInstance])
#define kParamsManager ((OnlineParamsManager*)[OnlineParamsManager sharedInstance])

#define kGoogleDriveSource [GoogleDriveSource sharedInstance]
#define kDropboxSource [DropboxSource sharedInstance]
#define kOneDriveSource [OneDriveSource sharedInstance]
#define kiTunesMusicSource ((iTunesMusicSource*)[iTunesMusicSource sharedInstance])

#define kDriveFileFetcher ((DriveFileFetcher*)[DriveFileFetcher sharedInstance])

#define kWiFiTransfer ((WiFiTransfer*)[WiFiTransfer sharedInstance])

#define _(key) key
#define avoidNULL(obj) ([obj isKindOfClass:[NSNull class]] ? nil : obj)
#define colorForKey(key) [kGlobal colorForKey:key]

#define kBottomSpace4HiddenBanner -50
#define kBottomSpace4VisibleBanner 0

#define kDefaultTrackArtwork [UIImage imageNamed:@"Music"]
#define kDefaultLargeTrackArtwork [UIImage imageNamed:@"LargeTrackArtwork"]
#define kDefaultPlaylistArtwork [UIImage imageNamed:@"File"]

#define kWiFiPort 9900

//constant
#if TARGET == kTargetFreeMusicNotUsed
static NSString * const kAppID = @"";
static NSString * const kAppName = @"";
static NSString * const kUmengAppKey = @"";
static NSString * const kDefaultBannerUnitID = @"";
static NSString * const kDefaultInterstitialUnitID = @"";

#elif TARGET == kTargetiMusic2
static NSString * const kAppID = @"988641649";
static NSString * const kAppName = @"Free Music";
static NSString * const kUmengAppKey = @"55dd3b0967e58e49d10007d7";
static NSString * const kDefaultBannerUnitID = @"ca-app-pub-4831730066138689/8053426052";
static NSString * const kDefaultInterstitialUnitID = @"ca-app-pub-4831730066138689/9530159256";
static NSString * const kParamsAppKey = @"FreeMusic4";
static NSString * const kOnlineDataAppKey = @"FreeMusic4";
// Drive SDK
static NSString * const kGoogleDriveKeychainItemName = @"Drive API";
static NSString * const kGoogleDriveClientID = @"219013598368-lqu2r687j332128mivbupi97l64fd53k.apps.googleusercontent.com";
static NSString * const kDropboxAppKey = @"jc2utrjiqxmt6iw";
static NSString * const kOneDriveAppKey = @"df540862-9175-4366-96b5-fb9f149e2059";

#elif TARGET == kTargetCloudMusic
static NSString * const kAppID = @"1033741753";
static NSString * const kAppName = @"Cloud Music";
static NSString * const kUmengAppKey = @"55dd3b0967e58e49d10007d7";
static NSString * const kDefaultBannerUnitID = @"ca-app-pub-1581772579654590/8969735269";
static NSString * const kDefaultInterstitialUnitID = @"ca-app-pub-1581772579654590/1446468469";
static NSString * const kParamsAppKey = @"FreeMusic";
static NSString * const kOnlineDataAppKey = @"FreeMusic";

#elif TARGET == kTargetFreeMusic
static NSString * const kAppID = @"1133819301";
static NSString * const kAppName = @"Free Music";
static NSString * const kUmengAppKey = @"57861490e0f55a08830020d6";
static NSString * const kDefaultBannerUnitID = @"ca-app-pub-1581772579654590/5989055265";
static NSString * const kDefaultInterstitialUnitID = @"ca-app-pub-1581772579654590/3235895263";
static NSString * const kParamsAppKey = @"FreeMusic";
static NSString * const kOnlineDataAppKey = @"FreeMusic";

// Drive SDK
static NSString * const kGoogleDriveKeychainItemName = @"Drive API";
static NSString * const kGoogleDriveClientID = @"219013598368-18qi6fojamie5lcd7lv4velmi5jp6l6p.apps.googleusercontent.com";
static NSString * const kDropboxAppKey = @"ju56a2twnrfdq65";
static NSString * const kOneDriveAppKey = @"c51e6007-5637-496a-be60-47ce3af16790";

#elif TARGET == kTargetFreeMusic2
static NSString * const kAppID = @"1138170664";
static NSString * const kAppName = @"Free Music";
static NSString * const kUmengAppKey = @"57987bde67e58ee81c001381";
static NSString * const kDefaultBannerUnitID = @"ca-app-pub-1581772579654590/1491730061";
static NSString * const kDefaultInterstitialUnitID = @"ca-app-pub-1581772579654590/2968463267";
static NSString * const kParamsAppKey = @"FreeMusic2";
static NSString * const kOnlineDataAppKey = @"FreeMusic2";

// Drive SDK
static NSString * const kGoogleDriveKeychainItemName = @"Drive API";
static NSString * const kGoogleDriveClientID = @"219013598368-i5r6nsp2nrt2ptul9tcguus2l5498bvi.apps.googleusercontent.com";
static NSString * const kDropboxAppKey = @"bgavdfthf4ig9zq";
static NSString * const kOneDriveAppKey = @"b0855e04-48ca-4143-9b0a-955a74ac7ad4";

#elif TARGET == kTargetFreeMusic3
static NSString * const kAppID = @"1142057477";
static NSString * const kAppName = @"Free Music";
static NSString * const kUmengAppKey = @"57aa904767e58edebe000deb";
static NSString * const kDefaultBannerUnitID = @"ca-app-pub-1581772579654590/7961545667";
static NSString * const kDefaultInterstitialUnitID = @"ca-app-pub-1581772579654590/9438278865";
static NSString * const kParamsAppKey = @"FreeMusic3";
static NSString * const kOnlineDataAppKey = @"FreeMusic3";

#elif TARGET == kTargetFreeMusic5
static NSString * const kAppID = @"1157858872";
static NSString * const kAppName = @"Free Music";
static NSString * const kUmengAppKey = @"57e4d86267e58e6f490005ac";
static NSString * const kDefaultBannerUnitID = @"ca-app-pub-4831730066138689/5715834459";
static NSString * const kDefaultInterstitialUnitID = @"ca-app-pub-4831730066138689/7192567651";
static NSString * const kParamsAppKey = @"FreeMusic5";
static NSString * const kOnlineDataAppKey = @"FreeMusic5";

// Drive SDK
static NSString * const kGoogleDriveKeychainItemName = @"Drive API";
static NSString * const kGoogleDriveClientID = @"833291824268-k1d6r8089adt6cpofvcrkits9rrdi0md.apps.googleusercontent.com";
static NSString * const kDropboxAppKey = @"c2y7yahi5fcy5iz";
static NSString * const kOneDriveAppKey = @"b0855e04-48ca-4143-9b0a-955a74ac7ad4";

#endif

//user defaults key
static NSString * const kUDKeyRepeatMode = @"1";
static NSString * const kUDKeyOffline = @"2";
static NSString * const kUDKeyFLOWableSwitchOn = @"3";
static NSString * const kUDKeyFLOWMode = @"4";
static NSString * const kUDKeyHasRate = @"5";
static NSString * const kUDKeyHasAccessAdRestrictingFLOWMode = @"5_1"; // 是否已经访问过 “限制FLOW模式的自家广告”
static NSString * const kUDKeyHasAccessAdRestrictingPlaylist = @"5_2"; // 是否已经访问过 “限制播放列表的自家广告”
static NSString * const kUDKeyPlayTimes = @"6";
static NSString * const kUDKeySearchTimes = @"7";
static NSString * const kUDKeyLoginTimes = @"8";
static NSString * const kUDKeyDateOfShowingImageAdLastTime = @"9";

//notification
static NSString * const kFLOWModeChangedNotification = @"kFLOWModeChangedNotification";
static NSString * const kForceFLOWableSwitchToOffNotification = @"kForceFLOWableSwitchToOff";
static NSString * const kParamsManagerDidUpdateParamsNotification = @"kParamsManagerDidUpdateParamsNotification";

// online params key
static NSString * const kParamNameFLOWMode = @"uma";
static NSString * const kParamNameClientIDs = @"umb";
static NSString * const kParamNameShowsBannerAd = @"umc";
static NSString * const kParamNameForceToSetFLOWMode = @"umd";
static NSString * const kParamNamePlaylistLimit = @"ume";
static NSString * const kParamNamePlayTimesToShowInterstitial = @"umf";
static NSString * const kParamNameSearchTimesToShowInterstitial = @"umg";
static NSString * const kParamNameLoginIntervalToShowAlertAd = @"umh";
static NSString * const kParamNameShowsMoreApps = @"umj";
static NSString * const kParamNameLoginIntervalToShowImageAd = @"umk";
static NSString * const kParamNameLoginIntervalToShowInterstitial = @"um_1";

static NSString * const kParamNameFLOWLimitType = @"umd_1";

static NSString * const kParamNamePlaylistLimitType = @"ume_1";
static NSString * const kParamNamePlaylistRateAlertTitle = @"ume_2";
static NSString * const kParamNamePlaylistRateAlertMessage = @"ume_3";
static NSString * const kParamNamePlaylistRateAlertCancelText = @"ume_4";
static NSString * const kParamNamePlaylistRateAlertConfirmText = @"ume_5";

static NSString * const kParamNameShowsRating = @"a_0";
static NSString * const kParamNameRatingTitle = @"a_1";
static NSString * const kParamNameRatingMessage = @"a_2";
static NSString * const kParamNameRatingCancelTitle = @"a_3";
static NSString * const kParamNameRateingConfirmTitle = @"a_4";

static NSString * const kParamNameAvailableDrives = @"e_1";

static NSString * const kParamNameBannerUnitID = @"umb_1";
static NSString * const kParamNameInterstitialUnitID = @"umb_2";

static NSString * const sourceFolder = @".data";

//enum
typedef enum {
    FLOWModeLimited = 0,
    FLOWModeRegular = 1,
    FLOWModeAllFLOWable = 2
} FLOWMode;

typedef enum {
    FLOWLimitTypeNone = 0,
    FLOWLimitTypeRate = 1,
    FLOWLimitTypeAd = 2
} FLOWLimitType;

typedef enum {
    PlaylistLimitTypeRate = 0,
    PlaylistLimitTypeAd
} PlaylistLimitType;

@interface Global : FESingleton

@property (nonatomic, readonly) UIColor *tintColor;
@property (nonatomic, readonly) NSString *cachedIconRootPath;
@property (nonatomic, readonly) NSString *cachedAudioRootPath;
@property (nonatomic, readonly) NSString *audioResumeDataRootPath;
@property (nonatomic, readonly) PlayerViewController *playerViewController;
@property (nonatomic, readonly) PlayerUserInfo *playerUserInfo;
@property (nonatomic, readonly) NSString *clientID;
@property (nonatomic, readonly) NSString *bannerUnitID;
@property (nonatomic, readonly) NSString *interstitialUnitID;
@property (nonatomic, strong) Playlist *currentPlayingPlaylist;

- (NSString*)generateRandomID;
- (RootController*)rootViewController;
- (UIBarButtonItem*)createPlayerBarButtonItem;
- (void)setAllTracksUnselected;
- (void)setAllPlaylistUnselected;
- (NSString*)formatSeconds:(NSTimeInterval)seconds;

- (BOOL)canOpenFLOWableSwitch;
- (void)increasePlayTimesAndProcess;
- (void)increaseSearchTimesAndProcess;
- (void)increaseLoginTimesAndProcess;
- (void)showInterstitial;
- (void)showImageAd;
- (void)showAlertAdWithResultHandler:(void (^)(BOOL confirmed))resultHandler;
- (void)pickClientID;

- (void)tintImageForView:(id)aView withColor:(UIColor*)color;
- (UIColor*)colorForKey:(NSString*)key;
- (UIViewController*)topViewController;

- (void)navigationItem:(UINavigationItem *)navigationItem customTitle:(NSString *)title;
- (void)navigationBar:(UINavigationBar *)navigationBar navigationItem:(UINavigationItem *)navigationItem backIndicatorWithImageName:(NSString *)name;

- (NSDictionary *)firstFontAttribute;
- (NSDictionary *)secondFontAttribute;
- (NSDictionary *)thirdFontAttribute;
- (NSDictionary *)fourthFontAttribute;
- (NSDictionary *)fifthFontAttribute;
- (NSDictionary *)navigationBarItemAttribute;

- (NSString *)regxWithText:(NSString *)text;
- (BOOL)isSongFileWithName:(NSString *)name;
- (void)setupDataPaths;
- (void)initPlayer;
- (UIBarButtonItem *)barButtonItemImageWithTitle:(NSString *)title selector:(SEL)selector;

- (void)fetchStreamRequestForTrack:(Track*)track completionHandler:(void (^)(NSURLRequest *request))completionHandler;
- (void)enqueuTracks:(NSArray*)tracks andPlayTrackAtIndex:(NSInteger)index shuffled:(BOOL)shuffled rootNavigationController:(UINavigationController*)rootNavigationController playlist:(Playlist*)playlist;
- (BOOL)isTrackFLOWable:(Track*)track;

@end

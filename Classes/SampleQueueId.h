//
//  SampleQueueId.h
//  ExampleApp
//
//  Created by EvilNOP on 20/01/2014.
//  Copyright (c) 2014 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SampleQueueId : NSObject
@property (readwrite) int count;
@property (readwrite) NSURL* url;

-(id) initWithUrl:(NSURL*)url andCount:(int)count;

@end

//
//  SourceContentViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/11/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "SourceContentViewController.h"
#import "HelpTableViewController.h"

@interface SourceContentViewController ()<GADBannerViewDelegate>

@property (nonatomic, strong) UIBarButtonItem *leftBarButtonItem;
@property (strong, nonatomic) IBOutlet GADBannerView *bannerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bannerViewBottomConstraint;

@end

@implementation SourceContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [kGlobal navigationItem:self.navigationItem customTitle:@"SOURCES"];
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    self.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Help"] style:UIBarButtonItemStylePlain target:self action:@selector(leftBarButtonItemAction)];
    self.navigationItem.leftBarButtonItem = self.leftBarButtonItem;
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];

    self.bannerView.backgroundColor = [UIColor clearColor];
    [self setupBannerAdIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setupBannerAdIfNeeded
{
    self.bannerView.hidden = YES;
    
    if ([kParamsManager boolForName:kParamNameShowsBannerAd]) {
        self.bannerView.adUnitID = kGlobal.bannerUnitID;
        self.bannerView.delegate = self;
        self.bannerView.rootViewController = self;
        self.bannerView.autoloadEnabled = YES;
        [self.bannerView loadRequest:[GADRequest request]];
    }
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (self.bannerView.hidden) {
        self.bannerView.hidden = NO;
        self.bannerViewBottomConstraint.constant = 0;
        [self.view bringSubviewToFront:self.bannerView];
        [self.view setNeedsLayout];
    }
}

#pragma mark - Button action

- (void)leftBarButtonItemAction {
    UIStoryboard *sourceStoryboard = [UIStoryboard storyboardWithName:@"Help" bundle:[NSBundle mainBundle]];
    HelpTableViewController *helpTableViewController = (HelpTableViewController *)[sourceStoryboard instantiateViewControllerWithIdentifier:@"HelpTableViewController"];
    [self.navigationController pushViewController:helpTableViewController animated:YES];
}

@end

//
//  DriveFileFetcher.m
//  FreeMusic
//
//  Created by EvilNOP on 16/8/11.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import "DriveFileFetcher.h"

//
@interface FetchingInfo : NSObject
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSURLDLTask *task;
@property (nonatomic) CGFloat progress;
@property (nonatomic) NSInteger totalSize;
@end



//
@implementation FetchingInfo
@end

//
@interface DriveFileFetcher() {
    NSMutableDictionary *_keyToFetchingInfo;
    FetchingInfo *_mockInfo; // 准备下载 但是 还未开始开始下载的 key 指向这个 “假的”info
}

@end

@implementation DriveFileFetcher

- (void)sharedInstanceInitializer {
    _keyToFetchingInfo = [NSMutableDictionary dictionary];
    _mockInfo = [[FetchingInfo alloc] init];
    _mockInfo.key = @"mock";
}

- (void)prepareToFetchFileWithKey:(NSString *)key {
    _keyToFetchingInfo[key] = _mockInfo;
    [self postNumberOfFetchingFilesChangedNotification];
}

- (void)startFetchingFileWithRequest:(NSURLRequest*)request forKey:(NSString*)key targetPath:(NSString*)targetPath totalSize:(NSInteger)totalSize {
    if (_keyToFetchingInfo[key] && _keyToFetchingInfo[key] != _mockInfo) {
        return;
    }
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSProgress *progress = nil;
    NSString *observingKeyPath = nil;
    if (totalSize == 0) {
        observingKeyPath = @"fractionCompleted";
    }
    else {
        observingKeyPath = @"completedUnitCount";
    }
    
    NSURLDLTask *task = [manager DLTaskWithRequest:request progress:&progress destination:^NSURL *(NSURL *_, NSURLResponse *response) {
        return [NSURL fileURLWithPath:targetPath];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        [progress removeObserver:self forKeyPath:observingKeyPath];
        [_keyToFetchingInfo removeObjectForKey:key];
        
        if (error == nil || error.code == -1013) { //完成 或者 resume失败
            [self deleteResumeDataForKey:key];
        }

        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        if (filePath) dict[@"filePath"] = filePath;
        if (error) dict[@"error"] = error;

        [self postNumberOfFetchingFilesChangedNotification];
    }];
    
    [task resume];
    
    FetchingInfo *info = [[FetchingInfo alloc] init];
    info.key = key;
    info.task = task;
    info.totalSize = totalSize;
    _keyToFetchingInfo[key] = info;
    
    //
    [progress addObserver:self forKeyPath:observingKeyPath options:NSKeyValueObservingOptionNew context:(__bridge void * _Nullable)(key)];
    
    //
    [self postNumberOfFetchingFilesChangedNotification];
}

- (void)startFetchingFileWithRequest:(NSURLRequest*)request forKey:(NSString*)key targetPath:(NSString*)targetPath {
    [self startFetchingFileWithRequest:request forKey:key targetPath:targetPath totalSize:0];
}

- (NSDictionary*)fetchingInfoForKey:(NSString*)key {
    FetchingInfo *info = _keyToFetchingInfo[key];
    if (info) {
        return @{ @"key": info.key, @"progress": @(info.progress) };
    }
    
    return nil;
}

- (void)cancelFetchingFileWithKey:(NSString *)key {
    FetchingInfo *FetchingInfo = _keyToFetchingInfo[key];
    [FetchingInfo.task cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
        NSString *path = [self resumeDataPathForKey:key];
        [resumeData writeToFile:path atomically:YES];
    }];
    
    [_keyToFetchingInfo removeObjectForKey:key];
    [self postNumberOfFetchingFilesChangedNotification];
}

#pragma mark - Observe
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    NSString *key = (__bridge NSString*)context;
    FetchingInfo *info = _keyToFetchingInfo[key];
    
    if ([keyPath isEqualToString:@"fractionCompleted"]) {
        info.progress = [change[@"new"] floatValue];
    }
    else if ([keyPath isEqualToString:@"completedUnitCount"]) {
        info.progress = [change[@"new"] integerValue] / (info.totalSize * 1.0);
    }
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        FEPostNotification(DriveFileFetcherFileFetchingProgressUpdatedNotification, nil, [self fetchingInfoForKey:key]);
    });
}

#pragma mark - Private
- (NSString*)resumeDataPathForKey:(NSString*)key {
    return [kUtil join:kGlobal.audioResumeDataRootPath, key, nil];
}

- (NSData*)resumeDataForKey:(NSString*)key {
    NSString *path = [self resumeDataPathForKey:key];
    return [NSData dataWithContentsOfFile:path];
}

- (void)deleteResumeDataForKey:(NSString*)key {
    NSString *path = [self resumeDataPathForKey:key];
    [kFM removeItemAtPath:path error:nil];
}

- (void)postNumberOfFetchingFilesChangedNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:DriveFileFetcherNumberOfFetchingFilesChangedNotification object:@(_keyToFetchingInfo.count)];
}

@end

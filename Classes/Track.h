//
//  Track.h
//  Drive
//
//  Created by EvilNOP on 15/3/11.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    TrackSourceDeprecated = 0,
    TrackSourceGoogleDrive,
    TrackSourceDropbox,
    TrackSourceOneDrive,
    TrackSourceiTunesMusic,
    TrackSourceImport
} TrackSource;

@interface Track : Model

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *remoteID;
@property (nonatomic, strong) NSString *iconURL;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic, strong) NSString *contentURL;
@property (nonatomic, strong) NSString *largeIconURL;
@property (nonatomic) NSInteger size;
@property (nonatomic, strong) NSString *extension;
@property (nonatomic, strong) NSString *artist;

//本地属性
@property (nonatomic) BOOL isSelected;

@property (nonatomic, readonly) NSString *audioLocalPath;
@property (nonatomic) TrackSource source;

- (BOOL)isAudioDownloaded;

@end

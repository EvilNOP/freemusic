//
//  FormatDate.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/12/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit

@objc class FormatDate: NSObject {
    
    class func formateDuration(milliseconds: Int) -> String {
        let seconds = milliseconds / 1000
        let second = seconds % 60
        let minute = seconds / 60 % 60
        let hour = seconds / 60 / 60 % 60
        
        if hour == 0 {
            return String(format: "%02d:%02d", minute, second)
        } else {
            return String(format: "%02d:%02d:%02d", hour, minute, second)
        }
    }
}

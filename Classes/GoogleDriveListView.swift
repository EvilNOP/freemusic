//
//  GoogleDriveListTableView.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/1/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD
import MJRefresh
import Alamofire

@objc class GoogleDriveListView: UIView {
    
    var parentsViewController: UIViewController!
    var cellSelectedHandler: ((index: NSInteger, isFolder: Bool, file: GTLDriveFile) -> Void)?
    var cancelSignInHandler: (() -> Void)?
    
    private let tableView = UITableView(frame: CGRectZero, style: .Plain)
    private let searchBar = UISearchBar()
    private var noLoginView: NoLoginView?
    private let noSearchingView = NoSearchingView()
    
    private let foler = "application/vnd.google-apps.folder"
    private let audio = "audio/mpeg"
    private let scopes = [kGTLAuthScopeDrive]
    private let service = GTLServiceDrive()
    private var credentials: GTMOAuth2Authentication!
    private var isSearchedViewController = false
    
    private var response: GTLDriveFileList = GTLDriveFileList()
    private var searchResponse: GTLDriveFileList = GTLDriveFileList()
    
    enum ButtonFlag: Int32 {
        case Add, Added
    }
    
    private var items: [GTLDriveFile] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var searchItems: [GTLDriveFile]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    func isVerified() -> Bool {
        return fetchCredentials().canAuthorize
    }
    
    func queryItemsWithText(queryText: String) {
        if MBProgressHUD(forView: self.tableView) == nil {
            MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)
        }
        
        let query = GTLQueryDrive.queryForFilesList()
        query.q = "\(queryText) and mimeType='\(audio)'"
        query.fields = "nextPageToken, *"
        query.pageSize = 20
        query.orderBy = "folder, name"
        if self.searchResponse.nextPageToken != nil {
            query.pageToken = self.searchResponse.nextPageToken
        } else {
            if self.tableView.mj_footer != nil {
                self.tableView.mj_footer?.hidden = true
            }
        }
        service.executeQuery(query, delegate: self, didFinishSelector: #selector(GoogleDriveListView.displayResultWithTicket(_:finishedWithObject:error:)))
    }
    
    func querylocalItemWithText(queryText: String) {
        if items.count != 0 {
            let regx = Global.sharedInstance().regxWithText(queryText)
            let predicate = NSPredicate(format: "SELF LIKE[cd] %@", regx)
            searchItems = items.filter { (file) -> Bool in
                predicate.evaluateWithObject(file.name)
            }
            if (self.tableView.mj_footer != nil) {
                self.tableView.mj_footer?.hidden = true
            }
        }
    }
    
    func verify() {
        guard parentsViewController != nil else { return }
        parentsViewController.presentViewController(createAuthController(), animated: true, completion: nil)
    }
    
    func fetchFiles(file: GTLDriveFile?) {
        
        if MBProgressHUD(forView: self.tableView) == nil {
            MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)
        }
        
        let query = GTLQueryDrive.queryForFilesList()
        var identifier: String = "root"
        if let parent = file {
            identifier = parent.identifier
        }
        query.q = "('\(identifier)' in parents) and (mimeType='\(foler)' or mimeType='\(audio)')"
        query.fields = "nextPageToken, *"
        query.pageSize = 20
        query.orderBy = "folder, name"
        if (self.response.nextPageToken != nil) {
            query.pageToken = self.response.nextPageToken
        }
        service.executeQuery(query, delegate: self, didFinishSelector: #selector(GoogleDriveListView.displayResultWithTicket(_:finishedWithObject:error:)))
    }
    
    class func removeAuthorize() {
        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kGoogleDriveKeychainItemName)
    }
    
    // Discuss: init位置
    convenience init(controller: UIViewController, searching: Bool) {
        self.init()
        parentsViewController = controller
        isSearchedViewController = searching
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.rowHeight = 70
        
        searchBar.barStyle = .Default
        searchBar.returnKeyType = .Search
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
        self.addSubview(searchBar)
        self.addSubview(tableView)
        
        noLoginView = NoLoginView(loginHandler: {
            self.verify()
        })
        
        service.authorizer = fetchCredentials()
        
        if searching {
            if isVerified() {
                tableView.tableFooterView = noSearchingView
                tableView.scrollEnabled = false
            } else {
                tableView.tableFooterView = noLoginView
                tableView.scrollEnabled = false
            }
        } else {
            if isVerified() {
                tableView.tableFooterView = UIView()
            }
        }
    }
    
    init () {
        super.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        searchBar.frame = CGRectMake(0, 0, self.width, 44)
        tableView.frame = CGRectMake(0, searchBar.height, self.width, self.height - searchBar.height)
        
        noLoginView!.frame = CGRectMake(0, 0, self.width, self.height - searchBar.height)
        noSearchingView.frame = CGRectMake(0, 0, self.width, self.height - searchBar.height)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension GoogleDriveListView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showsSearchItems(){
            return searchItems!.count
        } else {
            return items.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier: String = "Cell"
        
        var optionalCell = tableView.dequeueReusableCellWithIdentifier(identifier) as? TrackCell
        if optionalCell == nil {
            optionalCell = TrackCell(style: .Default, reuseIdentifier: identifier)
            
            let accessoryButton = FlagButton()
            accessoryButton.autoChangeFlag = false
            accessoryButton.size = CGSize(width: 40, height: self.tableView.rowHeight)
            accessoryButton.addFlag(ButtonFlag.Add.rawValue, icon: kUtil.tintImage(UIImage(named: "Add"), withColor: colorForKey("primaryColor")))
            accessoryButton.addFlag(ButtonFlag.Added.rawValue, icon: UIImage(named: "Added"))
            accessoryButton.addTarget(self, action: #selector(addButtonTapped(_:)), forControlEvents: .TouchUpInside)
            
            optionalCell?.accessoryView = accessoryButton
        }
        
        let cell = optionalCell!
        
        let file: GTLDriveFile
        
        if showsSearchItems() {
            file = searchItems![indexPath.row]
        } else {
            file = items[indexPath.row]
        }

        if file.mimeType == foler {
            cell.accessoryType = .DisclosureIndicator
            cell.imageView?.image = UIImage(named: "File")
            cell.accessoryView?.hidden = true
            
        } else if file.mimeType == audio {
            cell.accessoryType = .None
            cell.imageView?.image = UIImage(named: "Music")
            
            let accessoryButton = cell.accessoryView as! FlagButton
            accessoryButton.hidden = false
            accessoryButton.flag = (Track.existedWithID(file.identifier) ? ButtonFlag.Added.rawValue : ButtonFlag.Add.rawValue)
        }
        
        cell.textLabel?.attributedText = NSAttributedString(string: file.name, attributes: Global.sharedInstance().thirdFontAttribute() as? [String: AnyObject])
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let file: GTLDriveFile
        
        if showsSearchItems() {
            file = searchItems![indexPath.row]
        } else {
            file = items[indexPath.row]
        }
        
        if file.mimeType == foler {
            if isSearchedViewController == false {
                if let handler = cellSelectedHandler {
                    handler(index: indexPath.row, isFolder: file.mimeType == foler, file: file)
                }
            }
            
        } else {
            var tracks = [Track]()
            var index = 0
            for item in (self.searchItems ?? self.items) {
                if item.mimeType == foler {
                    continue
                }
                
                let track = kGoogleDriveSource.createTrackWithFile(item)
                tracks.append(track)
                
                if item == file {
                    index = tracks.count - 1
                }
            }
            
            kGlobal.enqueuTracks(tracks, andPlayTrackAtIndex: index, shuffled: false, rootNavigationController: self.parentsViewController.navigationController, playlist: nil)
        }
    }
}

// MARK: - Searchbar delegate
extension GoogleDriveListView: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        if isVerified() {
            return true
        }
        
        showAlert("", message: "Please login")
        self.tableView.tableFooterView = self.noLoginView
        return false
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
        
        tableView.tableFooterView = UIView()
        tableView.scrollEnabled = true
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if isSearchedViewController == false && searchBar.text != "" {
            let queryText = searchBar.text!
            if isVerified() {
                NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(self.querylocalItemWithText), object: queryText)
                performSelector(#selector(self.querylocalItemWithText), withObject: queryText, withObject: 0.5)
            }
        } else if isSearchedViewController && searchBar.text == "" {
            searchItems = nil
            self.tableView.mj_footer?.hidden = true
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if searchBar.text != "" {
            let queryText = "name contains '\(searchBar.text!)'"
            if isVerified() {
                if isSearchedViewController {
                    if self.searchItems == nil {
                        self.searchItems = []
                    }
                    
                    if self.searchResponse.nextPageToken != nil {
                        self.searchResponse = GTLDriveFileList()
                    }
                    
                    queryItemsWithText(queryText)
                } else {
                    querylocalItemWithText(searchBar.text!)
                }
            }
        }
        
        kGlobal.increaseSearchTimesAndProcess()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchItems = nil
        self.tableView.mj_footer?.hidden = true
        
        if isSearchedViewController {
            self.items = []
        }
        
        MBProgressHUD.hideHUDForView(self.tableView, animated: true)
        
        if isSearchedViewController == false || self.tableView.mj_footer != nil && self.tableView.mj_footer?.hidden == true {
            self.tableView.mj_footer?.hidden = false
            if self.response.nextPageToken == nil {
                self.tableView.mj_footer?.hidden = true
            }
        }
        
        if isSearchedViewController {
            tableView.tableFooterView = noSearchingView
            tableView.scrollEnabled = false
        } else {
            tableView.tableFooterView = UIView()
            tableView.scrollEnabled = true
        }
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        
        if let files = searchItems where files.count == 0 {
            tableView.tableFooterView = noSearchingView
            tableView.scrollEnabled = false
        }
    }
}

extension GoogleDriveListView {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.searchBar.isFirstResponder() {
            self.searchBar.resignFirstResponder()
        }
    }
}

// MARK: - Util
extension GoogleDriveListView {
    func showsSearchItems() -> Bool {
        return self.searchItems != nil
    }
    
    func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .Alert
        )
        
        let ok = UIAlertAction(
            title: "OK",
            style: .Default,
            handler: nil
        )
        
        alert.addAction(ok)
        parentsViewController.presentViewController(alert, animated: true, completion: nil)
    }
}

// MARK: - Handler
extension GoogleDriveListView {
    @objc private func addButtonTapped(sender: FlagButton) {
        let cell = kUtil.findSuperViewWithClass(TrackCell.self, subview: sender) as! TrackCell
        let indexPath = self.tableView.indexPathForCell(cell)
        let file = (self.searchItems ?? self.items)[indexPath!.row]
        
        if Track.existedWithID(file.identifier) == false {
            let track = kGoogleDriveSource.createTrackWithFile(file)
            track.save()
            sender.flag = ButtonFlag.Added.rawValue
        }
    }
}

// MARK: - Private
extension GoogleDriveListView {
    
    private func fetchCredentials() -> GTMOAuth2Authentication {
        credentials = GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kGoogleDriveKeychainItemName, clientID: kGoogleDriveClientID, clientSecret: nil)
        return credentials
    }
    
    private func createAuthController() -> UINavigationController {
        let scopeString = scopes.joinWithSeparator(" ")
        
        let authController =  GTMOAuth2ViewControllerTouch(
            scope: scopeString,
            clientID: kGoogleDriveClientID,
            clientSecret: nil,
            keychainItemName: kGoogleDriveKeychainItemName,
            delegate: self,
            finishedSelector: #selector(GoogleDriveListView.viewController(_:finishedWithAuth:error:))
        )
        
        let navigationController = UINavigationController(rootViewController: authController)
        let leftButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(self.cancelLogin))
        authController.navigationItem.leftBarButtonItem = leftButtonItem
        return navigationController
    }
    
    @objc private func cancelLogin() {
        guard parentsViewController != nil else { return }
        parentsViewController.dismissViewControllerAnimated(true, completion: nil)
        
        if let handler = cancelSignInHandler {
            handler()
        }
    }
    
    // Handle completion of the authorization process, and update the Drive API with the new credentials.
    @objc private func viewController(vc: UIViewController, finishedWithAuth authResult: GTMOAuth2Authentication, error: NSError?) {
        if isSearchedViewController {
            credentials = authResult
            service.authorizer = authResult
            tableView.tableFooterView = noSearchingView
            parentsViewController.dismissViewControllerAnimated(true, completion: nil)
        } else {
            if let error = error {
                service.authorizer = nil
                print(error.localizedDescription)
                return
            }
            credentials = authResult
            service.authorizer = authResult
            parentsViewController.dismissViewControllerAnimated(true, completion: nil)
            fetchFiles(nil)
        }
    }

    // Parse results and display
    @objc private func displayResultWithTicket(ticket: GTLServiceTicket, finishedWithObject response: GTLDriveFileList, error: NSError?) {
        MBProgressHUD.hideHUDForView(self.tableView, animated: true)
    
        if let error = error {
            showAlert("Error", message: error.localizedDescription)
            return
        }
        
        if let files = (response.files as? [GTLDriveFile]) where !files.isEmpty {
            
            if (self.tableView.mj_footer == nil) {
                self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(self.requestNextPage))
            }

            if (self.tableView.mj_footer?.isRefreshing() == true) {
                self.tableView.mj_footer?.endRefreshing()
            }
            
            if showsSearchItems() {
                
                if self.searchResponse.nextPageToken != nil {
                    self.searchItems?.appendContentsOf(files)
                } else {
                    self.searchItems = files
                }
 

            } else {
                items.appendContentsOf(files)

                self.tableView.reloadData()
            }
        } else {
            if showsSearchItems() {
                searchItems = []
            } else {
                items = []
            }
        }
        
        if isSearchedViewController {
            self.searchResponse = response
        } else {
            self.response = response
        }
        
        self.tableView.mj_footer?.hidden = (response.nextPageToken == nil)
    }
    
    @objc private func requestNextPage() {
        if isSearchedViewController {
            print(self.searchResponse.nextPageToken)
            if searchBar.text != nil && self.searchResponse.nextPageToken != nil {
                queryItemsWithText("name contains '\(searchBar.text!)'")
            }
        } else {
            if showsSearchItems() == false {
                fetchFiles(nil)
            }
        }
    }
}



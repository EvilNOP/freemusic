//
//  PlayerUserInfo.h
//  Drive
//
//  Created by EvilNOP on 15/4/15.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PlayerDatasourceTypeTrendingTracks,
    PlayerDatasourceTypeSearchingResults,
    PlayerDatasourceTypeSongs,
    PlayerDatasourceTypePlaylist
} PlayerDatasourceType;

@interface PlayerUserInfo : NSObject
@property (nonatomic) PlayerDatasourceType datasourceType;
@property (nonatomic, strong) id object;
+ (id)userInfoWithDatasourceType:(PlayerDatasourceType)datasourceType object:(id)object;
@end
 

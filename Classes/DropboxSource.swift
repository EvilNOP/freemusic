//
//  DropboxSource.swift
//  FreeMusic
//
//  Created by EvilNOP on 16/8/11.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class DropboxSource: NSObject {
    static let sharedInstance = DropboxSource()
    
    var rootViewController: UIViewController?
    
    typealias FetchFilesCompletionHandler = (files: [SwiftyJSON.JSON]?, cursor: String?, errorOccurs: Bool) -> Void
    typealias FetchStreamRequestCompletionHandler = (request: NSURLRequest) -> Void

    private var authViewController: UIViewController?
    
    func isAuthoirzed() -> Bool {
        return Dropbox.authorizedClient != nil
    }
    
    func authorizeWithCompletionHandler(completionHandler: (authorized: Bool) -> Void) {
        if self.rootViewController == nil {
            return
        }
        
        Dropbox.authorizeFromController(
            self.rootViewController!,
            completion: {
                completionHandler(authorized: true)
            },
            cancel: {
                completionHandler(authorized: false)
            }
        )
    }
    
    func removeAuthorization() {
        Dropbox.unlinkClient()
    }
    
    func fetchFiles(path: String, limit: Int, completionHandler: FetchFilesCompletionHandler) {
        if let client = Dropbox.authorizedClient {
            client.files.listFolderGetLatestCursor(path: path, recursive: false, includeMediaInfo: true).customResponse({ (data, error) in
                let json = newJSONWithData(data)
                completionHandler(files: json?["entries"].array, cursor: json?["cursor"].string, errorOccurs: (error != nil))
            })
        }
        else {
            completionHandler(files: nil, cursor: nil, errorOccurs: true)
        }
    }
    
    func fetchFilesWithCursor(cursor: String, completionHandler: FetchFilesCompletionHandler) {
        if let client = Dropbox.authorizedClient {
            client.files.listFolderContinue(cursor: cursor).customResponse({ (data, error) in
                let json = newJSONWithData(data)
                completionHandler(files: json?["entries"].array, cursor: json?["cursor"].string, errorOccurs: (error != nil))
            })
        }
        else {
            completionHandler(files: nil, cursor: nil, errorOccurs: true)
        }
    }
    
    func fetchStreamRequestForTrack(track: Track, completionHandler: FetchStreamRequestCompletionHandler) {
        if let client = Dropbox.authorizedClient {
            let request = client.files.streamRequestForFileWithID(track.remoteID)
            completionHandler(request: request)
        }
    }
    
    func createTrackWithItem(item: [String: AnyObject]) -> Track {
        let track = Track()
        track.remoteID = item["id"] as? String
        track.title = item["name"] as? String
        track.source = TrackSourceDropbox
        return track
    }
}

//
//  iTunesMusicSource.m
//  FreeMusic
//
//  Created by EvilNOP on 16/8/19.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import "iTunesMusicSource.h"

@implementation iTunesMusicSource

- (Track*)createTrackWithItem:(MPMediaItem *)item {
    Track *track = [[Track alloc] init];
    track.remoteID = [NSString stringWithFormat:@"%ld", (long)item.persistentID];
    track.title = item.title;
    track.duration = item.playbackDuration;
    track.source = TrackSourceiTunesMusic;
    track.artist = item.artist;

    return track;
}

- (MPMediaItem*)findMediaItemWithTrack:(Track*)track {
    MPMediaQuery *query = [MPMediaQuery songsQuery];
    MPMediaPropertyPredicate *predicate = [MPMediaPropertyPredicate predicateWithValue:track.remoteID forProperty:MPMediaItemPropertyPersistentID];
    [query addFilterPredicate:predicate];
    return [[query items] firstObject];
}

@end

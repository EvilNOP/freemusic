//
//  MoreViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "MoreViewController.h"
#import "FreeMusic-Swift.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

@import OneDriveSDK;

@interface MoreViewController()<MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *removeAllButton;
@property (weak, nonatomic) IBOutlet UILabel *storageLabel;
@property (weak, nonatomic) IBOutlet UILabel *availableLabel;
@property (weak, nonatomic) IBOutlet UILabel *allSongSizeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *googleDriveImageView;
@property (weak, nonatomic) IBOutlet UILabel *googleDriveLabel;

@property (weak, nonatomic) IBOutlet UIImageView *dropboxImageView;
@property (weak, nonatomic) IBOutlet UILabel *dropboxLabel;

@property (weak, nonatomic) IBOutlet UIImageView *oneDriveImageView;
@property (weak, nonatomic) IBOutlet UILabel *oneDriveLabel;

@end

@implementation MoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.removeAllButton.layer.cornerRadius = 5.0;
    self.removeAllButton.layer.masksToBounds = YES;
    self.removeAllButton.backgroundColor = colorForKey(@"primaryColor");
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.storageLabel.text = [NSString stringWithFormat:@"%0.2fG", [self allStorage]];
    self.availableLabel.text = [NSString stringWithFormat:@"%0.2fG", [self freeStorage]];
    
    UInt64 bytes = [self folderSize:[kUtil.documentsPath stringByAppendingPathComponent:sourceFolder]];
    self.allSongSizeLabel.text = [NSByteCountFormatter stringFromByteCount:bytes countStyle:NSByteCountFormatterCountStyleFile];
    
    [self changeGoogleDriveUI];
    [self changeDropboxUI];
    [self changeOneDriveUI];
}

#pragma mark - Tableview delegate & source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 20.0;
    } else {
        return 50;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *content = FEStringWithFormat(@"I’m using a cool app that you can discover and listen free music. Highly recommended! You can download it for free via the following link: https://itunes.apple.com/app/id%@", kAppID);
    
    if (indexPath.section == cloundServices) {
        
        if (indexPath.row == googleDrive) {
            
            [self selectGoogleCellAction];
            
        } else if (indexPath.row == dropbox) {
            
            [self selectDropboxCellAction];
            
        } else if (indexPath.row == oneDrive) {
            
            [self selectOneDriveCellAction];
        }
        
    } else if (indexPath.section == share) {
        if (indexPath.row == message) {
            
            [self selectMessageCellWithContent:content];
            
        } else if (indexPath.row == email) {
            
            [self selectEmailCellWithContent:content];
            
        } else if (indexPath.row == twitter) {
            
            NSString *content = FEStringWithFormat(@"Highly recommend a cool app which you can discover and listen free music. https://itunes.apple.com/app/%@", kAppID);
            [self selectTwitterCellWithContent:content];
            
        } else if (indexPath.row == facebook) {
            
            [self selectFacebookCellWithContent:content];
        }
    }
}

#pragma mark - TableView did selected action

- (void)selectGoogleCellAction {
    if ([self googleDriveIsVerified]) {
        [self showAlertControllerWithConfirm:^{
            [self googleSignOut];
        }];
    } else {
        [self googleLogin];
    }
}

- (void)selectDropboxCellAction {
    if ([self dropboxIsVerified]) {
        [self showAlertControllerWithConfirm:^{
            [self dropboxSignOut];
        }];
    } else {
        [self dropboxSignIn];
    }
}

- (void)selectOneDriveCellAction {
    if ([self oneDriveIsVerified]) {
        [self showAlertControllerWithConfirm:^{
            [self oneDriveSignOut];
        }];
    } else {
        [self oneDrivSignIn];
    }
}

- (void)selectMessageCellWithContent:(NSString *)content {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText]) {
        controller.body = content;
        controller.messageComposeDelegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)selectEmailCellWithContent:(NSString *)content {
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    if([MFMailComposeViewController canSendMail]) {
        [controller setSubject:kAppName];
        [controller setMessageBody:content isHTML:NO];
        controller.mailComposeDelegate = (id)self;
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)selectTwitterCellWithContent:(NSString *)content {
    SLComposeViewController *controller = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
    [controller setInitialText:content];
    [controller addImage:[UIImage imageNamed:@"iTunesArtwork"]];
    if (controller != nil) {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)selectFacebookCellWithContent:(NSString *)content {
    SLComposeViewController *controller = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [controller addImage:[UIImage imageNamed:@"iTunesArtwork"]];
    [controller setInitialText:content];
    NSString *urlString = FEStringWithFormat(@"https://itunes.apple.com/app/id%@", kAppID);
    [controller addURL:[NSURL URLWithString:urlString]];
    if (controller != nil) {
        [self presentViewController:controller animated:YES completion:nil];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate & MFMessageComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Button action

- (IBAction)removeAllButtonAction:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Delete all tracks?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *sure = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
        
        [self removeAllData];
        [kGlobal setupDataPaths];
        
        UInt64 bytes = [self folderSize:[kUtil.documentsPath stringByAppendingPathComponent:sourceFolder]];
        self.allSongSizeLabel.text = [NSByteCountFormatter stringFromByteCount:bytes countStyle:NSByteCountFormatterCountStyleFile];
        
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
        hud.label.text = @"Completed";
        hud.mode = MBProgressHUDModeText;
        [hud hideAnimated:YES afterDelay:1.0];
    }];
    [alert addAction:cancel];
    [alert addAction:sure];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)googleDriveCancelSignInBarButtonAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private

- (NSString *)documentsPath {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

- (NSDictionary *)fileSysAttributes {
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSDictionary *fileSysAttributes = [kFM attributesOfFileSystemForPath:path error:nil];
    return fileSysAttributes;
}

- (UInt64)folderSize:(NSString *)folderPath {
    NSArray *filesArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:folderPath error:nil];
    NSEnumerator *filesEnumerator = [filesArray objectEnumerator];
    NSString *fileName;
    UInt64 fileSize = 0;
    
    while (fileName = [filesEnumerator nextObject]) {
        if ([self isSpecialFile:fileName]) {
            continue;
        }
        
        NSDictionary *fileDictionary = [kFM attributesOfItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:nil];
        fileSize += [fileDictionary[NSFileSize] integerValue];
    }
    
    return fileSize;
}

- (BOOL)isSpecialFile:(NSString *)fileName {
    NSString *lastName = [fileName lastPathComponent];
    return ([lastName isEqualToString:@"."] || [lastName isEqualToString:@".."] || [lastName isEqualToString:@".DS_Store"]);
}

- (double)allStorage {
    return [[[self fileSysAttributes] objectForKey:NSFileSystemSize] doubleValue] / 1024.0 / 1024.0 / 1024.0;
}

- (double)freeStorage {
    return [[[self fileSysAttributes] objectForKey:NSFileSystemFreeSize] doubleValue] / 1024.0 / 1024.0 / 1024.0;
}

- (double)usedStorage {
    return [self allStorage] - [self freeStorage];
}

- (void)showAlertControllerWithConfirm:(void (^)())confirmAction  {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Sign Out?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        confirmAction();
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:confirm];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (NSInteger)currentTime {
    return [[NSDate new] timeIntervalSince1970];
}

- (void)showAlartWithMessage:(NSString *)message {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)removeAllData {
    NSError *removeDocDataError;
    NSError *removeTmpDataError;
    
    [kFM removeItemAtPath:[kUtil.documentsPath stringByAppendingPathComponent:sourceFolder] error:&removeDocDataError];
    [kFM removeItemAtPath:[kUtil.tmpPath stringByAppendingPathComponent:sourceFolder] error:&removeTmpDataError];
    
    if (removeDocDataError != nil) {
        NSLog(@"remove doc data error: %@", removeDocDataError.localizedDescription);
    }
    
    if (removeTmpDataError != nil) {
        NSLog(@"remove doc data error: %@", removeTmpDataError.localizedDescription);
    }
    
    [[SDImageCache sharedImageCache] cleanDisk];
    [[SDImageCache sharedImageCache] clearMemory];
}

#pragma mark - Google Drive

- (GTMOAuth2Authentication *)googleDriveCredential {
    return [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kGoogleDriveKeychainItemName clientID:kGoogleDriveClientID clientSecret:nil];
}

- (void)changeGoogleDriveUI {
    if ([self googleDriveIsVerified]) {
        self.googleDriveImageView.image = [kUtil tintImage:[UIImage imageNamed:@"GoogleDrive"] withColor:colorForKey(@"primaryColor")];
        self.googleDriveLabel.text = @"Sign out";
    } else {
        self.googleDriveImageView.image = [UIImage imageNamed:@"GoogleDriveSignOut"];
        self.googleDriveLabel.text = @"Sign in";
    }
}

- (BOOL)googleDriveIsVerified {
    GTMOAuth2Authentication *credential = [self googleDriveCredential];
    
    if ([credential canAuthorize]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)googleLogin {
    NSArray *scopes = @[kGTLAuthScopeDrive];
    
    GTMOAuth2ViewControllerTouch *loginController = [GTMOAuth2ViewControllerTouch controllerWithScope:[scopes componentsJoinedByString:@" "]
                                             clientID:kGoogleDriveClientID
                                         clientSecret:nil
                                     keychainItemName:kGoogleDriveKeychainItemName
                                    completionHandler:^(GTMOAuth2ViewControllerTouch *viewController, GTMOAuth2Authentication *auth, NSError *error) {
                                        [self dismissViewControllerAnimated:true completion:nil];
                                        if (error == nil) {
                                            self.googleDriveImageView.image = [kUtil tintImage:[UIImage imageNamed:@"GoogleDrive"] withColor:colorForKey(@"primaryColor")];
                                            self.googleDriveLabel.text = @"Sign out";
                                        }
                                    }];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:loginController];
    UIBarButtonItem *leftButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(googleDriveCancelSignInBarButtonAction)];
    loginController.navigationItem.leftBarButtonItem = leftButtonItem;
    [self presentViewController:navigationController animated:YES completion:nil];
    
}

- (void)googleSignOut {
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kGoogleDriveKeychainItemName];
    [self changeGoogleDriveUI];
}

#pragma mark - Dropbox

- (BOOL)dropboxIsVerified {
    return [DropboxHelper isVerified];
}

- (void)dropboxSignOut {
    [DropboxHelper removeAuthorizer];
    [self changeDropboxUI];
}

- (void)dropboxSignIn {
    [Dropbox authorizeFromController:self completion:^{
        self.dropboxImageView.image = [kUtil tintImage:[UIImage imageNamed:@"Dropbox"] withColor:colorForKey(@"primaryColor")];
        self.dropboxLabel.text = @"Sign out";
    } cancel:^{}];
}

- (void)changeDropboxUI {
    if ([self dropboxIsVerified]) {
        self.dropboxImageView.image = [kUtil tintImage:[UIImage imageNamed:@"Dropbox"] withColor:colorForKey(@"primaryColor")];
        self.dropboxLabel.text = @"Sign out";
    } else {
        self.dropboxImageView.image = [UIImage imageNamed:@"DropboxSignOut"];
        self.dropboxLabel.text = @"Sign in";
    }
}

#pragma mark - OneDrive 

- (BOOL)oneDriveIsVerified {
    if ([ODClient loadCurrentClient] == nil) {
        return NO;
    } else {
        return YES;
    }
}

- (void)oneDriveSignOut {
    [[ODClient loadCurrentClient] signOutWithCompletion:^(NSError *error) {
        [self changeOneDriveUI];
    }];
}

- (void)oneDrivSignIn {
    [ODClient authenticatedClientWithCompletion:^(ODClient *client, NSError *error) {
        if (error == nil) {
            [ODClient setCurrentClient:client];
            [[NSUserDefaults standardUserDefaults] setInteger:[self currentTime] forKey:@"oneDriveClientTime"];
            [self changeOneDriveUI];
        }
    }];
}

- (void)changeOneDriveUI {
    if ([self oneDriveIsVerified]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.oneDriveImageView.image = [kUtil tintImage:[UIImage imageNamed:@"OneDrive"] withColor:colorForKey(@"primaryColor")];
                self.oneDriveLabel.text = @"Sign out";
            });
    } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.oneDriveImageView.image = [UIImage imageNamed:@"OneDriveSignOut"];
                self.oneDriveLabel.text = @"Sign in";
            });
    }
}

@end

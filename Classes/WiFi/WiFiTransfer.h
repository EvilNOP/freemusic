//
//  WiFiTransfer.h
//  Coolplayer
//
//  Created by EvilNOP on 16/6/22.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const WiFiTransferDidReceiveFileNotification = @"WiFiTransferDidReceiveFileNotification";

@interface WiFiTransfer : FESingleton

- (void)runWithRootPath:(NSString*)path;
- (void)stop;

@end

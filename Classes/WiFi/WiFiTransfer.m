//
//  WiFiTransfer.m
//  Coolplayer
//
//  Created by EvilNOP on 16/6/22.
//  Copyright © 2016年 EvilNOP. All rights reserved.
//

#import "WiFiTransfer.h"

#import "GCDWebServer.h"
#import "GCDWebServerDataResponse.h"
#import "GCDWebServerMultiPartFormRequest.h"
#import "GCDWebServerFileResponse.h"
#import "GCDWebServerStreamedResponse.h"

static NSString * const kWebFolerName = @"Web";
static NSString * const kMapSuffix = @"-min.map";

@interface WiFiTransfer ()

@property (nonatomic, strong) NSString *rootPath;
@property (nonatomic, strong) GCDWebServer *server;

@end

@implementation WiFiTransfer

- (void)sharedInstanceInitializer {
    [GCDWebServer setLogLevel:3]; // warning
}

- (void)runWithRootPath:(NSString *)path {
    if (self.server.isRunning) {
        return;
    }
    
    self.rootPath = path;
    
    if (self.server == nil) {
        [self setupServer];
    }
    
    [self.server startWithPort:kWiFiPort bonjourName:nil];
}

- (void)stop {
    [self.server stop];
}

#pragma mark - Private
- (void)setupServer {
    _server = [[GCDWebServer alloc] init];
    
    __weak WiFiTransfer *SELF = self;
    
    [_server addDefaultHandlerForMethod:@"GET" requestClass:[GCDWebServerRequest class] processBlock:^GCDWebServerResponse *(GCDWebServerRequest *request) {        
        if ([request.path hasSuffix:kMapSuffix]) {
            return nil;
        }
        
        else if ([request.path isEqualToString:@"/"]) {
            //
            NSMutableDictionary *localizations = [NSMutableDictionary dictionary];
            //
            NSMutableDictionary *locals = [NSMutableDictionary dictionary];
            
            return [GCDWebServerDataResponse responseWithHTMLTemplate:[SELF templatePathWithName:@"index.html"]
                                                            variables:@{
                                                                @"app": kAppName,
                                                                @"escapedApp": [[kUtil appBundleName] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
                                                                @"localizations": [SELF JSONFromDictioanry:localizations],
                                                                @"locals": [SELF JSONFromDictioanry:locals]
                                                                }];
        }
        
        else if ([request.path hasPrefix:@"/public"]) {
            NSString *path = [SELF staticFilePathForRequestPath:request.path];
            
            if ([kFM fileExistsAtPath:path]) {
                return [GCDWebServerFileResponse responseWithFile:[SELF staticFilePathForRequestPath:request.path]];
            }
            else {
                return nil;
            }
        }
        
        return [GCDWebServerDataResponse responseWithJSONObject:@{}];
    }];
    
    [_server addDefaultHandlerForMethod:@"POST" requestClass:[GCDWebServerMultiPartFormRequest class] processBlock:^GCDWebServerResponse *(GCDWebServerRequest *request) {
        if ([request.path isEqualToString:@"/upload"]) {
            GCDWebServerMultiPartFormRequest *r = (GCDWebServerMultiPartFormRequest*)request;
            for (GCDWebServerMultiPartFile *multipartFile in r.files) {
                Track *track = [[Track alloc] init];
                track.remoteID = [NSString stringWithFormat:@"%d", arc4random()];
                track.source = TrackSourceImport;
                track.title = multipartFile.fileName;
                [track save];
                
                NSError *error = nil;
                [kFM moveItemAtPath:multipartFile.temporaryPath toPath:track.audioLocalPath error:&error];
                NSLog(@"%s %@", __func__, error);
            }
        }
        
        return [GCDWebServerDataResponse responseWithJSONObject:@{}];
    }];
}

- (NSString*)JSONFromDictioanry:(NSDictionary*)dictionary {
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil];
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (NSString*)staticFilePathForRequestPath:(NSString*)requestPath {
    NSString *resourceName = [kUtil join:kWebFolerName, requestPath, nil];
    return [kUtil pathForResource:resourceName];
}

- (NSString*)templatePathWithName:(NSString*)name {
    NSString *filename = [kUtil join:kWebFolerName, @"views", name, nil];
    return [kUtil pathForResource:filename];
}

@end

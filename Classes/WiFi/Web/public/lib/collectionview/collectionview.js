
/**
 * [format description]
 * @return {string} format
 * @return {string} arg1
 * @return {string} arg2
 * ...
 */
function format() {
    var args = arguments;
    var text = args[0];
    var index = 1;
    return text.replace(/%@/g, function() {
        var r = args[index];
        index += 1;
        return r;
    });
}


function FEPoint(x, y) {
    if (this instanceof FEPoint == false) {
        return new FEPoint(x, y);
    }

    this.x = x;
    this.y = y;
}


function FESize(width, height) {
    if (this instanceof FESize == false) {
        return new FESize(width, height);
    }
    
    this.width = width;
    this.height = height;
}


function FERect(x, y, width, height) {
    if (this instanceof FERect == false) {
        return new FERect(x, y , width, height);
    }

    this.origin = FEPoint(x, y);
    this.size = FESize(width, height);
}


function FEInset(top, left, bottom, right) {
    if (this instanceof FEInset == false) {
        return new FEInset(top, left, bottom, right);
    }

    this.top = top;
    this.left = left;
    this.bottom = bottom;
    this.right = right;
}


function FERange(location, length) {
    if (this instanceof FERange == false) {
        return new FERange(location, length);
    }

    this.location = location;
    this.length = length;
}


/**
 * [CollectionViewCell description]
 * @param {object} options {}
 */
function CollectionViewCell() {
    jQuery.fn.init.call(this, "<div class='collection-view-cell'></div>");
    this._index = -1;
}
CollectionViewCell.prototype = $();


/**
 * [CollectionView description]
 * @param {object} options {} 
 */
CollectionViewContentAlignment = {
    Left: 0,
    Center: 1,
    Right: 2
};

function CollectionView(options) {
    jQuery.fn.init.call(this, "<div class='collection-view'></div>");
    
    this.contentView = $("<div class='content-view'></div>");
    this.append(this.contentView);

    this.datasource = options.datasource;
    this.delegate = options.delegate;

    this._visibleCells = [];
    this._reusableCells = [];
    this._numberOfCellsInARow = 0;
    this._numberOfRows = 0;
    this._hasObservedScrollEvent = false;
}

CollectionView.prototype = $();
CollectionView.prototype.reloadData = function() {
    // datasource data
    this._sizeForCell = this.datasource.sizeForCell();
    this._contentInsets = this.datasource.contentInsets();
    this._lineSpacing = this.datasource.lineSpacing();
    this._interitemSpacing = this.datasource.interitemSpacing();
    this._numberOfItems = this.datasource.numberOfItems();
    this._contentAlignment = this.datasource.contentAlignment();

    // 设置padding
    this.css({
        "padding-top": this._contentInsets.top,
        "padding-left": this._contentInsets.left,
        "padding-bottom": this._contentInsets.bottom,
        "padding-right": this._contentInsets.right
    });

    // 计算一行显示几个cell，共几行
    this._numberOfCellsInARow = (this.width() + this._interitemSpacing) / (this._sizeForCell.width + this._interitemSpacing);
    this._numberOfCellsInARow = Math.floor(this._numberOfCellsInARow);
    this._numberOfRows = Math.ceil(this._numberOfItems / this._numberOfCellsInARow);

    // 设置contentView位置    
    var contentViewWidth = this._numberOfCellsInARow*this._sizeForCell.width + (this._numberOfCellsInARow-1)*this._interitemSpacing;
    var contentViewHeight = this._numberOfRows*this._sizeForCell.height + (this._numberOfRows-1)*this._lineSpacing;
    this.contentView.css({
        width: contentViewWidth,
        height: contentViewHeight
    });

    if (this._contentAlignment == CollectionViewContentAlignment.Left) {
        this.contentView.css({ left: 0 });
    }
    else if (this._contentAlignment == CollectionViewContentAlignment.Center) {
        this.contentView.css({ 
            left: (this.width() - this.contentView.width()) / 2.0 
        });
    }
    else if (this._contentAlignment == CollectionViewContentAlignment.Right) {
        this.contentView.css({
            left: this.width() - this.contentView.width()
        });
    }

    // 回收全部cell
    for (var i=this._visibleCells.length-1; i>=0; i--) {
        var cell = this._visibleCells[i];
        cell.hide();
        this._visibleCells.splice(i, 1);
        this._reusableCells.push(cell);
    }
    
    // var isIE = (!+[1,]);
    // if (isIE) {
        if (this._hasObservedScrollEvent == false) {
            this._loop();
            this._hasObservedScrollEvent = true;
        }

        this._onScroll();
    // }
    // else {
    //     if (this._hasObservedScrollEvent == false) {
    //         this.on("scroll", function() {
    //             _this._onScroll();
    //         });
    //         this._hasObservedScrollEvent = true;
    //     }

    //     this.trigger("scroll");
    // }
};

CollectionView.prototype.contentOffset = function(y, animated) {
    if (arguments.length == 0) {
        return this.scrollTop() - this._contentInsets.top;
    }

    var scrollTop = y + this._contentInsets.top;
    if (animated) {
        this.animate({
            scrollTop: scrollTop
        }, 250);
    }
    else {
        this.scrollTop(scrollTop);
    }
};

CollectionView.prototype.visibleCells = function() {
    return this._visibleCells.slice();
};

CollectionView.prototype._loop = function() {
    var _this = this;
    
    this._onScroll();
    
    setTimeout(function() {
        _this._loop();
    }, 5);
};

CollectionView.prototype._frameForCellAtIndex = function(index) {
    var row = Math.floor(index / this._numberOfCellsInARow);
    var column = index - (row * this._numberOfCellsInARow);

    return FERect(
        column*(this._sizeForCell.width + this._interitemSpacing),
        row*(this._sizeForCell.height + this._lineSpacing),
        this._sizeForCell.width,
        this._sizeForCell.height
    );
};

CollectionView.prototype._rangeForCellsThatShouldBeShown = function() {
    if (this._numberOfItems == 0) {
        return FERange(0, 0);
    }

    var contentOffset = this.contentOffset();
    var visibleBottom = contentOffset + this.innerHeight();

    var startRow = Math.floor(contentOffset / (this._sizeForCell.height + this._lineSpacing));
    if (startRow < 0) {
        startRow = 0;
    }
    var startIndex = startRow * this._numberOfCellsInARow;

    var endIndex;
    var endRow = Math.floor(visibleBottom / (this._sizeForCell.height + this._lineSpacing));
    var maxRow = this._numberOfRows - 1;
    if (endRow >= maxRow) {
        endIndex = this._numberOfItems - 1;
    }
    else {
        endIndex = (endRow+1) * this._numberOfCellsInARow - 1;
    }

    return FERange(startIndex, endIndex-startIndex+1);
};

CollectionView.prototype._dequeueReusableCell = function() {
    return this._reusableCells.pop();
};

CollectionView.prototype._fetchAndShowCellForItemAtIndex = function(index) {
    var _this = this;

    // 从datasource获取cell
    var reusableCell = this._dequeueReusableCell();
    var cell = this.datasource.cellForItemAtIndex(reusableCell, index);
    var isNewCell = (reusableCell == null && cell);
    cell._index = index;

    // cell点击事件
    if (isNewCell) {
        cell.on("click.cv", function(event) {
            _this.onClickCell(cell);
        });
    }

    // 显示cell
    var frame = this._frameForCellAtIndex(index);
    cell.css({
        top: frame.origin.y,
        left: frame.origin.x,
        width: frame.size.width,
        height: frame.size.height
    }); 

    this.contentView.append(cell);
    cell.show();

    return cell;
};

CollectionView.prototype._visibleCellsMinIndex = function() {
    return (this._visibleCells.length > 0 ? this._visibleCells[0]._index : -1);
};

CollectionView.prototype._visibleCellsMaxIndex = function() {
    return (this._visibleCells.length > 0 ? this._visibleCells[this._visibleCells.length-1]._index : -1);
};

CollectionView.prototype._onScroll = function() {
    if (this._numberOfItems == 0) {
        return;
    }

    //找出当前可视区域的cell index
    var indexRange = this._rangeForCellsThatShouldBeShown();
    var minIndex = indexRange.location;
    var maxIndex = indexRange.location + indexRange.length - 1;

    if (this._visibleCellsMinIndex() == minIndex && this._visibleCellsMaxIndex() == maxIndex) {
        return;
    }

    // 回收cell
    for (var i = this._visibleCells.length-1; i >= 0; i--) {
        var cell = this._visibleCells[i];
        if (cell._index < minIndex || cell._index > maxIndex) {
            cell.hide();
            this._visibleCells.splice(i, 1);
            this._reusableCells.push(cell);
        };
    }

    var visibleCellsMinIndex = this._visibleCellsMinIndex();
    var visibleCellsMaxIndex = this._visibleCellsMaxIndex();

    // 显示可视区域的cell
    var forwardCells = [];
    var backwardCells = [];

    for (var i=minIndex; i<=maxIndex; i++) {
        if (i < visibleCellsMinIndex) {
            var cell = this._fetchAndShowCellForItemAtIndex(i);
            forwardCells.push(cell);        
        }
        else if (i > visibleCellsMaxIndex) {
            var cell = this._fetchAndShowCellForItemAtIndex(i);
            backwardCells.push(cell);
        }
    }

    if (forwardCells.length > 0) {
        this._visibleCells = $.merge(forwardCells, this._visibleCells);    
    }
    if (backwardCells.length > 0) {
        this._visibleCells = $.merge(this._visibleCells, backwardCells);
    }
};

CollectionView.prototype.onClickCell = function(cell) {
    if (this.delegate || this.delegate.onClickCell) {
        this.delegate.onClickCell(cell, cell._index);
    }
};

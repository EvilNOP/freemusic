/**
 * definition
 */
var Status = {
    Regular: 0,
    Uploading: 1
};

var kIsIE = (!+[1,]);

var variales = {
    fileUploadXHRs: [],
    status: Status.Regular
};

var views = {
    progress: null
};

/**
 * main
 */
$.ajaxSetup({cache: false});

$(document).ready(function () {
    setupFileUploader();
    setupEventHandlers();
    setupColors();

    viewDidAppear();
});

function viewDidAppear() {
    views.progress = $('.progress');
    setCurrentStatus(Status.Regular);
}

function setupColors() {
    // $("ul#folders").css("background-color", locals.primaryColor);
    // $(".progress-background").css("background-color", locals.primaryColor);
}

function setupFileUploader() {
    $("#fileupload").fileupload({
        autoUpload: true,
        sequentialUploads: true,
        url: '/upload',
        type: 'POST',
        dataType: 'json',

        add: function (e, data) {
            var xhr = data.submit();
            variales.fileUploadXHRs.push(xhr);
        },

        start: function (e, data) {
            console.log('start');
            setCurrentStatus(Status.Uploading);
        },

        progressall: function (e, data) {
            var progress = data.loaded * 1.0 / data.total;

            var progress = parseInt(progress * 100) + '%';
            views.progress.find('span').text(progress);
            views.progress.find('div').css({width: progress});

            console.log(progress);
        },

        stop: function (e, data) {
            setTimeout(function () {
                setCurrentStatus(Status.Regular);
            }, 2000);

            variales.fileUploadXHRs = [];
        }
    });
}

function setupEventHandlers() {
    $("#cancel-button").click(function () {
        if (variales.status == Status.Uploading) {
            var xhrs = variales.fileUploadXHRs;
            for (var i = 0; i < xhrs.length; i++) {
                xhrs[i].abort();
            }
            variales.fileUploadXHRs = [];
            setCurrentStatus(Status.Regular);
        }
    });
}

function setCurrentStatus(status) {
    variales.status = status;

    if (status == Status.Regular) {
        $('#cancel-button').hide();
        $('#upload-button').show();
        views.progress.hide();
        views.progress.find('div').css({width: 0});
    }
    else if (status == Status.Uploading) {
        $('#cancel-button').show();
        $('#upload-button').hide();
        views.progress.show();
    }
}

/**
 * helper
 */
function join() {
    var components = [];
    for (var i = 0; i < arguments.length; i++) {
        var arg = arguments[i];

        if (i > 0) {
            arg = arg.replace(/^\/*/, "");
        }

        if (i < arguments.length - 1) {
            arg = arg.replace(/\/*$/, "");
        }

        components.push(arg);
    }

    return components.join('/');
}

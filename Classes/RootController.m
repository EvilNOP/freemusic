//
//  RootViewController.m
//  Drive
//
//  Created by EvilNOP on 15/3/10.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "RootController.h"
#import "SongsViewController.h"
#import "PlayListViewController.h"
#import "SettingsViewController.h"
#import "SourceTableViewController.h"
#import "MoreViewController.h"
#import "FreeMusic-Swift.h"
#import "SourceContentViewController.h"
#import "SettingContentViewController.h"

UINavigationController* newNavigation(id rootController, NSString *title, NSString *tabbarIconName);
UINavigationController* newNavigation(id rootController, NSString *title, NSString *tabbarIconName) {
    UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:rootController];
    n.tabBarItem.title = title;
    n.tabBarItem.image = [UIImage imageNamed:tabbarIconName];
    return n;
}

@interface RootController () <UITabBarControllerDelegate> {
    UILabel *_downloadingFilesBadge;
    NSUInteger _numberOfDownloadingAudio;
}

@end

@implementation RootController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableArray *viewControllers = [NSMutableArray array];
    UIStoryboard *sourceStoryboard = [UIStoryboard storyboardWithName:@"Source" bundle:[NSBundle mainBundle]];
    SourceContentViewController *sourceContentViewController = (SourceContentViewController *)[sourceStoryboard instantiateViewControllerWithIdentifier:@"SourceContentViewController"];
    [viewControllers addObject:newNavigation(sourceContentViewController, _(@"Sources"), @"SourcesPre")];
    
    SearchViewController *searching = [[SearchViewController alloc] init];
    [viewControllers addObject:newNavigation(searching, _(@"Search"), @"SearchPre")];
    
    SongsViewController *songsViewController = [[SongsViewController alloc] initWithType:SongsViewControllerTypeRegular];
    [viewControllers addObject:newNavigation(songsViewController, _(@"Songs"), @"SongsPre")];
    
    PlayListViewController *playlistViewController = [[PlayListViewController alloc] initWithType:PlayListViewControllerTypeRegular];
    [viewControllers addObject:newNavigation(playlistViewController, _(@"Playlist"), @"PlaylistPre")];
    
    UIStoryboard *moreStoryboard = [UIStoryboard storyboardWithName:@"More" bundle:[NSBundle mainBundle]];
    SettingContentViewController *moreContentViewController = (SettingContentViewController *)[moreStoryboard instantiateViewControllerWithIdentifier:@"SettingContentViewController"];
    [viewControllers addObject:newNavigation(moreContentViewController, _(@"More"), @"MorePre")];
    
    self.viewControllers = viewControllers;
    self.delegate = (id)self;
    
    _downloadingFilesBadge = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    _downloadingFilesBadge.layer.cornerRadius = _downloadingFilesBadge.width/2.0;
    _downloadingFilesBadge.layer.masksToBounds = YES;
    _downloadingFilesBadge.backgroundColor = FEHexRGBA(0xd92424, 1.0);
    _downloadingFilesBadge.text = @"10";
    _downloadingFilesBadge.font = [UIFont systemFontOfSize:10];
    _downloadingFilesBadge.textAlignment = NSTextAlignmentCenter;
    _downloadingFilesBadge.adjustsFontSizeToFitWidth = YES;
    _downloadingFilesBadge.textColor = FEHexRGBA(0xffffff, 1.0);
    [self.tabBar addSubview:_downloadingFilesBadge];
    [self setDownloadingFilesBadgeNumber:0];
    
    FEAddObserver(self, DriveFileFetcherNumberOfFetchingFilesChangedNotification, @selector(numberOfDownloadingFilesChanged:));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    _downloadingFilesBadge.center = CGPointMake(self.view.width/2.0+16, _downloadingFilesBadge.height/2.0+1);
}

#pragma mark - Notification
- (void)numberOfDownloadingFilesChanged:(NSNotification*)notification {
    NSInteger number = [notification.object intValue];
    [self setDownloadingFilesBadgeNumber:number];
}

#pragma mark - UITabBarControllerDelegate
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    FEPostNotification(kRootControllerDidSelectViewControllerNotification, viewController, nil);
}

#pragma mark - Public
- (void)setDownloadingFilesBadgeNumber:(NSUInteger)number
{
    if (number == 0) {
        _downloadingFilesBadge.hidden = YES;
    }
    else {
        _downloadingFilesBadge.hidden = NO;
        
        if (number > 99) {
            _downloadingFilesBadge.text = @"99+";
        }
        else {
            _downloadingFilesBadge.text = FEStringWithFormat(@"%lu", (unsigned long)number);
        }
    }
}

@end

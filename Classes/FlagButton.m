//
//  StateButton.m
//  Drive
//
//  Created by EvilNOP on 15/3/30.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "FlagButton.h"

static NSString * const kKeyFlag = @"flag";
static NSString * const kKeyIcon = @"icon";

@interface FlagButton () {
    NSMutableArray *_flagInfoGroup;
}
@end

@implementation FlagButton

- (id)init
{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    _flagInfoGroup = [NSMutableArray array];
    _autoChangeFlag = YES;
    [self addTarget:(id)self action:@selector(tapHandler:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Public
- (void)addFlag:(int)flag icon:(UIImage *)icon
{
    NSDictionary *info = @{ kKeyFlag: @(flag), kKeyIcon: icon };
    [_flagInfoGroup addObject:info];
    if (_flagInfoGroup.count == 1) {
        [self setFlag:flag];
    }
}

- (void)setFlag:(int)currentFlag
{
    for (NSDictionary *info in _flagInfoGroup) {
        if ([info[kKeyFlag] intValue] == currentFlag) {
            [self updateFlagAndIconWithFlagInfo:info];
            break;
        }
    }
}

#pragma mark - Handler
- (void)tapHandler:(id)sender
{
    if (_autoChangeFlag == NO) {
        return;
    }
    
    int index = 0;
    for (NSDictionary *info in _flagInfoGroup) {
        if ([info[kKeyFlag] intValue] == _flag) {
            NSDictionary *nextFlagInfo = _flagInfoGroup[(index+1) % _flagInfoGroup.count];
            [self updateFlagAndIconWithFlagInfo:nextFlagInfo];
            break;
        }
        
        index++;
    }

}

#pragma mark - Prvate
- (void)updateFlagAndIconWithFlagInfo:(NSDictionary*)flagInfo
{
    _flag = [flagInfo[kKeyFlag] intValue];
    
    UIImage *icon = flagInfo[kKeyIcon];
    [self setImage:icon forState:UIControlStateNormal];
}

@end

//
//  HelpTableViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/18/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "HelpTableViewController.h"

@interface HelpTableViewController()

@property (weak, nonatomic) IBOutlet UILabel *oneLabel;
@property (weak, nonatomic) IBOutlet UILabel *twoLabel;
@property (weak, nonatomic) IBOutlet UILabel *threeLabel;


@end

@implementation HelpTableViewController

- (void)viewDidLoad {
    self.oneLabel.textColor = kGlobal.tintColor;
    self.twoLabel.textColor = kGlobal.tintColor;
    self.threeLabel.textColor = kGlobal.tintColor;
    
    [kGlobal navigationItem:self.navigationItem customTitle:@"HELP"];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0 || indexPath.row == 2) {
        cell.backgroundColor = colorForKey(@"bigBackgroundColor");
    }
}

@end

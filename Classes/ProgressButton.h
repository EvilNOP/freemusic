//
//  ProgressButton.h
//  Drive
//
//  Created by EvilNOP on 15/4/14.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FFCircularProgressView.h"

@interface ProgressButton : UIButton

@property (nonatomic, readonly) FFCircularProgressView *progressView;

@end

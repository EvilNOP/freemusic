//
//  OneDriveBrowserViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "OneDriveBrowserViewController.h"
#import "FreeMusic-Swift.h"

@interface OneDriveBrowserViewController()

@property (nonatomic, strong) OneDriveListView *oneDriveListView;

@end

@implementation OneDriveBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    if (self.file == nil) {
        [kGlobal navigationItem:self.navigationItem customTitle:@"One Drive"];
    } else {
        [kGlobal navigationItem:self.navigationItem customTitle:self.file.name];
    }
    
    self.oneDriveListView = [[OneDriveListView alloc] initWithController:self searching:NO];
    
    if ([self.oneDriveListView isVerified]) {
        [self.oneDriveListView loadChildren: self.file];
        self.oneDriveListView.currentFile = self.file;
    } else {
        [self.oneDriveListView verify];
    }
    
    __weak OneDriveBrowserViewController *weakSelf = self;
    self.oneDriveListView.cellSelectedHanlder = ^(NSInteger index, BOOL isFolder, ODItem *file) {
        if (isFolder) {
            OneDriveBrowserViewController *oneDriveBrowser = [[OneDriveBrowserViewController alloc] init];
            oneDriveBrowser.file = file;
            [weakSelf.navigationController pushViewController:oneDriveBrowser animated:true];
        }
    };
    
    self.oneDriveListView.cancelSignInHandler = ^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    [self.view addSubview:self.oneDriveListView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.oneDriveListView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}


@end

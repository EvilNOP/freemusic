//
//  GoogleDriveSource.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/11/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import Foundation

let GoogleDriveMimeTypeFolder = "application/vnd.google-apps.folder"
let GoogleDriveMimeTypeAudio = "audio/mpeg"

@objc class GoogleDriveSource: NSObject {
    static let sharedInstance = GoogleDriveSource()
    
    typealias AuthCompletionHandler = (authorized: Bool) -> Void
    typealias FetchFilesCompletionHandler = (files: [GTLDriveFile]?, nextPageInfo: NextPageInfo?, error: NSError?) -> Void
    typealias FetchStreamRequestCompletionHandler = (request: NSURLRequest) -> Void
    
    class NextPageInfo {
        var token: String
        var pageSize: Int
        var file: GTLDriveFile?
        var queryText: String?
        
        init(token: String, pageSize: Int, file: GTLDriveFile? = nil) {
            self.token = token
            self.pageSize = pageSize
            self.file = file
        }
        
        init(token: String, pageSize: Int, queryText: String) {
            self.token = token
            self.pageSize = pageSize
            self.queryText = queryText
        }
    }
    
    private let service = GTLServiceDrive()
    private let scopes = [kGTLAuthScopeDrive]
    private var authViewController: UIViewController?
    private var authCompletionHandler: AuthCompletionHandler?
    private var fetchFilesCompletionHandler: FetchFilesCompletionHandler?
    
    private var fileIDToFetchStreamRequestCompletionHandler = [String: FetchStreamRequestCompletionHandler]()
    
    override init() {
        super.init()
        
        self.service.authorizer = self.authentication()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(didFindGoogleDriveFileStreamInfo(_:)), name: "DidFindGoogleDriveFileStreamInfoNotification", object: nil)
    }
    
    // MARK: - Public
    func isAuthroized() -> Bool {
        return self.authentication().canAuthorize
    }
    
    func authorizeWithRootViewController(controller: UIViewController, completionHandler: AuthCompletionHandler?) {
        self.authViewController = self.createAuthViewController()
        controller.presentViewController(self.authViewController!, animated: true, completion: nil)
        self.authCompletionHandler = completionHandler
    }
    
    func removeAuthorization() {
        GTMOAuth2ViewControllerTouch.removeAuthFromKeychainForName(kGoogleDriveKeychainItemName)
    }
    
    func fetchFiles(file: GTLDriveFile?, pageSize: Int, nextPageToken: String? = nil, completionHandler: FetchFilesCompletionHandler) {
        let query = GTLQueryDrive.queryForFilesList()
        var identifier: String = "root"
        if let parent = file {
            identifier = parent.identifier
        }
        query.q = "('\(identifier)' in parents) and (mimeType='\(GoogleDriveMimeTypeFolder)' or mimeType='\(GoogleDriveMimeTypeAudio)')"
        query.fields = "nextPageToken, *"
        query.pageSize = pageSize
        query.pageToken = nextPageToken
        
        service.executeQuery(query) { (ticket, response, error) in
            let files = response?.files as? [GTLDriveFile]
            
            var nextPageInfo: NextPageInfo? = nil
            if let token = response?.nextPageToken {
                nextPageInfo = NextPageInfo(token: token, pageSize: pageSize, file: file)
            }
            
            completionHandler(files: files, nextPageInfo: nextPageInfo, error: error)
        }
    }
    
    func fetchFilesWithNextPageInfo(nextPageInfo: NextPageInfo, completionHandler: FetchFilesCompletionHandler) {
        self.fetchFiles(nextPageInfo.file, pageSize: nextPageInfo.pageSize, nextPageToken: nextPageInfo.token, completionHandler: completionHandler)
    }
    
    func searchFilesWithText(queryText: String, pageSize: Int, nextPageToken: String? = nil, completionHandler: FetchFilesCompletionHandler) {
        let query = GTLQueryDrive.queryForFilesList()
        query.q = "\(queryText) and mimeType='\(GoogleDriveMimeTypeAudio)'"
        query.fields = "nextPageToken, *"
        query.pageSize = pageSize
        query.orderBy = "folder, name"
        query.pageToken = nextPageToken
        
        service.executeQuery(query) { (ticket, response, error) in
            let files = response?.files as? [GTLDriveFile]
            
            var nextPageInfo: NextPageInfo? = nil
            if let token = response?.nextPageToken {
                nextPageInfo = NextPageInfo(token: token, pageSize: pageSize, queryText: queryText)
            }
            
            completionHandler(files: files, nextPageInfo:nextPageInfo, error: error)
        }
    }
    
    func searchFilesWithNextPageInfo(nextPageInfo: NextPageInfo, completionHandler: FetchFilesCompletionHandler) {
        self.searchFilesWithText(nextPageInfo.queryText!, pageSize: nextPageInfo.pageSize, nextPageToken: nextPageInfo.token, completionHandler: completionHandler)
    }
    
    func fetchStreamRequestForTrack(track: Track, completionHandler: FetchStreamRequestCompletionHandler) {
        let fetcher = service.fetcherService.fetcherWithURLString(track.contentURL)
        fetcher.beginFetchWithCompletionHandler(nil)

        self.fileIDToFetchStreamRequestCompletionHandler[track.remoteID] = completionHandler
    }
    
    func createTrackWithFile(file: GTLDriveFile) -> Track {
        let track = Track()
        track.remoteID = file.identifier
        track.title = file.name
        track.size = file.size.integerValue
        track.contentURL = file.webContentLink
        track.source = TrackSourceGoogleDrive
        
        return track
    }
    
    // MARK: - Notification
    func didFindGoogleDriveFileStreamInfo(notification: NSNotification) {
        let userInfo = notification.userInfo
        let url = userInfo?["url"] as? String
        let headers = userInfo?["headers"] as? [String: String]
        let dataTask = userInfo?["dataTask"] as? NSURLSessionDataTask
        
        if url == nil || headers == nil || dataTask == nil {
            return
        }
        
        for (fileID, completionHandler) in self.fileIDToFetchStreamRequestCompletionHandler {
            if url?.rangeOfString(fileID) != nil {
                let request = NSMutableURLRequest(URL: NSURL(string: url!)!)
                for (key, value) in headers! {
                    if key == "Authorization" || key == "Cookie" || key == "User-Agent" {
                        request.setValue(value, forHTTPHeaderField: key)
                    }
                }

                completionHandler(request: request)
                self.fileIDToFetchStreamRequestCompletionHandler.removeValueForKey(fileID)
                dataTask?.cancel()
                
                break
            }
        }
    }
    
    // MARK: - Private
    private func authentication() -> GTMOAuth2Authentication {
        return GTMOAuth2ViewControllerTouch.authForGoogleFromKeychainForName(kGoogleDriveKeychainItemName, clientID: kGoogleDriveClientID, clientSecret: nil)
    }
    
    private func createAuthViewController() -> UINavigationController {
        let scopeString = scopes.joinWithSeparator(" ")
        
        let c =  GTMOAuth2ViewControllerTouch(
            scope: scopeString,
            clientID: kGoogleDriveClientID,
            clientSecret: nil,
            keychainItemName: kGoogleDriveKeychainItemName,
            delegate: self,
            finishedSelector: #selector(self.authViewController(_:finishedWithAuth:error:))
        )
        
        let navigationController = UINavigationController(rootViewController: c)
        let leftButtonItem = UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: #selector(self.cancelLogin))
        c.navigationItem.leftBarButtonItem = leftButtonItem
        return navigationController
    }
    
    // MARK: - Callback
    @objc private func authViewController(controller: UIViewController, finishedWithAuth authResult: GTMOAuth2Authentication, error: NSError?) {
        self.service.authorizer = authResult
        self.authCompletionHandler?(authorized: (error == nil))
        
        self.authViewController?.dismissViewControllerAnimated(true, completion: nil)
        self.authViewController = nil
    }
    
    @objc private func cancelLogin() {
        self.authCompletionHandler?(authorized: false)
        
        self.authViewController?.dismissViewControllerAnimated(true, completion: nil)
        self.authViewController = nil
    }
}

//
//  TrackCell.m
//  Drive
//
//  Created by EvilNOP on 15/3/6.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "TrackCell.h"

@interface TrackCell () {
    UIImageView *_selectionImageView;
}

@end

@implementation TrackCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeTrackCell];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initializeTrackCell];
    }
    return self;
}

- (void)initializeTrackCell
{
    self.imageViewMargin = UIEdgeInsetsMake(7, 15, 7, 10);
    
    _selectionImageView = [[UIImageView alloc] init];
    _selectionImageView.contentMode = UIViewContentModeCenter;
    [self updateSelectionImage];
    [self.contentView addSubview:_selectionImageView];
    
    self.textLabel.numberOfLines = 2;
    self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.detailTextLabel.font = [UIFont systemFontOfSize:12];
    self.detailTextLabel.textColor = FEHexRGBA(0xc7c7cc, 1.0);
    
    _rhythmView = [[RhythmView alloc] initWithNumberOfLines:3];
    _rhythmView.lineTintColor = kGlobal.tintColor;
    _rhythmView.frame = CGRectMake(0, 0, 12, 8);
    _rhythmView.lineSpacing = 1.5;
    _rhythmView.defaultLineHeight = 2;
    _rhythmView.hidden = YES;
    [self.contentView addSubview:_rhythmView];
    
    _playingIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Playing"]];
    _playingIconView.frame = CGRectMake(0, 0, 10, 10);
    _playingIconView.hidden = YES;
    [self.contentView addSubview:_playingIconView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    _selectionImageView.frame = CGRectMake(-30, 0, 30, self.height);
    
    CGSize textSize = CGSizeZero;
    if (self.detailTextLabel) {
        textSize = [self.detailTextLabel.text sizeWithAttributes:@{NSFontAttributeName:self.detailTextLabel.font}];
    }
    _rhythmView.center = CGPointMake(self.detailTextLabel.x + textSize.width + _rhythmView.width,
                                     self.detailTextLabel.center.y);
    _playingIconView.center = _rhythmView.center;
    
    if (_didLayoutSubviews) {
        _didLayoutSubviews(self);
    }
    
}

#pragma mark - Private
- (void)updateSelectionImage
{
    if (_isSelected) {
        _selectionImageView.image = [UIImage imageNamed:@"Selected"];
    }
    else {
        _selectionImageView.image = [kUtil tintImage:[UIImage imageNamed:@"Unselected"] withColor:colorForKey(@"primaryColor")];
    }
}

#pragma mark - Public
- (void)setIsSelected:(BOOL)isSelected
{
    _isSelected = isSelected;
    [self updateSelectionImage];
}

- (void)setHidesSelectionBox:(BOOL)hidesSelectionBox
{
    _hidesSelectionBox = hidesSelectionBox;
    _selectionImageView.hidden = _hidesSelectionBox;
}

- (void)setShowsRhythmView:(BOOL)showsRhythmView
{
    _showsRhythmView = showsRhythmView;
    _rhythmView.hidden = (_showsRhythmView == NO);
    if (_rhythmView.hidden) {
        [_rhythmView stopAnimating];
    }
}

- (void)setShowsPlayingIcon:(BOOL)showsPlayingIcon
{
    _showsPlayingIcon = showsPlayingIcon;
    _playingIconView.hidden = (_showsPlayingIcon == NO);
}

- (void)setArtwork:(UIImage *)icon
{
    if (icon) {
        self.imageView.image = icon;
    }
    else {
        self.imageView.image = kDefaultTrackArtwork;
    }
}

@end

//
//  Global.m
//  Drive
//
//  Created by EvilNOP on 15/3/5.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "Global.h"
#import "AppDelegate.h"
#import "ImageAdViewController.h"
#import "AlertAdController.h"
#import "FreeMusic-Swift.h"

static NSString * const kUDKeyAlertADIndex = @"Global.m.1";
static NSString * const kUDKeyImageAdIndex = @"Global.m.2";
static NSString * const kUDKeyHasAlert4NoCache = @"Global.m.3";

@interface Global () <GADInterstitialDelegate> {
    GADInterstitial *_interstitial;
    ImageAdViewController *_imageAdViewController;
    BOOL _hasReceivedInterstitialBefore;
}

@end

@implementation Global

#pragma mark - Init
- (void)sharedInstanceInitializer {
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{
                  kUDKeyFLOWMode: @(FLOWModeLimited)
                                                              }];
    
    [kParamsManager registerWithAppKey:kParamsAppKey mode:OnlineParamsModeDebug defaults:@{
                                                              kParamNameShowsBannerAd: @1,
                                                              kParamNameSearchTimesToShowInterstitial: @3
                                                              }];
    
    _tintColor = colorForKey(@"tintColor");
    [[UINavigationBar appearance] setTintColor:_tintColor];
    [[UITabBar appearance] setTintColor:_tintColor];
    
    [self setupDataPaths];
    
    // Player
    [self initPlayer];
    
    // 插屏广告
    [self requestInterstitial];
    
    // notification
    FEAddObserver(self, UIApplicationDidBecomeActiveNotification, @selector(appDidBecomeActive:));
    
    // 声明不会下载、缓存音乐
    if (FEUserDefaultsBoolForKey(kUDKeyHasAlert4NoCache) == NO) {
        setTimeout(0.5, ^{
            FEAlertController *alert = [[FEAlertController alloc] initWithTitle:@"Dear loving users" message:@"Please acknowledge the app does not feature cache, save or download function in any manner whatsover to comply with content provider Terms of Service.\n Thank you for your support and understanding !" buttonTitles:nil cancelButtonTitle:@"OK" destructiveButtonTitle:nil preferredStyle:UIAlertControllerStyleAlert handler:nil];
            [alert showInController:self.rootViewController animated:YES completion:nil];
        });
        
        FESetUserDefaults(kUDKeyHasAlert4NoCache, @YES);
    }
    
    // WiFi access
    [kWiFiTransfer runWithRootPath:nil];
}

- (void)setupDataPaths {
    _cachedIconRootPath = [kUtil join:kUtil.tmpPath, @".data", @"icon", nil];
    [kFM createDirectoryAtPath:_cachedIconRootPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    _audioResumeDataRootPath = [kUtil join:kUtil.tmpPath, @".data", @"resume", nil];
    [kFM createDirectoryAtPath:_audioResumeDataRootPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    _cachedAudioRootPath = [kUtil join:kUtil.documentsPath, @".data", @"autodelete", nil];
    [kFM createDirectoryAtPath:_cachedAudioRootPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSString *databasePath = [kUtil join:kUtil.documentsPath, @".data", @"db", nil];
    [Model setPersistantStorePath:databasePath];
}

- (void)initPlayer {
    _playerViewController = [[PlayerViewController alloc] init];
    NSArray *tracks = [[Track allRecords] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Track *track1 = obj1;
        Track *track2 = obj2;
        return [track2.creationDate compare:track1.creationDate];
    }];
    [_playerViewController setTracks:tracks currentIndex:0];
    _playerViewController.hidesBottomBarWhenPushed = YES;
}

#pragma mark - Notification
- (void)appDidBecomeActive:(NSNotification*)notification {
    [self increaseLoginTimesAndProcess];
}

#pragma mark - GADInterstitialDelegate
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    if (_hasReceivedInterstitialBefore == NO) {
        NSInteger loginTimes = FEUserDefaultsIntegerForKey(kUDKeyLoginTimes);
        [self showInterstitialIfNeededWithLoginTimes:loginTimes];
    }
    
    _hasReceivedInterstitialBefore = YES;
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    [self requestInterstitial];
}

#pragma mark - GADInterstitial
- (void)showInterstitialIfNeededWithLoginTimes:(NSInteger)loginTimes {
    NSInteger loginIntervalToShowInterstitial = [kParamsManager integerForName:kParamNameLoginIntervalToShowInterstitial];
    
    if (loginIntervalToShowInterstitial > 0 &&
        loginTimes % loginIntervalToShowInterstitial == 0) {
        
        [self showInterstitial];
    }
}

- (void)requestInterstitial
{
#if TARGET_IPHONE_SIMULATOR
    //
#else
    if (_interstitial == nil || _interstitial.hasBeenUsed) {
        _interstitial = [[GADInterstitial alloc] initWithAdUnitID:kGlobal.interstitialUnitID];
        _interstitial.delegate = (id)self;
        [_interstitial loadRequest:[GADRequest request]];
    }
#endif
}

#pragma mark - Public - Marketing
- (void)increasePlayTimesAndProcess {
    NSInteger playTimes = FEUserDefaultsIntegerForKey(kUDKeyPlayTimes);
    playTimes++;
    FESetUserDefaults(kUDKeyPlayTimes, @(playTimes));
    
    NSInteger playTimesToShowInterstitial = [kParamsManager integerForName:kParamNamePlayTimesToShowInterstitial];
    if (playTimesToShowInterstitial > 0 && (playTimes % playTimesToShowInterstitial) == 0) {
        setTimeout(0.5, ^{
            [self showInterstitial];
        });
    }
}

- (void)increaseSearchTimesAndProcess {
    NSInteger searchTimes = FEUserDefaultsIntegerForKey(kUDKeySearchTimes);
    searchTimes++;
    FESetUserDefaults(kUDKeySearchTimes, @(searchTimes));

    NSInteger searchTimesToShowInterstitial = [kParamsManager integerForName:kParamNameSearchTimesToShowInterstitial];
    if (searchTimesToShowInterstitial > 0 && (searchTimes % searchTimesToShowInterstitial) == 0) {
        setTimeout(0.5, ^{
            [self showInterstitial];
        });
    }
}

- (void)increaseLoginTimesAndProcess {
    NSInteger loginTimes = FEUserDefaultsIntegerForKey(kUDKeyLoginTimes);
    loginTimes++;
    FESetUserDefaults(kUDKeyLoginTimes, @(loginTimes));
    
    // alert广告
    NSInteger loginIntervalToShowAlertAd = [kParamsManager integerForName:kParamNameLoginIntervalToShowAlertAd];
    if ([ADManager alertAdInfoList].count > 0 &&
        loginIntervalToShowAlertAd > 0 &&
        loginTimes % loginIntervalToShowAlertAd == 0) {
        
        [self performSelector:@selector(showAlertAdWithResultHandler:) withObject:nil afterDelay:0.5];
    }
    
    // 图片广告
    NSInteger loginIntervalToShowImageAd = [kParamsManager integerForName:kParamNameLoginIntervalToShowImageAd];
    if ([ADManager imageAdInfoList].count > 0 &&
        loginIntervalToShowImageAd > 0 &&
        loginTimes % loginIntervalToShowImageAd == 0) {
        
        [self performSelector:@selector(showImageAd) withObject:nil afterDelay:0.5];
    }
    
    // 插屏广告
    // 注意：首次启动，即使条件满足也不会弹出广告，因为此时广告请求还未得到响应，这里的弹出广告对“从后台打开应用”的情况有效
    // 首次启动是否弹出广告的判断见 - [Global interstitialDidReceiveAd:] 回调
    [self showInterstitialIfNeededWithLoginTimes:loginTimes];
}

#pragma mark - Public - Ad
- (void)showAlertAdWithResultHandler:(void (^)(BOOL confirmed))resultHandler {
    NSArray *infoList = [ADManager alertAdInfoList];
    if (infoList.count == 0) {
        if (resultHandler) {
            resultHandler(NO);
        }
        return;
    }
    
    NSInteger index = FEUserDefaultsIntegerForKey(kUDKeyAlertADIndex) % infoList.count;
    ADAlertInfo *info = infoList[index];
    
    AlertAdController *alert = [[AlertAdController alloc] initWithAdInfo:info resultHandler:resultHandler];
    [kUtil repeatBlock:^(BOOL *stop) {
        if ([kGlobal topViewController] == [kGlobal rootViewController]) {
            [alert showInController:[kGlobal rootViewController] animated:YES completion:nil];
            *stop = YES;
        }
    } interval:0.5];
    
    index++;
    FESetUserDefaults(kUDKeyAlertADIndex, @(index));
}

- (void)showImageAd {
    NSArray *infoList = [ADManager imageAdInfoList];
    if (infoList.count == 0) {
        return;
    }
    
    NSInteger index = FEUserDefaultsIntegerForKey(kUDKeyImageAdIndex) % infoList.count;
    ADImageInfo *info = infoList[index];
    
    if (_imageAdViewController == nil) {
        _imageAdViewController = [[ImageAdViewController alloc] init];
        _imageAdViewController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _imageAdViewController.completionHandler = ^(ImageAdViewController *c, BOOL canceled) {
            [_imageAdViewController.view removeFromSuperview];
            _imageAdViewController = nil;
        };
    }
    _imageAdViewController.adInfo = info;
    
    [kGlobal.rootViewController.view addSubview:_imageAdViewController.view];
    _imageAdViewController.view.frame = kGlobal.rootViewController.view.bounds;
    [_imageAdViewController startEnterAnimation];
    
    index++;
    FESetUserDefaults(kUDKeyImageAdIndex, @(index));
}

- (void)showInterstitial {
    if (_interstitial.isReady) {
        [_interstitial presentFromRootViewController:kGlobal.rootViewController];
    }
}

- (NSString*)bannerUnitID {
    NSString *bannerUnitID = [kParamsManager valueForName:kParamNameBannerUnitID];
    if (bannerUnitID.length == 0) {
        bannerUnitID = kDefaultBannerUnitID;
    }
    return bannerUnitID;
}

- (NSString*)interstitialUnitID {
    NSString *interstitialUnitID = [kParamsManager valueForName:kParamNameInterstitialUnitID];
    if (interstitialUnitID.length == 0) {
        interstitialUnitID = kDefaultInterstitialUnitID;
    }
    return interstitialUnitID;
}

#pragma mark Public - Others
- (NSString*)generateRandomID {
    static NSMutableArray *charList = nil;
    if (charList == nil) {
        charList = [[NSMutableArray alloc] init];
        
        for (int i=48; i<=57; i++) { //ASCII 0-9
            [charList addObject:[NSString stringWithFormat:@"%c", i]];
        }
        for (int i=97; i<=122; i++) { //ASCII a-z
            [charList addObject:[NSString stringWithFormat:@"%c", i]];
        }
    }
    
    NSMutableString *r = [NSMutableString string];
    for (int i=0; i<=20; i++) {
        int index = (arc4random() % charList.count);
        [r appendString:charList[index]];
    }
    
    return r;
}

- (RootController*)rootViewController {
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    return (RootController*)appDelegate.window.rootViewController;
}

- (UIViewController*)topViewController {
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    UIViewController *c = appDelegate.window.rootViewController;
    while (c.presentedViewController) {
        c = c.presentedViewController;
    }
    
    return c;
}

- (UIBarButtonItem*)createPlayerBarButtonItem {
    // 生成Player图标
    float targetImageHeight = 26;
    
    NSDictionary *attributes = @{
                                 NSForegroundColorAttributeName: colorForKey(@"tintColor"),
                                 NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSKernAttributeName: @3
                                 };
    
    UIView *container = [[UIView alloc] init];
    
    UILabel *label = [[UILabel alloc] init];
    [container addSubview:label];
    label.attributedText = [[NSAttributedString alloc] initWithString:@"PLAYER" attributes:attributes];
    [label sizeToFit];
    label.y = (targetImageHeight - label.height) / 2.0;
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [container addSubview:imageView];
    UIImage *icon = [UIImage imageNamed:@"Next"];
    imageView.image = icon;
    imageView.frame = CGRectMake(label.right, 0, icon.size.width, icon.size.height);
    imageView.y = (targetImageHeight - imageView.height) / 2.0;
    
    container.frame = CGRectMake(0, 0, imageView.right, targetImageHeight);
    UIImage *image = [kUtil viewToImage:container scale:0];
    
    // 创建Player BarButtonItem
    UIBarButtonItem *playerButtonItem = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:(id)self action:@selector(playerBarButtonItemHandler:)];
    playerButtonItem.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    
    return playerButtonItem;
}

- (void)playerBarButtonItemHandler:(UIBarButtonItem*)item {
    UINavigationController *navigationController = (UINavigationController*)kGlobal.rootViewController.selectedViewController;
    [navigationController pushViewController:kGlobal.playerViewController animated:YES];
}

- (void)setAllTracksUnselected {
    for (Track *track in [Track allRecords]) {
        track.isSelected = NO;
    }
}

- (void)setAllPlaylistUnselected {
    for (Playlist *playlist in [Playlist allRecords]) {
        playlist.isSelected = NO;
    }
}

- (NSString*)formatSeconds:(NSTimeInterval)seconds {
    int secondsIntValue = floor(seconds);
    int minute = secondsIntValue / 60;
    int second = secondsIntValue - minute*60;
    return FEStringWithFormat(@"%.2d:%.2d", minute, second);
}

- (BOOL)canOpenFLOWableSwitch {
    FLOWLimitType limitType = (FLOWLimitType)[kParamsManager integerForName:kParamNameFLOWLimitType];
    
    return limitType == FLOWLimitTypeNone ||
    (limitType == FLOWLimitTypeAd && [ADManager alertAdInfoList].count == 0) ||
    FEUserDefaultsBoolForKey(kUDKeyHasRate) ||
    FEUserDefaultsBoolForKey(kUDKeyHasAccessAdRestrictingFLOWMode);
}

- (void)pickClientID {
    NSArray *clientIDs = [kParamsManager valueForName:kParamNameClientIDs];

    if (clientIDs.count == 0) {
        clientIDs = @[@"b9ec6bf926eb4d0eb6beed50e6913b01",
                      @"4d3d1443fd60e38adc4ce8df2659403d",
                      @"9c26c4fef995d21441a74731cf437167"];
    }
    int index = arc4random() % clientIDs.count;

    _clientID = clientIDs[index];
}

- (void)tintImageForView:(id)aView withColor:(UIColor*)color {
    if ([aView isKindOfClass:[UIButton class]]) {
        UIButton *button = aView;
        UIImage *image = [kUtil tintImage:button.imageView.image withColor:color];
        [button setImage:image forState:UIControlStateNormal];
    }
    else if ([aView isKindOfClass:[UIImageView class]]) {
        UIImageView *imageView = aView;
        UIImage *image = [kUtil tintImage:imageView.image withColor:color];
        imageView.image = image;
    }
}

- (UIColor*)colorForKey:(NSString*)key {
    static NSDictionary *colors = nil;
    if (colors == nil) {
        NSString *path = [kUtil pathForResource:@"Colors.plist"];
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        NSString *target = [NSString stringWithFormat:@"%d", TARGET];
        colors = dict[target];
    }

    NSString *colorString = colors[key];
    if (colorString) {
        NSArray *components = [colorString componentsSeparatedByString:@","];
        NSString *hexString = [NSString stringWithFormat:@"0x%@", [components firstObject]];
        NSString *alphaString = [components lastObject];
        
        long hex = strtoul([hexString UTF8String], 0, 0);
        float alpha = [alphaString floatValue];
        return FEHexRGBA(hex, alpha);
    }
    
    return nil;
}

- (void)navigationItem:(UINavigationItem *)navigationItem customTitle:(NSString *)title {
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:title attributes:self.firstFontAttribute];
    [titleLabel sizeToFit];
    navigationItem.titleView = titleLabel;
}

- (void)navigationBar:(UINavigationBar *)navigationBar navigationItem:(UINavigationItem *)navigationItem backIndicatorWithImageName:(NSString *)name {
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
    navigationItem.backBarButtonItem = backButtonItem;
    
    float targetImageHeight = 26;
    UIView *container = [[UIView alloc] init];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [container addSubview:imageView];
    UIImage *icon = [UIImage imageNamed:name];
    imageView.image = icon;
    imageView.frame = CGRectMake(2, 0, icon.size.width, icon.size.height);
    imageView.y = (targetImageHeight - imageView.height) / 2.0 + 2.5;
    
    container.frame = CGRectMake(0, 0, imageView.right, targetImageHeight);
    UIImage *image = [kUtil viewToImage:container scale:0];
    
    [navigationBar setBackIndicatorTransitionMaskImage:image];
    [navigationBar setBackIndicatorImage:image];
}

- (NSDictionary *)firstFontAttribute {
    return @{
             NSForegroundColorAttributeName: colorForKey(@"firstFontColor"),
             NSFontAttributeName: [UIFont systemFontOfSize:18.0 weight:UIFontWeightMedium],
             NSKernAttributeName: @4
             };
}

- (NSDictionary *)secondFontAttribute {
    return @{
             NSForegroundColorAttributeName: colorForKey(@"secondFontColor"),
             NSFontAttributeName: [UIFont systemFontOfSize:18.0 weight:UIFontWeightRegular],
             NSKernAttributeName: @4
             };
}

- (NSDictionary *)thirdFontAttribute {
    return @{
             NSForegroundColorAttributeName: colorForKey(@"thirdFontColor"),
             NSFontAttributeName: [UIFont systemFontOfSize:16.0 weight:UIFontWeightRegular],
             NSKernAttributeName: @0
             };
}

- (NSDictionary *)fourthFontAttribute {
    return @{
             NSForegroundColorAttributeName: colorForKey(@"fourthFontColor"),
             NSFontAttributeName: [UIFont systemFontOfSize:12.0 weight:UIFontWeightRegular],
             NSKernAttributeName: @2
             };
}

- (NSDictionary *)fifthFontAttribute {
    return @{
             NSForegroundColorAttributeName: colorForKey(@"fifthFontColor"),
             NSFontAttributeName: [UIFont systemFontOfSize:10.0 weight:UIFontWeightRegular],
             NSKernAttributeName: @0
             };
}

- (NSDictionary *)navigationBarItemAttribute {
    return @{
             NSForegroundColorAttributeName: colorForKey(@"tintColor"),
             NSFontAttributeName: [UIFont systemFontOfSize:15.0],
             NSKernAttributeName: @3
             };
}

- (NSString *)regxWithText:(NSString *)text {
    NSMutableString *regx = [NSMutableString string];
    
    NSUInteger length = [text length];
    
    for (int i = 0; i < length; i++) {
        
        NSRange range = NSMakeRange(i, 1);
        NSString *subString = [text substringWithRange:range];
        
        const char *cString = [subString UTF8String];
        if (strlen(cString) == 3) { // 中文
            [regx appendString:[NSString stringWithFormat:@"*%@", subString]];
        } else {
            if ([self isMetaCharacter:subString] == NO) {
                [regx appendString:[NSString stringWithFormat:@"*%@", subString]];
            } else {
                [regx appendString:[NSString stringWithFormat:@"*\\%@", subString]];
            }
        }
    }
    
    if (regx.length > 0) {
        [regx appendString:@"*"];
    }
    
    return regx;
}

- (BOOL)isMetaCharacter:(NSString *)string {
    NSArray *metas = @[@"^", @"$", @"(", @")", @"*", @"+", @"?", @".", @"[", @"]", @"\\", @"{", @"}", @"|"];
    NSString *chs = [NSString stringWithFormat:@"%@", string];
    if ([metas containsObject:chs]) {
        return YES;
    }
    return NO;
}

- (BOOL)isSongFileWithName:(NSString *)name {
    NSArray *formats = [self supportAudioFormat];
    NSString *extension = [[name pathExtension] lowercaseString];
    return [formats containsObject:extension];
}

- (NSArray *)supportAudioFormat {
    return @[@"mp3", @"wma", @"rm", @"acc", @"ogg", @"ape", @"flac", @"flv",];
}

- (UIBarButtonItem *)barButtonItemImageWithTitle:(NSString *)title selector:(SEL)selector {
    float targetImageHeight = 26;
    UIView *container = [[UIView alloc] init];
    
    UILabel *label = [[UILabel alloc] init];
    [container addSubview:label];
    label.attributedText = [[NSAttributedString alloc] initWithString:title attributes:[kGlobal navigationBarItemAttribute]];
    [label sizeToFit];
    label.y = (targetImageHeight - label.height) / 2.0;
    
    container.frame = CGRectMake(0, 0, label.right, targetImageHeight);
    UIImage *image = [kUtil viewToImage:container scale:0];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:(id)self action:selector];
    item.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    return item;
}

- (void)fetchStreamRequestForTrack:(Track*)track completionHandler:(void (^)(NSURLRequest *request))completionHandler {
    if (track.source == TrackSourceGoogleDrive) {
        [kGoogleDriveSource fetchStreamRequestForTrack:track completionHandler:completionHandler];
    }
    else if (track.source == TrackSourceDropbox) {
        [kDropboxSource fetchStreamRequestForTrack:track completionHandler:completionHandler];
    }
    else if (track.source == TrackSourceOneDrive) {
        [kOneDriveSource fetchStreamRequestForTrack:track completionHandler:completionHandler];
    }
}

- (void)enqueuTracks:(NSArray*)tracks andPlayTrackAtIndex:(NSInteger)index shuffled:(BOOL)shuffled rootNavigationController:(UINavigationController*)rootNavigationController playlist:(Playlist*)playlist {
    self.currentPlayingPlaylist = playlist;
    [kGlobal.playerViewController setTracks:tracks currentIndex:index];
    kGlobal.playerViewController.isShuffled = shuffled;
    [kGlobal.playerViewController play];
    [rootNavigationController pushViewController:kGlobal.playerViewController animated:YES];
}

- (BOOL)isTrackFLOWable:(Track*)track {
    BOOL result = NO;

    if (track.source == TrackSourceiTunesMusic) {
        result = NO;
    }
    else {
        result = YES;
    }
    
    return result;
}

@end

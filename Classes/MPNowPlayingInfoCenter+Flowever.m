//
//  MPNowPlayingInfoCenter+Flowever.m
//  FreeMusic
//
//  Created by EvilNOP on 15/8/26.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "MPNowPlayingInfoCenter+Flowever.h"
#import <objc/runtime.h>


@implementation MPNowPlayingInfoCenter (Flowever)

+ (void)load {
    Method m1 = class_getInstanceMethod(self, @selector(setNowPlayingInfo:));
    Method m2 = class_getInstanceMethod(self, @selector(newSetNowPlayingInfo:));
    method_exchangeImplementations(m1, m2);
}

- (void)newSetNowPlayingInfo:(NSDictionary *)nowPlayingInfo {
    if (MPNowPlayingInfoCenterUpdatable) {
        [self newSetNowPlayingInfo:nowPlayingInfo]; // call original setNowPlayingInfo:
    }
}

@end

//
//  WifiTransferTableViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/10/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "WifiTransferTableViewController.h"
#import "Reachability.h"

@interface WifiTransferTableViewController ()

@property (weak, nonatomic) IBOutlet UIView *addressContentView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *connetImageView;

@end

@implementation WifiTransferTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [kGlobal navigationItem:self.navigationItem customTitle:@"WIFI TRANSFER"];
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    
    self.tableView.rowHeight = 300;
    self.tableView.estimatedRowHeight = UITableViewRowAnimationAutomatic;
    
    self.addressContentView.layer.cornerRadius = self.addressContentView.halfHeight;
    self.addressContentView.layer.borderColor = colorForKey(@"primaryColor").CGColor;
    self.addressContentView.layer.borderWidth = 0.5;
    
    self.connetImageView.backgroundColor = colorForKey(@"primaryColor");
    self.addressLabel.textColor = colorForKey(@"primaryColor");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    FEAddObserver(self, kReachabilityChangedNotification, @selector(reachabilityDidChange:));
    [self updateUIByWiFiStatus];
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    FERemoveObserver2(self, kReachabilityChangedNotification);
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.view.height;
}

#pragma mark - Primary

- (void)reachabilityDidChange:(NSNotification *)notification {
    [self updateUIByWiFiStatus];
}

- (BOOL)isWiFiAvailable {
    Reachability *reachability = [Reachability reachabilityForLocalWiFi];
    [reachability startNotifier];
    return [reachability currentReachabilityStatus] == ReachableViaWiFi;
}


- (void)updateUIByWiFiStatus {
    if ([self isWiFiAvailable]) {
        self.connetImageView.image = [UIImage imageNamed:@"Connect2"];
        if (TARGET_IPHONE_SIMULATOR) {
            self.addressLabel.text = [NSString stringWithFormat:@"localhost:%d", kWiFiPort];
        } else {
            self.addressLabel.text = [NSString stringWithFormat:@"http://%@:%d", [kUtil localIPAddress], kWiFiPort];
        }
    }
    else {
        self.connetImageView.image = [UIImage imageNamed:@"ConnectNoWiFi"];
        self.addressLabel.text = @"Not connected to a wireless network";
    }
}

@end

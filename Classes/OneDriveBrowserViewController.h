//
//  OneDriveBrowserViewController.h
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>
@import OneDriveSDK;

@interface OneDriveBrowserViewController : UIViewController

@property (nonatomic, strong) ODItem *file;

@end

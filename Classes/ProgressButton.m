//
//  ProgressButton.m
//  Drive
//
//  Created by EvilNOP on 15/4/14.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "ProgressButton.h"

@implementation ProgressButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _progressView = [[FFCircularProgressView alloc] initWithFrame:frame];
        _progressView.userInteractionEnabled = NO;
        _progressView.tintColor = FEHexRGBA(0xC66E35, 1.0);
        [self addSubview:_progressView];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _progressView.center = CGPointMake(self.width/2.0, self.height/2.0);
}

@end

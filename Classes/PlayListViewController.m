//
//  PlayListViewController.m
//  Drive
//
//  Created by EvilNOP on 15/3/11.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "PlayListViewController.h"
#import "SongsViewController.h"

#define isRegularType (_type == PlayListViewControllerTypeRegular)
#define isSelectionType (_type == PlayListViewControllerTypeSelection)

enum {
    kSectionTagTool,
    kSectionTagPlaylist
};

@interface PlayListViewController () <UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate> {
    __weak IBOutlet TrackTableView *_tableView;
    NSMutableArray *_sectionInfoList;
    NSMutableArray *_playlists;
    IBOutlet UITableViewCell *_toolCell;
    
    __weak IBOutlet GADBannerView *_bannerView;
    __weak IBOutlet UIImageView *_creationImageView;
}

@end

@implementation PlayListViewController

- (id)initWithType:(PlayListViewControllerType)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    if (isRegularType) {
        [kGlobal navigationItem:self.navigationItem customTitle:@"PLAYLIST"];
        self.navigationItem.leftBarButtonItem = [self createEditBarButtonItem];
        self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    }
    else if (isSelectionType) {
        [kGlobal navigationItem:self.navigationItem customTitle:@"ADD TO PLAYLIST"];
        self.navigationItem.leftBarButtonItem = [self createAtToPlaylistCancelButtonItem];
        self.navigationItem.rightBarButtonItem = [self createAtToPlaylistDoneButtonItem];
        [_tableView setEditing:YES animated:NO];
    }
    
    [self tintIcons];
    
    [self setupBannerAdIfNeeded];
    
    _tableView.allowsSelectionDuringEditing = YES;
    _tableView.hidesLoadMoreIndicator = YES;
    _tableView.rowHeight = 90;
    _tableView.delegate = (id)self;
    _tableView.dataSource = (id)self;
    
    FEAddObserver(self, kRootControllerDidSelectViewControllerNotification, @selector(rootControllerDidSelectViewController:));
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [kGlobal setAllPlaylistUnselected];
    [self reloadPlaylistData];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _bannerView.frame = CGRectMake(0, 0, self.view.width, 50);
    _bannerView.bottom = self.view.height;
    
    CGFloat tableViewHeight = self.view.height - (_bannerView.hidden ? 0 : _bannerView.height);
    _tableView.frame = CGRectMake(0, 0, self.view.width, tableViewHeight);
}

- (void)dealloc
{
    FERemoveObserver(self);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notification
- (void)rootControllerDidSelectViewController:(NSNotification*)notification
{
    UINavigationController *navigationController = notification.object;
    if (self.navigationController == navigationController) {
        [self.navigationController popToViewController:self animated:NO];
    }
}

#pragma mark - Tableview delegate & datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _sectionInfoList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SectionInfo *sectionInfo = _sectionInfoList[section];
    return sectionInfo.items.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    if (sectionInfo.tag == kSectionTagTool) {
        return _toolCell;
    }
    else if (sectionInfo.tag == kSectionTagPlaylist) {
        static NSString *kIdentifier = @"C";
        TrackCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifier];
        if (cell == nil) {
            cell = [[TrackCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kIdentifier];

            if (isRegularType) {
                cell.hidesSelectionBox = YES;
            }
        }
        
        Playlist *playlist = _playlists[indexPath.row];
        
        cell.textLabel.text = playlist.name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%d %@",
                                     playlist.numberOfTracks,
                                     (playlist.numberOfTracks>1?@"audios":@"audio")];

        NSURL *url = [NSURL URLWithString:[playlist lastTrack].iconURL];
        [cell.imageView sd_setImageWithURL:url placeholderImage:kDefaultPlaylistArtwork];
        
        cell.isSelected = (playlist.isSelected && _tableView.isEditing);
        
        //playing icon
        cell.showsPlayingIcon = (kGlobal.currentPlayingPlaylist == playlist);
        
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    if (sectionInfo.tag == kSectionTagTool) {
        return 50;
    }
    
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    if (sectionInfo.tag == kSectionTagTool) {
        if (_playlists.count >= [self playlistLimit]) {
            if ([kParamsManager integerForName:kParamNamePlaylistLimitType] == PlaylistLimitTypeAd) {
                [kGlobal showAlertAdWithResultHandler:^(BOOL confirmed) {
                    if (confirmed) {
                        FESetUserDefaults(kUDKeyHasAccessAdRestrictingPlaylist, @YES);
                    }
                }];
            }
            else {
                NSString *title = [kParamsManager valueForName:kParamNamePlaylistRateAlertTitle];
                NSString *message = [kParamsManager valueForName:kParamNamePlaylistRateAlertMessage];
                NSString *cancel = [kParamsManager valueForName:kParamNamePlaylistRateAlertCancelText];
                NSString *confirm = [kParamsManager valueForName:kParamNamePlaylistRateAlertConfirmText];
                
                FEAlertController *alert = [[FEAlertController alloc] initWithTitle:title message:message buttonTitles:@[confirm] cancelButtonTitle:cancel destructiveButtonTitle:nil preferredStyle:UIAlertControllerStyleAlert handler:^(FEAlertController *c, NSString *buttonTitle) {
                    
                    if (FEStringEqual(buttonTitle, confirm)) {
                        [kUtil openAppInAppStoreWithAppID:kAppID];
                        FESetUserDefaults(kUDKeyHasRate, @YES);
                    }
                }];
                [alert showInController:self animated:YES completion:nil];
            }
            
            return;
        }
        
        FEAlertController *alert = [[FEAlertController alloc] initWithTitle:@"" message:_(@"New Playlist") buttonTitles:@[_(@"OK")] cancelButtonTitle:_(@"Cancel") destructiveButtonTitle:nil preferredStyle:UIAlertControllerStyleAlert handler:^(FEAlertController *c, NSString *buttonTitle) {
            
            NSString *name = [c textFieldAtIndex:0].text;
            name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (FEStringEqual(buttonTitle, _(@"OK")) && name.length > 0) {
                Playlist *playlist = [[Playlist alloc] init];
                playlist.name = name;
                [playlist save];
                [_playlists addObject:playlist];
                [_tableView reloadData];
            }
        }];
        if ([alert.internal isKindOfClass:[UIAlertController class]]) {
            [alert.internal addTextFieldWithConfigurationHandler:nil];
        }
        else if ([alert.internal isKindOfClass:[UIAlertView class]]) {
            [alert.internal setAlertViewStyle:UIAlertViewStylePlainTextInput];
        }
        [alert showInController:self animated:YES completion:nil];
    }
    else if (sectionInfo.tag == kSectionTagPlaylist) {
        Playlist *playlist = _playlists[indexPath.row];
        
        if (isRegularType) {
            if (_tableView.isEditing == NO) {
                SongsViewController *c = [[SongsViewController alloc] initWithType:SongsViewControllerTypePlaylistDetails];
                c.playlist = playlist;
                [self.navigationController pushViewController:c animated:YES];
            }
            else { //重命名
                FEAlertController *alert = [[FEAlertController alloc] initWithTitle:@"" message:@"Rename" buttonTitles:@[@"OK"] cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil preferredStyle:UIAlertControllerStyleAlert handler:^(FEAlertController *c, NSString *buttonTitle) {
                    
                    NSString *newName = [c textFieldAtIndex:0].text;
                    if (FEStringEqual(buttonTitle, @"OK") &&
                        newName.length > 0 &&
                        FEStringEqual(newName, playlist.name) == NO
                    ) {
                        playlist.name = newName;
                        [playlist save];
                        [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                    }
                }];
                
                if ([alert.internal isKindOfClass:[UIAlertView class]]) {
                    [alert.internal setAlertViewStyle:UIAlertViewStylePlainTextInput];
                    
                }
                else if ([alert.internal isKindOfClass:[UIAlertController class]]) {
                    [alert.internal addTextFieldWithConfigurationHandler:nil];
                }
                UITextField *textField = [alert textFieldAtIndex:0];
                textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                textField.autocorrectionType = UITextAutocorrectionTypeNo;
                textField.text = playlist.name;
                textField.clearButtonMode = UITextFieldViewModeWhileEditing;
                
                [alert showInController:self animated:YES completion:nil];
            }
        }
        else if (isSelectionType) {
            Playlist *playlist = _playlists[indexPath.row];
            playlist.isSelected = !playlist.isSelected;
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    return (sectionInfo.tag != kSectionTagTool);
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCellEditingStyle r = UITableViewCellEditingStyleNone;
    if (isRegularType) {
        r = (_tableView.isEditing == NO ? UITableViewCellEditingStyleNone : UITableViewCellEditingStyleDelete);
    }
    else if (isSelectionType) {
        r = UITableViewCellEditingStyleNone;
    }
    
    return r;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[indexPath.section];
    return (sectionInfo.tag != kSectionTagTool);
}

- (NSIndexPath*)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    SectionInfo *sectionInfo = _sectionInfoList[proposedDestinationIndexPath.section];
    return (sectionInfo.tag == kSectionTagTool ? sourceIndexPath : proposedDestinationIndexPath);
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    if (sourceIndexPath.row != destinationIndexPath.row) {
        Playlist *playlist = _playlists[sourceIndexPath.row];
        [_playlists removeObjectAtIndex:sourceIndexPath.row];
        [_playlists insertObject:playlist atIndex:destinationIndexPath.row];
        
        NSInteger order = 1;
        for (Playlist *playlist in _playlists) {
            playlist.order = order;
            order++;
            [playlist save];
        }
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Playlist *playlist = _playlists[indexPath.row];
        [playlist delete];
        [_playlists removeObjectAtIndex:indexPath.row];
        [_tableView reloadData];
    }
}

#pragma mark - Handler (_type == PlayListViewControllerTypeRegular)
- (void)editBarButtonItemHandler:(UIBarButtonItem*)sender
{
    [_tableView setEditing:YES animated:YES];
    self.navigationItem.leftBarButtonItem = [self createEndEditingBarButtonItem];
}

- (void)endEditingBarButtonItemHandler:(UIBarButtonItem*)sender
{
    [_tableView setEditing:NO animated:YES];
    self.navigationItem.leftBarButtonItem = [self createEditBarButtonItem];
}

#pragma mark - Handler (_type == PlayListViewControllerTypeSelection)
- (void)cancelBarButtonItemHandler:(UIBarButtonItem*)sender
{
    if (_didCancelHandler) {
        _didCancelHandler(self);
    }
}

- (void)finishSelectingPlaylistsButtonItemHandler:(UIBarButtonItem*)sender
{
    if (_didSelectPlaylistsHandler) {
        NSMutableArray *selectedPlaylists = [NSMutableArray array];
        for (Playlist *playlist in _playlists) {
            if (playlist.isSelected) {
                [selectedPlaylists addObject:playlist];
            }
        }
        _didSelectPlaylistsHandler(selectedPlaylists, self);
    }
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    if (_bannerView.hidden) {
        _bannerView.hidden = NO;
        [self.view bringSubviewToFront:_bannerView];
        [self.view setNeedsLayout];
    }
}

#pragma mark - Private
- (void)reloadPlaylistData
{
    NSArray *playlists = [[Playlist allRecords] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        Playlist *playlist1 = obj1;
        Playlist *playlist2 = obj2;
        if (playlist1.order <= playlist2.order) {
            return NSOrderedAscending;
        }
        return NSOrderedDescending;
    }];
    _playlists = [playlists mutableCopy];

    _sectionInfoList = [NSMutableArray array];
    [_sectionInfoList addObject:newSectionInfo(@"", @[@""], kSectionTagTool)];
    [_sectionInfoList addObject:newSectionInfo(@"", _playlists, kSectionTagPlaylist)];
    [_tableView reloadData];
}

- (UIBarButtonItem*)createEditBarButtonItem
{
    return [self barButtonItem:@"EDIT" selector:@selector(editBarButtonItemHandler:)];
}

- (UIBarButtonItem*)createEndEditingBarButtonItem
{
    return [self barButtonItem:@"DONE" selector:@selector(endEditingBarButtonItemHandler:)];
}

- (UIBarButtonItem *)createAtToPlaylistCancelButtonItem {
    return [self barButtonItem:@"CANCEL" selector:@selector(cancelBarButtonItemHandler:)];
}

- (UIBarButtonItem *)createAtToPlaylistDoneButtonItem {
    return [self barButtonItem:@"DONE" selector:@selector(finishSelectingPlaylistsButtonItemHandler:)];
}

- (UIBarButtonItem *)barButtonItem:(NSString *)title selector:(SEL)selector {
    float targetImageHeight = 26;
    UIView *container = [[UIView alloc] init];
    
    UILabel *label = [[UILabel alloc] init];
    [container addSubview:label];
    label.attributedText = [[NSAttributedString alloc] initWithString:title attributes:[kGlobal navigationBarItemAttribute]];
    [label sizeToFit];
    label.y = (targetImageHeight - label.height) / 2.0;
    
    container.frame = CGRectMake(0, 0, label.right, targetImageHeight);
    UIImage *image = [kUtil viewToImage:container scale:0];
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStylePlain target:(id)self action:selector];
    item.imageInsets = UIEdgeInsetsMake(0, -5, 0, 5);
    return item;
}

- (void)setupBannerAdIfNeeded
{
    _bannerView.hidden = YES;
    
    if (isRegularType && [kParamsManager boolForName:kParamNameShowsBannerAd]) {
        _bannerView.adUnitID = kGlobal.bannerUnitID;
        _bannerView.delegate = (id)self;
        _bannerView.rootViewController = self;
        _bannerView.autoloadEnabled = YES;
        [_bannerView loadRequest:[GADRequest request]];
    }
}

- (NSInteger)playlistLimit {
    PlaylistLimitType limitType = (PlaylistLimitType)[kParamsManager integerForName:kParamNamePlaylistLimitType];
    NSInteger limit = [kParamsManager integerForName:kParamNamePlaylistLimit];
    
    if (limit == 0 ||
        (limitType == PlaylistLimitTypeAd && [ADManager alertAdInfoList].count == 0) ||
        FEUserDefaultsBoolForKey(kUDKeyHasRate) ||
        FEUserDefaultsBoolForKey(kUDKeyHasAccessAdRestrictingPlaylist)
        ) {
        return NSIntegerMax;
    }
    
    return limit;
}

- (void)tintIcons {
    UIImage *addIcon = _creationImageView.image;
    addIcon = [kUtil tintImage:addIcon withColor:kGlobal.tintColor];
    _creationImageView.image = addIcon;
}

@end

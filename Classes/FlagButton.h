//
//  StateButton.h
//  Drive
//
//  Created by EvilNOP on 15/3/30.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlagButton : UIButton

@property (nonatomic) int flag;
@property (nonatomic) BOOL autoChangeFlag;

- (void)addFlag:(int)flag icon:(UIImage*)icon;

@end

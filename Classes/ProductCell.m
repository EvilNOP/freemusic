//
//  ProductCell.m
//  helo
//
//  Created by EvilNOP on 14-1-5.
//  Copyright (c) 2014年 Fire. All rights reserved.
//

#import "ProductCell.h"

#define kRatingImageWidth 200
#define kRatingImageHeight 13
#define kStarSideLength kRatingImageHeight

@implementation ProductCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone; 
        
        _headerBackgroundView = [[UIView alloc] init];
        _headerBackgroundView.backgroundColor = FEHexRGBA(0xffffff, 1.0);
        [self.contentView addSubview:_headerBackgroundView];
        
        [super setBackgroundColor:FEHexRGBA(0xeeeeee, 1.0)];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:13];
        _titleLabel.adjustsFontSizeToFitWidth = YES;
        _titleLabel.textColor = FEHexRGBA(0x2e2e2e, 1.0);
        [self.contentView addSubview:_titleLabel];
        
        _detailsLabel = [[UILabel alloc] init];
        _detailsLabel.font = [UIFont systemFontOfSize:11];
        _detailsLabel.textColor = FEHexRGBA(0x555555, 1.0);
        _detailsLabel.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_detailsLabel];
        
        _iconView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconView];
        _iconView.layer.cornerRadius = 8.0;
        _iconView.layer.masksToBounds = YES;
        
        _ratingImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_ratingImageView];
        
        _downloadButton = [[UIButton alloc] init];
        _downloadButton.layer.borderWidth = 1.0;
        _downloadButton.layer.borderColor = FEHexRGBA(0x007aff, 1.0).CGColor;
        _downloadButton.layer.cornerRadius = 3.0;
        [_downloadButton setTitleColor:FEHexRGBA(0x007aff, 1.0) forState:UIControlStateNormal];
        [_downloadButton setTitleColor:FEHexRGBA(0x0061ca, 1.0) forState:UIControlStateHighlighted];
        _downloadButton.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        _downloadButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        _downloadButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_downloadButton];
        
        _screenShotScrollView = [[ScreenShotScrollView alloc] init];
        [self.contentView addSubview:_screenShotScrollView];
        
        _descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.font = [UIFont systemFontOfSize:12];
        _descriptionLabel.textColor = FEHexRGBA(0x555555, 1.0);
        _descriptionLabel.adjustsFontSizeToFitWidth = YES;
        _descriptionLabel.numberOfLines = 0;
        [self.contentView addSubview:_descriptionLabel];
        
        _status = kProductCellStatusShrink;
        
        _topShadow = [CAGradientLayer layer];
        _topShadow.startPoint = CGPointMake(0.5, 0.0);
        _topShadow.endPoint = CGPointMake(0.5, 1.0);
        _topShadow.colors = @[(id)FEHexRGBA(0x3e3c3c, 0.2).CGColor, (id)[UIColor clearColor].CGColor];
        _topShadow.locations = @[@0.0, @1.0];
        _topShadow.hidden = YES;
        [self.layer addSublayer:_topShadow];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([kUtil iosVersion] >= 7.0) {
        self.separatorInset = UIEdgeInsetsMake(0, 3, 0, 3);
    }
    
    _headerBackgroundView.frame = CGRectMake(0, 0, self.contentView.width, kProductCellShrinkedHeight);
    
    float iconMarginLeft = 8, iconMarginTop = 6, iconMarginBottom = 6;
    float iconHeight = kProductCellShrinkedHeight-iconMarginTop-iconMarginBottom;
    float iconWidth = iconHeight;
    _iconView.frame = CGRectMake(iconMarginLeft,
                                 iconMarginTop,
                                 iconWidth,
                                 iconHeight);
    
    float btnWidth = 55, btnHeight = 30, btnMarginRight = 10;
    _downloadButton.frame = CGRectMake(0, 0, btnWidth, btnHeight);
    _downloadButton.center = CGPointMake(self.contentView.width - btnWidth/2.0 - btnMarginRight,
                                         kProductCellShrinkedHeight/2.0);
    
    float labelX = _iconView.right + 6;
    _titleLabel.frame = CGRectMake(labelX,
                                   iconMarginTop-3,
                                   _downloadButton.x-labelX, 25);
    
    _detailsLabel.frame = CGRectMake(labelX,
                                     _titleLabel.bottom-10,
                                     _titleLabel.width,
                                     kProductCellShrinkedHeight-_titleLabel.bottom-iconMarginBottom);
    
    _ratingImageView.frame = CGRectMake(labelX,
                                        _detailsLabel.bottom-5,
                                        kRatingImageWidth,
                                        kRatingImageHeight);
    
    if (_status == kProductCellStatusExpand) {
        _screenShotScrollView.frame = CGRectMake(0,
                                                 kProductCellShrinkedHeight,
                                                 self.contentView.width,
                                                 kProductCellExpandedScreenshotScrollViewHeight);
        
        float descriptionLabelMarginLeft = 7, descriptionLabelMarginRight = 7;
        _descriptionLabel.frame = CGRectMake(
                                             descriptionLabelMarginLeft,
                                             _screenShotScrollView.bottom,
                                             self.contentView.width - descriptionLabelMarginLeft - descriptionLabelMarginRight,
                                             kProductCellExpandedDescriptionHeight);
    }
    else {
        _screenShotScrollView.frame = CGRectZero;
        _descriptionLabel.frame = CGRectZero;
    }
    
    float shadowHeight = 3;
    _topShadow.frame = CGRectMake(0,
                                  _headerBackgroundView.bottom,
                                  self.width,
                                  shadowHeight);
}

#pragma mark - Overwrite
- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    //
}

#pragma mark - Private
- (UIImage*)drawRatingImageWithRating:(float)rating numberOfReviews:(int)numberOfReviews
{
    //Star
    UIImage *star = [UIImage imageNamed:@"Star.png"];
    UIImage *halfStar = [UIImage imageNamed:@"HalfStar.png"];
    
    //Rating
    BOOL decimalEqualToZero = YES;
    int floorRating = floorf(rating);
    int ceilRating = ceilf(rating);
    if (floorRating != rating) { //小数部分不为0
        decimalEqualToZero = NO;
        rating = floorRating + 0.5;
    }
    
    //Draw
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kRatingImageWidth, kRatingImageHeight)];
    if (numberOfReviews > 0) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(ceilRating*kStarSideLength+5, 0,
                                                                   90, kRatingImageHeight)];
        label.textColor = FEHexRGBA(0x555555, 1.0);
        label.font = [UIFont systemFontOfSize:kRatingImageHeight-2];
        label.adjustsFontSizeToFitWidth = YES;
        label.text = [NSString stringWithFormat:@"(%d)", numberOfReviews];
        [v addSubview:label];
    }
    
    UIGraphicsBeginImageContextWithOptions(v.frame.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [v.layer renderInContext:context];
    
    for (int i=1; i<=ceilRating; i++) {
        BOOL theLastStar = (i == ceilRating);
        if (theLastStar && decimalEqualToZero == NO) {
            [halfStar drawInRect:CGRectMake((i-1)*kStarSideLength, 0, kStarSideLength, kStarSideLength)];
        }
        else {
            [star drawInRect:CGRectMake((i-1)*kStarSideLength, 0, kStarSideLength, kStarSideLength)];
        }
    }
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - Public
- (void)expand
{
    [_screenShotScrollView loadData];
    _status = kProductCellStatusExpand;
    _topShadow.hidden = NO;
}

- (void)shrink
{
    [_screenShotScrollView unloadData];
    _status = kProductCellStatusShrink;
    _topShadow.hidden = YES;
}

- (void)setScreenShotScrollViewDatasource:(id<ScreenShotScrollViewDatasource>)datasource
{
    _screenShotScrollView.datasource = datasource;
}

- (void)setRating:(float)rating numberOfReviews:(int)numberOfReviews
{
    UIImage *image = [self drawRatingImageWithRating:rating numberOfReviews:numberOfReviews];
    _ratingImageView.image = image;
}

@end

//
//  OneDriveListView.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/3/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import UIKit
import OneDriveSDK
import Alamofire
import MBProgressHUD
import MJRefresh

class OneDriveListView: UIView {
    
    let oneDriveClientTime = "oneDriveClientTime"
    let timeInterval = 1800

    var parentsViewController: UIViewController!
    var cellSelectedHanlder: ((index: NSInteger, isFolder: Bool, file: ODItem) -> Void)?
    var cancelSignInHandler: (() -> Void)?
    
    var currentFile: ODItem?
    
    private var noLoginView: NoLoginView?
    private let noSearchingView = NoSearchingView()
    private var isSearchedViewController = false
    private let cancelLoginErrorCode = 3
    private let oneDriveAPI = "https://api.onedrive.com/v1.0"
    private var pageSize = 20
    private var nextLink = ""
    private var lastRequest: Alamofire.Request?
    private let unauthorized = 401
    private var searchPageSize = 20
    private var searchNextLink = ""
    private var searchRequest: Alamofire.Request?
    
    private let tableView = UITableView(frame: CGRectZero, style: .Plain)
    private let searchBar = UISearchBar()
    
    var accessToken: String?
    
    enum ButtonFlag: Int32 {
        case Add, Added
    }
    
    var client: ODClient? {
        didSet {
            accessToken = client!.authProvider.accountSession?().accessToken
        }
    }
    
    private var items: [ODItem] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    private var searchItems: [ODItem]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    func isVerified() -> Bool {
        return ODClient.loadCurrentClient() != nil
    }
    
    func verify() {
        guard parentsViewController != nil else { return }
        reAuthenticate()
    }
    
    class func remoeAuthorizer() {
        if let currentClient = ODClient.loadCurrentClient() {
            currentClient.signOutWithCompletion({ error in
                if error != nil {
                    print(error.localizedDescription)
                }
            })
        }
    }
    
    convenience init(controller: UIViewController, searching: Bool) {
        self.init()
        
        parentsViewController = controller
        isSearchedViewController = searching
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.whiteColor()
        tableView.rowHeight = 70
        
        searchBar.barStyle = .Default
        searchBar.returnKeyType = .Search
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        
        noLoginView = NoLoginView(loginHandler: {
            self.verify()
        })
        
        let isVerified = self.isVerified()
        
        if isVerified {
            self.loadCurrentClient()
        }
        
        if isSearchedViewController {
            if isVerified {
                tableView.tableFooterView = noSearchingView
                tableView.scrollEnabled = false
            } else {
                tableView.tableFooterView = noLoginView
                tableView.scrollEnabled = false
            }
        } else {
            tableView.tableFooterView = UIView()
        }
        
        self.addSubview(searchBar)
        self.addSubview(tableView)
    }
    
    init () {
        super.init(frame: CGRectZero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        searchBar.frame = CGRectMake(0, 0, self.width, 44)
        tableView.frame = CGRectMake(0, searchBar.height, self.width, self.height - searchBar.height)
        
        noLoginView!.frame = CGRectMake(0, 0, self.width, self.height - searchBar.height)
        noSearchingView.frame = CGRectMake(0, 0, self.width, self.height - searchBar.height)
    }
}

extension OneDriveListView: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.showsSearchItems() {
            return searchItems!.count
        } else {
            return items.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let identifier: String = "Cell"
        
        var optionalCell = tableView.dequeueReusableCellWithIdentifier(identifier) as? TrackCell
        if optionalCell == nil {
            optionalCell = TrackCell(style: .Default, reuseIdentifier: identifier)
            
            let accessoryButton = FlagButton()
            accessoryButton.autoChangeFlag = false
            accessoryButton.size = CGSize(width: 40, height: self.tableView.rowHeight)
            accessoryButton.addFlag(ButtonFlag.Add.rawValue, icon: UIImage(named: "Add"))
            accessoryButton.addFlag(ButtonFlag.Added.rawValue, icon: UIImage(named: "Added"))
            accessoryButton.addTarget(self, action: #selector(addButtonTapped(_:)), forControlEvents: .TouchUpInside)
            
            optionalCell?.accessoryView = accessoryButton
        }
        
        let cell = optionalCell!
        
        var file: ODItem
        
        if showsSearchItems() {
            file = searchItems![indexPath.row]
        } else {
            file = items[indexPath.row]
        }
        
        if file.folder != nil {
            cell.accessoryType = .DisclosureIndicator
            cell.imageView?.image = UIImage(named: "File")
            cell.accessoryView?.hidden = true
        } else {
            cell.accessoryType = .None
            cell.imageView?.image = UIImage(named: "Music")
            
            let accessoryButton = cell.accessoryView as! FlagButton
            accessoryButton.hidden = false
            accessoryButton.flag = (Track.existedWithID(file.id) ? ButtonFlag.Added.rawValue : ButtonFlag.Add.rawValue)
        }
        
        cell.textLabel?.attributedText = NSAttributedString(string: file.name, attributes: Global.sharedInstance().thirdFontAttribute() as? [String: AnyObject])
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var file: ODItem
        
        if showsSearchItems() {
            file = searchItems![indexPath.row]
        } else {
            file = items[indexPath.row]
        }
        
        if file.folder != nil {
            if isSearchedViewController == false {
                if let handler = cellSelectedHanlder {
                    handler(index: indexPath.row, isFolder: file.folder != nil, file: file)
                }
            }
        }
        else {
            var tracks = [Track]()
            var index = 0
            for item in (self.searchItems ?? self.items) {
                if item.folder != nil {
                    continue
                }
                
                let track = kOneDriveSource.createTrackWithItem(item)
                tracks.append(track)
                
                if item == file {
                    index = tracks.count - 1
                }
            }
            
            kGlobal.enqueuTracks(tracks, andPlayTrackAtIndex: index, shuffled: false, rootNavigationController: self.parentsViewController.navigationController, playlist: nil)
        }
    }
}

// MARK: - Searchbar delegate
extension OneDriveListView: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        if isVerified() {
            return true
        }
        
        self.tableView.tableFooterView = self.noLoginView
        showAlert("", message: "Please login")
        return false
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        
        if self.tableView.mj_footer != nil {
            self.tableView.mj_footer.hidden = true
        }
        
        tableView.tableFooterView = UIView()
        tableView.scrollEnabled = true
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if isSearchedViewController == false && searchBar.text != "" {
            let queryText = searchBar.text!
            if isVerified() {
                NSObject.cancelPreviousPerformRequestsWithTarget(self, selector: #selector(querylocalItemWithText), object: queryText)
                performSelector(#selector(querylocalItemWithText), withObject: queryText, afterDelay: 0.5)
            }
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if searchBar.text != "" {
            let queryText = searchBar.text!
            if isVerified() {
                if isSearchedViewController {
                    searchFile(queryText)
                } else {
                    querylocalItemWithText(queryText)
                }
            }
        }
        
        kGlobal.increaseSearchTimesAndProcess()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        if let request = self.searchRequest {
            request.cancel()
            if self.tableView.mj_footer != nil && self.tableView.mj_footer.isRefreshing() {
               self.tableView.mj_footer.endRefreshing()
            }
        }
        
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchItems = nil
        
        if isSearchedViewController {
            tableView.tableFooterView = noSearchingView
            tableView.scrollEnabled = false
        } else {
            tableView.tableFooterView = UIView()
            tableView.scrollEnabled = true
        }
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        
        if searchItems == nil || searchItems!.count == 0 {
            tableView.tableFooterView = noSearchingView
            tableView.scrollEnabled = false
        }
    }
}

extension OneDriveListView {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if self.searchBar.isFirstResponder() {
            self.searchBar.resignFirstResponder()
        }
    }
}

extension OneDriveListView {
    @objc private func addButtonTapped(sender: FlagButton) {
        let cell = kUtil.findSuperViewWithClass(TrackCell.self, subview: sender) as! TrackCell
        let indexPath = self.tableView.indexPathForCell(cell)
        let file = (self.searchItems ?? self.items)[indexPath!.row]
        
        if Track.existedWithID(file.id) == false {
            let track = kOneDriveSource.createTrackWithItem(file)
            track.save()
            sender.flag = ButtonFlag.Added.rawValue
        }
    }
}

// MARK: - Private
extension OneDriveListView {
    
    func loadCurrentClient() {
        self.client = ODClient.loadCurrentClient();
    }
    
    func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .Alert
        )
        
        let ok = UIAlertAction(
            title: "OK",
            style: .Default,
            handler: nil
        )
        
        alert.addAction(ok)
        parentsViewController.presentViewController(alert, animated: true, completion: nil)
    }
    
    func reAuthenticate() {
        ODClient.clientWithCompletion { (currentClient, error) in
            if (error == nil) {
                ODClient.setCurrentClient(currentClient)
                self.client = currentClient
                
                dispatch_sync(dispatch_get_main_queue(), {
                    if self.isSearchedViewController {
                        self.tableView.tableFooterView = self.noSearchingView
                    } else {
                        self.loadChildren(nil)
                    }
                })
            } else {
                if error.code == self.cancelLoginErrorCode {    // user cancel sign in
                    if let handler = self.cancelSignInHandler {
                        handler()
                    }
                }
            }
        }
    }
    
    func loadChildren(file: ODItem?) {
        
        setTimeout(0.1) {   // Wait the login view to dismiss
            MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)
        }
        var identifier = "root"
        
        if let item = file {
            identifier = item.id
        }
        
        if let token = accessToken {
            let headers = ["Authorization":"Bearer \(token)"]
            let url = "\(self.oneDriveAPI)/drive/items/\(identifier)/children"
            
            let urlParams = [
                "top":self.pageSize,
                "filter":"folder ne null or audio ne null"
                ]
            
            Alamofire.request(.GET, url, headers: headers, parameters: urlParams as? [String : AnyObject]).validate(statusCode: 200..<500).responseJSON { (response) in
                
                guard response.result.isSuccess else {
                    print(response.result.error?.code);
                    
                    if response.result.error?.code == 401 {
                        dispatch_async(dispatch_get_main_queue(), { 
                            MBProgressHUD.hideHUDForView(self.tableView, animated: true)
                        })
                        OneDriveListView.remoeAuthorizer()
                        self.reAuthenticate()
                    }
                    
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: AnyObject] else { print("Invalid tag information"); return }
                
                if let error = responseJSON["error"] {
                    let e = error as! [String: AnyObject]
                    if e["code"] as! String == "unauthenticated" {
                        OneDriveListView.remoeAuthorizer()
                        self.reAuthenticate()
                        return
                    }
                }
                
                if let values = responseJSON["value"] {
                    
                    var array: [ODItem] = []
                    
                    for value in values as! [AnyObject] {
                        let item = ODItem(dictionary: value as! [NSObject: AnyObject])
                        array.append(item)
                    }
                    
                    self.items.appendContentsOf(array)
                    self.tableView.reloadData()
                    
                    if (self.tableView.mj_footer == nil) {
                        self.tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(self.loadNextPage))
                    }
                    
                    if let nextLink = responseJSON["@odata.nextLink"] as? String {
                        self.nextLink = nextLink
                    } else {
                        self.nextLink = ""
                        if self.tableView.mj_footer != nil {
                            self.tableView.mj_footer.hidden = true
                        }
                    }
                    
                    MBProgressHUD.hideHUDForView(self.tableView, animated: true)
                }
            }
        }
    }
    
    func searchFile(query: String) {
        print(accessToken)
        MBProgressHUD.showHUDAddedTo(self.tableView, animated: true)
        if let token = accessToken {
            let headers = ["Authorization":"Bearer \(token)"]
            let url = "\(self.oneDriveAPI)/drive/root/view.search"
            
            let urlParams = [
                "q":query.lowercaseString,
                "filter":"contains(tolower(name), '\(query.lowercaseString)') and audio ne null",
                "top":self.searchPageSize,
                ]
            
            self.searchRequest = Alamofire.request(.GET, url, headers: headers, parameters: urlParams as? [String : AnyObject]).validate(statusCode: 200..<300).responseJSON { (response) in
                MBProgressHUD.hideHUDForView(self.tableView, animated: true)
                guard response.result.isSuccess else {
                    print(response.result.error?.description);
                    if response.result.error?.description.containsString("\(self.unauthorized)") == true {
                        OneDriveListView.remoeAuthorizer()
                        self.reAuthenticate()
                    }
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: AnyObject] else { print("Invalid tag information"); return }
                self.searchItems = []
                self.searchResultOperator(responseJSON)
            }
        }
    }
    
    func querylocalItemWithText(queryText: String) {
        if items.count != 0 {
            let regx = Global.sharedInstance().regxWithText(queryText)
            let predicate = NSPredicate(format: "SELF LIKE[cd] %@", regx)
            searchItems = items.filter { predicate.evaluateWithObject($0.name) }
        }
    }
    
    func loadNextPage() {
        if (self.tableView.mj_footer != nil && self.tableView.mj_footer.isRefreshing()) {
            self.tableView.mj_footer.endRefreshing()
        }
        
        if let request = self.lastRequest {
            request.cancel()
        }
        
        if self.nextLink == "" && self.tableView.mj_footer != nil {
            self.tableView.mj_footer.hidden = true
        }
        
        if self.nextLink != "" {
            if let token = accessToken {
                let headers = ["Authorization":"Bearer \(token)"]
                
                self.lastRequest = Alamofire.request(.GET, self.nextLink, headers: headers, parameters: nil).validate(statusCode: 200..<500).responseJSON { (response) in
                    
                    guard response.result.isSuccess else {
                        print(response.result.error?.localizedDescription);
                        
                        if response.result.error?.code == 401 {
                            OneDriveListView.remoeAuthorizer()
                            self.reAuthenticate()
                        }
                        
                        return
                    }
                    
                    guard let responseJSON = response.result.value as? [String: AnyObject] else { print("Invalid tag information"); return }
                    print(responseJSON)
                    
                    if responseJSON["@odata.nextLink"] != nil {
                        self.nextLink = responseJSON["@odata.nextLink"] as! String
                    } else {
                        self.nextLink = ""
                        if self.tableView.mj_footer != nil {
                            self.tableView.mj_footer.hidden = true
                        }
                    }
                    
                    var tmp: [ODItem] = []
                    for item in responseJSON["value"] as! [AnyObject] {
                        let odItem = ODItem(dictionary: item as! [NSObject: AnyObject])
                        if odItem.folder != nil || odItem.audio != nil {
                            tmp.append(odItem)
                        }
                    }
                    self.items.appendContentsOf(tmp)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func searchNextPage() {
        if self.searchNextLink != "" {
            print(true)
            if let token = accessToken {
                let headers = ["Authorization":"Bearer \(token)"]
                let url = self.searchNextLink
                
                self.searchRequest = Alamofire.request(.GET, url, headers: headers, parameters: nil).validate(statusCode: 200..<300).responseJSON { (response) in
                    
                    guard response.result.isSuccess else {
                        print(response.result.error?.description);
                        if response.result.error?.description.containsString("\(self.unauthorized)") == true {
                            OneDriveListView.remoeAuthorizer()
                            self.reAuthenticate()
                        }
                        return
                    }
                    
                    guard let responseJSON = response.result.value as? [String: AnyObject] else { print("Invalid tag information"); return }
                    print(responseJSON)
                    self.searchResultOperator(responseJSON)
                }
            }
        } else {
            self.tableView.mj_footer.hidden = true
        }
    }
    
    func searchResultOperator(responseJSON: [String: AnyObject]) {
        var tmp: [ODItem] = []
        
        for item in responseJSON["value"] as! [AnyObject] {
            let odItem = ODItem(dictionary: item as! [NSObject: AnyObject])
            tmp.append(odItem)
        }
        self.searchItems?.appendContentsOf(tmp)

        if let nextLink = responseJSON["@odata.nextLink"] as? String {
            self.searchNextLink = nextLink
        } else {
            self.searchNextLink = ""
        }
        
        if self.tableView.mj_footer == nil {
            self.tableView.mj_footer =  MJRefreshAutoNormalFooter(refreshingTarget: self, refreshingAction: #selector(self.searchNextPage))
        } else {
            self.tableView.mj_footer.endRefreshing()
        }
        
        self.tableView.mj_footer.hidden = (self.searchNextLink == "")

    }
    
    func showsSearchItems() -> Bool {
        return self.searchItems != nil
    }
    
    class func currentTime() -> Int {
        let timeStamp = Int(NSDate().timeIntervalSince1970)
        return timeStamp
    }
}

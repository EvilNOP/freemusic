//
//  FileDetector.m
//  Coolplayer
//
//  Created by EvilNOP on 6/16/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "FileDetector.h"

@interface FileDetector()

@property (strong, nonatomic) NSString *detectingPath;
@property (strong, nonatomic) FileDetectorDetectingHandler detectingHandler;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSMutableDictionary *filePathToAttributes;

@end

@implementation FileDetector

+ (id)detectFilesAtPath:(NSString *)path detectingHandler:(FileDetectorDetectingHandler)handler {
    FileDetector *detector = [[FileDetector alloc] init];
    detector.detectingPath = path;
    detector.detectingHandler = handler;

    return detector;
}

- (id)init {
    self = [super init];
    if (self) {
        self.detectingInterval = 1.5;
        self.filePathToAttributes = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (void)run {
    [self.timer invalidate];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.detectingInterval target:self selector:@selector(timerHandler:) userInfo:nil repeats:YES];
    [self.timer fire];
}

- (void)stop {
    [self.timer invalidate];
}

#pragma mark - Handler
- (void)timerHandler:(NSTimer*)sender {
    NSDirectoryEnumerator *enumerator = [kFM enumeratorAtPath:self.detectingPath];
    NSString *subPath;
    BOOL fileChanged = false;
    NSMutableDictionary *newSubPathToAttributes = [NSMutableDictionary dictionary];
    
    while (subPath = [enumerator nextObject]) {
        if (self.ignoreFilesBlock) {
            if (self.ignoreFilesBlock(self, subPath)) {
                continue;
            }
        }
        
        NSString *fullPath = [kUtil join:self.detectingPath, subPath, nil];
        NSDictionary *attributes = [kFM attributesOfItemAtPath:fullPath error:nil];
        
        NSDate *date = attributes[NSFileModificationDate];
        NSDictionary *originalAttributes = _filePathToAttributes[subPath];
        
        if (originalAttributes == nil || [originalAttributes[NSFileModificationDate] isEqual:date] == false) {
            fileChanged = true;
        }
        
        newSubPathToAttributes[subPath] = attributes;
        [_filePathToAttributes removeObjectForKey:subPath];
    }
    
    if (_filePathToAttributes.count > 0) {
        fileChanged = true;
    }
    
    self.detectingHandler(self, fileChanged, newSubPathToAttributes);
    
    _filePathToAttributes = newSubPathToAttributes;
}

@end

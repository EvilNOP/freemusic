//
//  PlayerViewController.m
//  Drive
//
//  Created by EvilNOP on 15/3/15.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "PlayerViewController.h"
#import "FlagButton.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Slider.h"
#import "CBAutoScrollLabel.h"
#import "TrackCell.h"
#import "FreeMusic-Swift.h"
#import "SampleQueueId.h"
#import "STKAudioPlayer.h"

enum {
    kFlagRepeatNone,
    kFlagRepeatAll,
    kFlagRepeatOne
};

enum {
    kFlagPaused,
    kFlagPlaying
};

enum {
    kFlagShuffleOff,
    kFlagShuffleOn
};

enum {
    kFlagClockDisabled,
    kFlagClockEnabled
};

static MPMusicPlayerController *playerForSettingVolume = nil;

@interface PlayerViewController () <UITableViewDataSource, UITableViewDelegate, STKAudioPlayerDelegate> {
    __weak IBOutlet UIImageView *_artworkImageView;
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UIView *_container;
    
    __weak IBOutlet CBAutoScrollLabel *_trackTitleLabel;
    __weak IBOutlet UILabel *_currentTimeLabel;
    __weak IBOutlet UILabel *_durationLabel;
    __weak IBOutlet FlagButton *_repeatButton;
    __weak IBOutlet FlagButton *_shuffleButton;
    __weak IBOutlet UIImageView *_minVolumeImageView;
    __weak IBOutlet UIImageView *_maxVolumeImageView;
    __weak IBOutlet Slider *_volumeSlider;
    __weak IBOutlet Slider *_progressSlider;
    __weak IBOutlet FlagButton *_playOrPauseButton;
    __weak IBOutlet FlagButton *_clockButton;
    __weak IBOutlet UIButton *_backwordButton;
    __weak IBOutlet UIButton *_forwardButton;
    
    NSTimer *_timer;
    BOOL _showsTrackList;
    NSUInteger _currentIndex;
    
    NSDate *_timeForStoppingPlaying;
    
    STKAudioPlayer *_audioPlayer;
    
    MPMusicPlayerController *_iTunesMusicPlayer;
    BOOL _isUsingItunesMusicPlayer;
}

@end

@implementation PlayerViewController

+ (void)initialize {
    playerForSettingVolume = [[MPMusicPlayerController alloc] init];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    FEScreenInch inch = [kUtil screenInch];
    if (inch == FEScreenInch3_5) {
        self.edgesForExtendedLayout = UIRectEdgeAll;
        _volumeSlider.hidden = YES;
        _minVolumeImageView.hidden = YES;
        _maxVolumeImageView.hidden = YES;
    }
    else {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    float bottomMargin = 0;
    switch (inch) {
        case FEScreenInch3_5:
            bottomMargin = -10;
            break;
        case FEScreenInch4_0:
            bottomMargin = -35;
            break;
        case FEScreenInch4_7:
            bottomMargin = -53;
            break;
        default:
            bottomMargin = -63;
            break;
    }
    [self.view addSubview:_minVolumeImageView];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [btn addTarget:(id)self action:@selector(trackListButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self makeupTrackListButton];
    
    [_playOrPauseButton addFlag:kFlagPaused icon:[UIImage imageNamed:@"Play"]];
    [_playOrPauseButton addFlag:kFlagPlaying icon:[UIImage imageNamed:@"Pause"]];
    _playOrPauseButton.autoChangeFlag = NO;
    [_playOrPauseButton addTarget:(id)self action:@selector(playOrPauseButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    [_repeatButton addFlag:kFlagRepeatNone icon:[UIImage imageNamed:@"RepeatNone"]];
    [_repeatButton addFlag:kFlagRepeatAll icon:[kUtil tintImage:[UIImage imageNamed:@"RepeatAll"] withColor:kGlobal.tintColor]];
    [_repeatButton addFlag:kFlagRepeatOne icon:[UIImage imageNamed:@"RepeatOne"]];
    _repeatButton.flag = (int)FEUserDefaultsIntegerForKey(kUDKeyRepeatMode);
    [_repeatButton addTarget:(id)self action:@selector(repeatButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    [_shuffleButton addFlag:kFlagShuffleOff icon:[UIImage imageNamed:@"ShuffleDisabled"]];
    [_shuffleButton addFlag:kFlagShuffleOn icon:[kUtil tintImage:[UIImage imageNamed:@"ShuffleEnabled"] withColor:kGlobal.tintColor]];
    [self setIsShuffled:_isShuffled];
    [_shuffleButton addTarget:(id)self action:@selector(shuffleButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    [_clockButton addFlag:kFlagClockDisabled icon:[UIImage imageNamed:@"ClockDisabled"]];
    [_clockButton addFlag:kFlagClockEnabled icon:[kUtil tintImage:[UIImage imageNamed:@"ClockEnabled"] withColor:kGlobal.tintColor]];
    _clockButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [_clockButton setTitleColor:kGlobal.tintColor forState:UIControlStateNormal];
    _clockButton.autoChangeFlag = NO;
    [_clockButton addTarget:(id)self action:@selector(clockButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    
    _volumeSlider.backgroundColor = [UIColor clearColor];
    _volumeSlider.trackHeight = 4;
    [_volumeSlider setProgressColor:FEHexRGBA(0x444444, 1.0)];
    _volumeSlider.value = playerForSettingVolume.volume;
    [_volumeSlider setHandleIcon:[UIImage imageNamed:@"Handle2"]];
    _volumeSlider.valueChangedhandler = ^(Slider *slider) {
        playerForSettingVolume.volume = slider.value;
    };
    
    _progressSlider.backgroundColor = [UIColor clearColor];
    [_progressSlider setProgressColor:kGlobal.tintColor];
    UIImage *handleIcon = [kUtil tintImage:[UIImage imageNamed:@"Handle"] withColor:kGlobal.tintColor];
    [_progressSlider setHandleIcon:handleIcon];
    _progressSlider.trackHeight = 5;
    _progressSlider.minValue = 0;
    _progressSlider.maxValue = 1;
    _progressSlider.valueChangedhandler = ^(Slider *slider) {
        if (slider.dragging) {
            _currentTimeLabel.text = [kGlobal formatSeconds:_progressSlider.value * [self duration]];
        }
        else {
            [self seekToTime:slider.value * [self duration]];
        }
    };
    
    _trackTitleLabel.text = @"";
    _trackTitleLabel.font = [UIFont boldSystemFontOfSize:18];
    _trackTitleLabel.textColor = FEHexRGBA(0x383838, 1.0);
    _trackTitleLabel.pauseInterval = 2;
    _trackTitleLabel.fadeLength = 20;
    _trackTitleLabel.scrollSpeed = 30;
    _trackTitleLabel.labelSpacing = 60;
    _trackTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self updatePlayOrPauseButton];
    [self performSelector:@selector(updateUIForCurrentTrack) withObject:nil afterDelay:0.1];

    _tableView.delegate = (id)self;
    _tableView.dataSource = (id)self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.rowHeight = 50;
    _tableView.backgroundColor = FEHexRGBA(0xf8f8f8, 1.0);
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timerHandler:) userInfo:nil repeats:YES];
    [_timer fire];
    
    FEAddObserver(self, @"AVSystemController_SystemVolumeDidChangeNotification", @selector(volumeDidChange:));
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [kGlobal increasePlayTimesAndProcess];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    _container.frame = CGRectMake(0, 0, self.view.width, self.view.width);
    
    CGFloat horizontalSpacingBetweenBottomButtons = 0;
    CGFloat verticalSpacingBetweenClockButtonAndVolumeSlider = 0;

    if (FEIsPhone) {
        switch ([kUtil screenInch]) {
            case FEScreenInch3_5:
                horizontalSpacingBetweenBottomButtons = 120;
                verticalSpacingBetweenClockButtonAndVolumeSlider = 50;
                break;
            
            case FEScreenInch4_7:
                horizontalSpacingBetweenBottomButtons = 120;
                verticalSpacingBetweenClockButtonAndVolumeSlider = 40;
                break;
                
            case FEScreenInch4_0:
                horizontalSpacingBetweenBottomButtons = 80;
                verticalSpacingBetweenClockButtonAndVolumeSlider = 25;
                break;
                
            default:
                horizontalSpacingBetweenBottomButtons = 80;
                verticalSpacingBetweenClockButtonAndVolumeSlider = 40;
                break;
        }
    }
    else {
        horizontalSpacingBetweenBottomButtons = 120;
        verticalSpacingBetweenClockButtonAndVolumeSlider = 25;
    }
    
    //
    _repeatButton.right = self.view.width / 2 - horizontalSpacingBetweenBottomButtons;
    _shuffleButton.x = self.view.width / 2 + horizontalSpacingBetweenBottomButtons;
    
    //
    _volumeSlider.y = _clockButton.y - verticalSpacingBetweenClockButtonAndVolumeSlider;
    _minVolumeImageView.centerY = _volumeSlider.centerY;
    _minVolumeImageView.right = _volumeSlider.x - 10;
    
    _maxVolumeImageView.centerY = _volumeSlider.centerY;
    _maxVolumeImageView.x = _volumeSlider.right + 10;
    
    //
    _playOrPauseButton.bottom = _volumeSlider.y;
    _backwordButton.centerY = _playOrPauseButton.centerY;
    _backwordButton.right = self.view.width / 2 - 85;
    
    _forwardButton.centerY = _playOrPauseButton.centerY;
    _forwardButton.x = self.view.width / 2 + 85;
    
    //
    _trackTitleLabel.bottom = _playOrPauseButton.y - 10;
    
    //
    _progressSlider.bottom = _trackTitleLabel.y;
    _currentTimeLabel.centerY = _progressSlider.centerY;
    _currentTimeLabel.right = _progressSlider.x - 5;
    
    _durationLabel.centerY = _progressSlider.centerY;
    _durationLabel.x = _progressSlider.right + 5;
}

- (void)volumeDidChange:(NSNotification*)notification
{
    if (_volumeSlider.dragging == NO) {
        _volumeSlider.value = playerForSettingVolume.volume;
    }
}

#pragma mark - TableView delegate & datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _tracks.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *kIdentifier = @"I";
    TrackCell *cell = [tableView dequeueReusableCellWithIdentifier:kIdentifier];
    if (cell == nil) {
        cell = [[TrackCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kIdentifier];
        cell.backgroundColor = _tableView.backgroundColor;
        
        cell.imageViewMargin = UIEdgeInsetsMake(2, 15, 2, 7);
        
        cell.textLabel.numberOfLines = 1;
        cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14];
    }
    
    Track *track = _tracks[indexPath.row];
    cell.textLabel.text = track.title;
    cell.detailTextLabel.text = (track.duration > 0 ? [kGlobal formatSeconds:track.duration] : nil);
    [cell.imageView sd_setImageWithURL:[NSURL URLWithString:track.iconURL] placeholderImage:kDefaultTrackArtwork];
    
    if (indexPath.row == _currentIndex) {
        cell.textLabel.textColor = kGlobal.tintColor;
    }
    else {
        cell.textLabel.textColor = FEHexRGBA(0x000000, 1.0);
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.row == _currentIndex) {
        //
    }
    else {
        [self playTrackAtIndex:indexPath.row];
        [kGlobal increasePlayTimesAndProcess];
    }
}

#pragma mark - Handler
- (void)timerHandler:(NSTimer*)timer {
    if (_progressSlider.dragging) {
        return;
    }

    // 更新当前时间label、总时间Label、进度slider
    NSInteger duration = [self duration];
    duration = MAX(duration, 0);
    
    if (_isUsingItunesMusicPlayer) {
        if (isnan(_iTunesMusicPlayer.currentPlaybackTime) == NO) {
            _currentTimeLabel.text = [kGlobal formatSeconds:_iTunesMusicPlayer.currentPlaybackTime];
            _durationLabel.text = [kGlobal formatSeconds:duration];
            _progressSlider.value = (duration > 0 ? _iTunesMusicPlayer.currentPlaybackTime / duration : 0);
        }
    }
    else {
        _currentTimeLabel.text = [kGlobal formatSeconds:_audioPlayer.progress];
        _durationLabel.text = [kGlobal formatSeconds:duration];
        _progressSlider.value = (duration > 0 ? _audioPlayer.progress / duration : 0);
    }

    // 定时停止播放器
    if (_timeForStoppingPlaying) {
        NSTimeInterval interval = [_timeForStoppingPlaying timeIntervalSinceDate:[NSDate date]];
        if (interval < 0) {
            [self disableClock];
            [self stop];
        }
        else {
            [_clockButton setTitle:FEStringWithFormat(@" %@", [kGlobal formatSeconds:interval]) forState:UIControlStateNormal];
        }
    }
}

- (void)repeatButtonHandler:(id)sender {
    FESetUserDefaults(kUDKeyRepeatMode, @(_repeatButton.flag));
}

- (void)shuffleButtonHandler:(id)sender {
    //
}

- (void)playOrPauseButtonHandler:(id)sender {
    if ([self isPlaying]) {
        [self pause];
    }
    else {
        [self play];
    }
}

- (void)clockButtonHandler:(id)sender {
    FEAlertController *sheet = [[FEAlertController alloc] initWithTitle:@"Sleep Timer" message:nil buttonTitles:@[@"10 mins", @"20 mins", @"30 mins", @"60 mins", @"90 mins"] cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Off" preferredStyle:UIAlertControllerStyleActionSheet handler:^(FEAlertController *c, NSString *buttonTitle) {
        
        if (FEStringEqual(buttonTitle, @"Cancel")) {
            //
        }
        else if (FEStringEqual(buttonTitle, @"Off")) {
            [self disableClock];
        }
        else {
            NSString *stringMinutes = [[buttonTitle componentsSeparatedByString:@" "] firstObject];
            int seconds = [stringMinutes intValue] * 60;
            _timeForStoppingPlaying = [[NSDate date] dateByAddingTimeInterval:seconds];
            _clockButton.flag = kFlagClockEnabled;
            [_clockButton setTitle:FEStringWithFormat(@" %@", [kGlobal formatSeconds:seconds-1]) forState:UIControlStateNormal];
        }
    }];
    sheet.sourceRect = _clockButton.frame;
    [sheet showInController:self animated:YES completion:nil];
}

- (IBAction)backwardButtonHandler:(id)sender {
    if (_tracks.count <= 1) {
        return;
    }
    
    int index = (int)_currentIndex - 1;
    if (index < 0) {
        index += _tracks.count;
    }
    [self playTrackAtIndex:index];
}

- (IBAction)forwardButtonHandler:(id)sender {
    if (_tracks.count <= 1) {
        return;
    }
    
    NSUInteger index = (_currentIndex+1) % _tracks.count;
    [self playTrackAtIndex:index];
}

- (void)trackListButtonHandler:(id)sender {
    _showsTrackList = !_showsTrackList;
    UIButton *btn = (UIButton*)self.navigationItem.rightBarButtonItem.customView;
    
    // animation
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:btn cache:YES];
    
    [self makeupTrackListButton];
    
    [UIView commitAnimations];
    
    // animation
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_container cache:YES];
    
    [_container bringSubviewToFront:(_showsTrackList ? _tableView : _artworkImageView)];
    
    [UIView commitAnimations];
}

#pragma mark - STKAudioPlayerDelegate
- (void)audioPlayer:(STKAudioPlayer *)audioPlayer stateChanged:(STKAudioPlayerState)state previousState:(STKAudioPlayerState)previousState {
    NSLog(@"%s %ld", __func__, (long)state);
    FEPostNotification(PlayerViewControllerPlayingStateChangedNotification, nil, nil);
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updatePlayOrPauseButton) object:nil];
    [self performSelector:@selector(updatePlayOrPauseButton) withObject:nil afterDelay:0.1];
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer didFinishPlayingQueueItemId:(STKDataSource *)queueItemId withReason:(STKAudioPlayerStopReason)stopReason andProgress:(double)progress andDuration:(double)duration {
    NSLog(@"%s %f %f %ld", __func__, progress, duration, (long)stopReason);
    
    if (stopReason == STKAudioPlayerStopReasonEof) {
        
        BOOL isLastTrack = (_currentIndex + 1 >= _tracks.count);
        if (isLastTrack && _repeatButton.flag == kFlagRepeatNone) {
            return;
        }
        
        if (_repeatButton.flag == kFlagRepeatOne) {
            [_audioPlayer playDataSource:queueItemId];
        }
        else {
            if ([self isShuffled] == NO) {
                NSUInteger index = (_currentIndex + 1) % _tracks.count;
                [self playTrackAtIndex:index];
            }
            else {
                [self playTrackAtIndex:[self generateShuffledIndex]];
            }
        }
    }
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer unexpectedError:(STKAudioPlayerErrorCode)errorCode {
    //
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer didStartPlayingQueueItemId:(NSObject *)queueItemId {
    //
}

- (void)audioPlayer:(STKAudioPlayer *)audioPlayer didFinishBufferingSourceWithQueueItemId:(NSObject *)queueItemId {
    //
}

#pragma mark - Private
- (NSInteger)duration {
    if (_isUsingItunesMusicPlayer) {
        return [self currentTrack].duration;
    }
    
    return (_audioPlayer.duration > 0 ? _audioPlayer.duration : [self currentTrack].duration);
}

- (void)updateUIForCurrentTrack {
    if (_tracks.count == 0) {
        self.navigationController.title = nil;
        _artworkImageView.image = kDefaultLargeTrackArtwork;
        _trackTitleLabel.text = @"No Track";
        _durationLabel.text = [kGlobal formatSeconds:0];
    }
    else {
        Track *track = [self currentTrack];
        
        self.navigationItem.title = FEStringWithFormat(@"%d / %d", (int)_currentIndex+1, (int)_tracks.count);
        [self updateArtworkWithTrack:track];
        _trackTitleLabel.text = track.title;
        _durationLabel.text = [kGlobal formatSeconds:track.duration];
        [self makeupTrackListButton];
    }
}
- (void)updatePlayOrPauseButton {
    _playOrPauseButton.flag = ([self isPlaying] ? kFlagPlaying : kFlagPaused);
}

- (void)seekToTime:(NSTimeInterval)time {
    if (_isUsingItunesMusicPlayer) {
        if (_iTunesMusicPlayer) {
            _iTunesMusicPlayer.currentPlaybackTime = time;
        }
    }
    else {
        if (_audioPlayer) {
            [_audioPlayer seekToTime:time];
        }
    }
}

- (void)makeupTrackListButton {
    UIButton *btn = (UIButton*)self.navigationItem.rightBarButtonItem.customView;
    if (_showsTrackList == NO) {
        UIImage *icon = [kUtil tintImage:[UIImage imageNamed:@"List"] withColor:kGlobal.tintColor];
        [btn setImage:icon forState:UIControlStateNormal];
    }
    else {
        [btn sd_setImageWithURL:[NSURL URLWithString:[self currentTrack].iconURL] forState:UIControlStateNormal placeholderImage:kDefaultTrackArtwork];
    }
}

- (void)disableClock {
    _timeForStoppingPlaying = nil;
    _clockButton.flag = kFlagClockDisabled;
    [_clockButton setTitle:nil forState:UIControlStateNormal];
}

- (void)createPlayerItemWithTrack:(Track*)track completionHandler:(void (^)(AVPlayerItem *item))completionHandler {
    NSURL *url = nil;
    
    if (track.isAudioDownloaded) {
        if (track.extension == nil) {
            NSString *symbolLinkPath = [self createFileSymbolLinkForTrack:track];
            url = [NSURL fileURLWithPath:symbolLinkPath];
        }
        else {
            url = [NSURL fileURLWithPath:track.audioLocalPath];
        }
        
        AVPlayerItem *item = [AVPlayerItem playerItemWithURL:url];
        completionHandler(item);
    }
    else {
        [kGlobal fetchStreamRequestForTrack:track completionHandler:^(NSURLRequest *request) {
            NSDictionary *headers = request.allHTTPHeaderFields;
            NSDictionary *options = (headers != nil ? @{ @"AVURLAssetHTTPHeaderFieldsKey": request.allHTTPHeaderFields } : @{});
            AVURLAsset *asset = [AVURLAsset URLAssetWithURL:request.URL options:options];
            AVPlayerItem *item = [AVPlayerItem playerItemWithAsset:asset];
            completionHandler(item);
        }];
    }
}

- (void)updateArtworkWithTrack:(Track*)track {
    _artworkImageView.image = kDefaultLargeTrackArtwork;
}

- (NSUInteger)generateShuffledIndex {
    NSUInteger index = arc4random() % _tracks.count;
    while (_tracks.count > 1 && index == _currentIndex) {
        index = arc4random() % _tracks.count;
    }
    return index;
}

- (void)setControlCenterPlayingInfoWithTrack:(Track*)track {
    UIImage *image = _artworkImageView.image;
    if (image == nil) {
        image = kDefaultLargeTrackArtwork;
    }
    
    MPNowPlayingInfoCenterUpdatable = YES;
    MPNowPlayingInfoCenterUpdatable = YES;
    MPMediaItemArtwork *artwork = [[MPMediaItemArtwork alloc] initWithImage:image];
    [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:@{
                MPMediaItemPropertyTitle: track.title ? track.title : kAppName,
                MPMediaItemPropertyPlaybackDuration: @(track.duration),
                MPMediaItemPropertyArtist: track.artist ? track.artist : kAppName,
                MPMediaItemPropertyArtwork: artwork
                }];
    MPNowPlayingInfoCenterUpdatable = NO;
}

- (NSString*)createFileSymbolLinkForTrack:(Track*)track
{
    NSString *audioPath = [track audioLocalPath];
    NSString *symbolLinkPath = [kUtil join:kUtil.documentsPath, @".mocha.mp3", nil];
    [kFM removeItemAtPath:symbolLinkPath error:nil];
    [kFM createSymbolicLinkAtPath:symbolLinkPath withDestinationPath:audioPath error:nil];
    return symbolLinkPath;
}

- (void)stop {
    [_iTunesMusicPlayer stop];
    [_audioPlayer stop];
}

- (void)playTrackAtIndex:(NSUInteger)index {
    [self stop];
    _currentIndex = index;
    [self play];
}

#pragma mark - Public
- (void)setTracks:(NSArray*)tracks currentIndex:(NSUInteger)currentIndex
{
    Track *backupCurrentTrack = [self currentTrack];
    
    _tracks = tracks;
    _currentIndex = currentIndex;
    
    if (backupCurrentTrack != [self currentTrack]) {
        [self stop];
    }
}

- (void)play
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    if (session.category != AVAudioSessionCategoryPlayback) {
        [session setCategory:AVAudioSessionCategoryPlayback error:nil];
        [session setActive:YES error:nil];
    }
    
    if (_tracks.count == 0) {
        return;
    }
    
    [self setupPlayersIfNeeded];
    
    if (_iTunesMusicPlayer.playbackState == MPMusicPlaybackStatePaused && isnan(_iTunesMusicPlayer.currentPlaybackTime) == NO) { // 暂停
        [_iTunesMusicPlayer play];
        return;
    }
    
    if (_audioPlayer.state == STKAudioPlayerStatePaused) { // 暂停
        [_audioPlayer resume];
        return;
    }
    
    Track *track = [self currentTrack];
    _progressSlider.value = 0;
    [self updateUIForCurrentTrack];
    [self setControlCenterPlayingInfoWithTrack:track];
    
    [_tableView reloadData];
    
    if (track.source == TrackSourceiTunesMusic) {
        _isUsingItunesMusicPlayer = YES;
        
        MPMediaItem *item = [kiTunesMusicSource findMediaItemWithTrack:track];
        MPMediaItemCollection *collection = [MPMediaItemCollection collectionWithItems:@[item]];
        [_iTunesMusicPlayer setQueueWithItemCollection:collection];
        [_iTunesMusicPlayer play];
    }
    else {
        _isUsingItunesMusicPlayer = NO;
        
        if ([track isAudioDownloaded]) {
            NSURL *url = [NSURL fileURLWithPath:track.audioLocalPath];
            STKDataSource *dataSource = [STKAudioPlayer dataSourceFromURL:url];
            setTimeout(0.1, ^{
                [_audioPlayer playDataSource:dataSource];
            });
        }
        else {
            [kGlobal fetchStreamRequestForTrack:track completionHandler:^(NSURLRequest *request) {
                STKDataSource *dataSource = [STKAudioPlayer dataSourceFromURL:request.URL headers:request.allHTTPHeaderFields];
                [_audioPlayer playDataSource:dataSource];
            }];
        }
    }
    
}

- (void)pause
{
    [_iTunesMusicPlayer pause];
    [_audioPlayer pause];
}

- (void)playNextTrack
{
    [self forwardButtonHandler:_forwardButton];
}

- (void)playPreviousTrack
{
    [self backwardButtonHandler:_backwordButton];
}

- (void)setIsShuffled:(BOOL)isShuffled
{
    _isShuffled = isShuffled;
    _shuffleButton.flag = (isShuffled ? kFlagShuffleOn : kFlagShuffleOff);
}

- (Track*)currentTrack
{
    if (_tracks.count == 0 || _currentIndex + 1 > _tracks.count) {
        return nil;
    }
    return _tracks[_currentIndex];
}

- (BOOL)isPlaying {
    if (_isUsingItunesMusicPlayer) {
        return _iTunesMusicPlayer.playbackState == MPMusicPlaybackStatePlaying;
    }
    
    return (_audioPlayer && _audioPlayer.state <= STKAudioPlayerStateBuffering);
}

#pragma mark - iTunesMusicPlayer
- (void)iTunesPlayerPlaybackStateDidChange:(NSNotification*)notification {
    NSLog(@"%s: %ld %f %f", __func__, (long)_iTunesMusicPlayer.playbackState, [self currentTrack].duration, _iTunesMusicPlayer.currentPlaybackTime);
    
    if (_isUsingItunesMusicPlayer == NO) {
        NSLog(@"%s return", __func__);
        return;
    }
    
    FEPostNotification(PlayerViewControllerPlayingStateChangedNotification, nil, nil);
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updatePlayOrPauseButton) object:nil];
    [self performSelector:@selector(updatePlayOrPauseButton) withObject:nil afterDelay:0.1];
    
    BOOL hasPlayedToEndTime = isnan(_iTunesMusicPlayer.currentPlaybackTime);
    if (hasPlayedToEndTime) {
        BOOL isLastTrack = (_currentIndex + 1 >= _tracks.count);
        if (isLastTrack && _repeatButton.flag == kFlagRepeatNone) {
            return;
        }
    
        if (_repeatButton.flag == kFlagRepeatOne) {
            _iTunesMusicPlayer.currentPlaybackTime = 0;
            [_iTunesMusicPlayer play];
        }
        else {
            if ([self isShuffled] == NO) {
                NSUInteger index = (_currentIndex + 1) % _tracks.count;
                [self playTrackAtIndex:index];
            }
            else {
                [self playTrackAtIndex:[self generateShuffledIndex]];
            }
        }
    }
}

#pragma mark - Private
- (void)setupPlayersIfNeeded {
    if (_iTunesMusicPlayer == nil) {
        _iTunesMusicPlayer = [MPMusicPlayerController systemMusicPlayer];
        [_iTunesMusicPlayer beginGeneratingPlaybackNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iTunesPlayerPlaybackStateDidChange:) name:MPMusicPlayerControllerPlaybackStateDidChangeNotification object:nil];
    }
    
    if (_audioPlayer == nil) {
        _audioPlayer = [[STKAudioPlayer alloc] initWithOptions:(STKAudioPlayerOptions){
            .flushQueueOnSeek = YES,
            .enableVolumeMixer = NO,
            .equalizerBandFrequencies = {50, 100, 200, 400, 800, 1600, 2600, 16000}
        }];
        _audioPlayer.meteringEnabled = YES;
        _audioPlayer.volume = 1;
        _audioPlayer.delegate = self;
    }
}

@end

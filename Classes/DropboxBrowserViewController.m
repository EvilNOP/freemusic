//
//  DropboxBrowserViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "DropboxBrowserViewController.h"
#import "FreeMusic-Swift.h"

@interface DropboxBrowserViewController()

@property (nonatomic, strong) DropboxListView *dropboxListView;

@end

@implementation DropboxBrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    self.dropboxListView = [[DropboxListView alloc] initWithController:self searching:NO];
    
    if (![self.dropboxListView isVerified]) {
        [kGlobal navigationItem:self.navigationItem customTitle:@"Dropbox"];
        [self.dropboxListView verify];
    } else {
        if (self.file == nil) {
            [kGlobal navigationItem:self.navigationItem customTitle:@"Dropbox"];
            [self.dropboxListView fetchFile:@""];
        } else {
            [kGlobal navigationItem:self.navigationItem customTitle:[self.file objectForKey:@"name"]];
            [self.dropboxListView fetchFile:[self.file objectForKey:@"path_display"]];
        }

    }
    
    __weak DropboxBrowserViewController *weakSelf = self;
    self.dropboxListView.cellSelectedHanlder = ^(NSInteger index, BOOL isFolder, NSDictionary *file) {
        if (isFolder) {
            DropboxBrowserViewController *dropboxBrowser = [[DropboxBrowserViewController alloc] init];
            dropboxBrowser.file = file;
            [weakSelf.navigationController pushViewController:dropboxBrowser animated:true];
        }
    };
    
    self.dropboxListView.cancelSignInHandler = ^ {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    
    [self.view addSubview:self.dropboxListView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    self.dropboxListView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
}


@end

//
//  NSDictionary+NestedKey.m
//  Drive
//
//  Created by EvilNOP on 15/3/10.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "NSDictionary+NestedKey.h"

@implementation NSDictionary (NestedKey)

- (id)valueForNestedKey:(NSString*)nestedKey
{
    NSArray *components = [nestedKey componentsSeparatedByString:@"."];
    id r = self[components[0]];
    if ([self isNilOrNull:r]) {
        return nil;
    }
    
    for (int i=1; i<components.count; i++) {
        NSString *key = components[i];
        r = r[key];
        if ([self isNilOrNull:r]) {
            return nil;
        }
    }
    
    return r;
}

#pragma mark - Private
- (BOOL)isNilOrNull:(id)value
{
    return (value == nil || value == [NSNull null]);
}

@end

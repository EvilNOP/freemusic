//
//  DropboxBrowserViewController.h
//  FreeMusic
//
//  Created by EvilNOP on 8/4/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropboxBrowserViewController : UIViewController

@property (nonatomic, strong) NSDictionary *file;

@end

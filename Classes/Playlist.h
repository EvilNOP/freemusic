//
//  Playlist.h
//  Drive
//
//  Created by EvilNOP on 15/3/12.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Playlist : Model

@property (nonatomic, strong) NSString *name;
@property (nonatomic) NSInteger order;
@property (nonatomic) BOOL isSelected;

- (int)numberOfTracks;
- (void)addTrack:(Track*)track;
- (void)addTracks:(NSArray*)tracks;
- (void)removeTrackWithID:(NSString*)trackID;
- (void)removeTracksWithIDs:(NSArray*)trackIDs;
- (NSArray*)tracks;
- (Track*)firstTrack;
- (Track*)lastTrack;

@end

//
//  Slider.h
//  Drive
//
//  Created by EvilNOP on 15/3/30.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kSliderHeight 40
#define kSliderHandleButtonWidth 44

@interface Slider : UIView

@property (nonatomic) float value;
@property (nonatomic) float minValue;
@property (nonatomic) float maxValue;
@property (nonatomic) BOOL draggable;
@property (strong, nonatomic) void (^valueChangedhandler) (Slider *slider);
@property (nonatomic, readonly) BOOL dragging;
@property (nonatomic) float trackHeight;

- (void)setHandleIcon:(UIImage*)icon;
- (void)setTrackColor:(UIColor*)color;
- (void)setProgressColor:(UIColor*)color;
- (void)setLoadedRangeColor:(UIColor*)color;
- (void)setLoadedRange:(NSRange)range;

@end

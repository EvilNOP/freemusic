//
//  Track.m
//  Drive
//
//  Created by EvilNOP on 15/3/11.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "Track.h"

static NSString * const kKeyRemoteID = @"id";
static NSString * const kKeyTitle = @"title";
static NSString * const kKeyIconURL = @"artwork_url";
static NSString * const kKeyLargeIconURL = @"large_icon_url";
static NSString * const kKeyDuration = @"full_duration";
static NSString * const kKeySource = @"source";
static NSString * const kKeySize = @"size";
static NSString * const kKeyContentURL = @"content_url";
static NSString * const kKeyExtension = @"extension";
static NSString * const kKeyArtist = @"artist";

@interface Track () <NSCoding> {
    
}

@end


@implementation Track

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    if (_remoteID) [aCoder encodeObject:_remoteID forKey:kKeyRemoteID];
    if (_title) [aCoder encodeObject:_title forKey:kKeyTitle];
    if (_iconURL) [aCoder encodeObject:_iconURL forKey:kKeyIconURL];
    if (_largeIconURL) [aCoder encodeObject:_largeIconURL forKey:kKeyLargeIconURL];
    if (_contentURL) [aCoder encodeObject:_contentURL forKey:kKeyContentURL];
    if (_extension) [aCoder encodeObject:_extension forKey:kKeyExtension];
    if (_artist) [aCoder encodeObject:_artist forKey:kKeyArtist];
    [aCoder encodeObject:@(_duration) forKey:kKeyDuration];
    [aCoder encodeObject:@(_source) forKey:kKeySource];
    [aCoder encodeObject:@(_size) forKey:kKeySize];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _remoteID = [aDecoder decodeObjectForKey:kKeyRemoteID];
        _title = [aDecoder decodeObjectForKey:kKeyTitle];
        _iconURL = [aDecoder decodeObjectForKey:kKeyIconURL];
        _largeIconURL = [aDecoder decodeObjectForKey:kKeyLargeIconURL];
        _contentURL = [aDecoder decodeObjectForKey:kKeyContentURL];
        _extension = [aDecoder decodeObjectForKey:kKeyExtension];
        _artist = [aDecoder decodeObjectForKey:kKeyArtist];
        _duration = [[aDecoder decodeObjectForKey:kKeyDuration] doubleValue];
        _source = [[aDecoder decodeObjectForKey:kKeySource] intValue];
        _size = [[aDecoder decodeObjectForKey:kKeySize] integerValue];
    }
    
    return self;
}

#pragma mark - Hook
- (void)willSave {
    if (self.ID == nil) {
        self.ID = _remoteID;
    }
}

- (void)willDelete {
    [kFM removeItemAtPath:[self audioLocalPath] error:nil];
}

#pragma mark - Public
- (NSString*)audioLocalPath {
    NSString *path = [kUtil join:kGlobal.cachedAudioRootPath, _remoteID, nil];
    if (_extension) {
        path = [NSString stringWithFormat:@"%@.%@", path, _extension];
    }
    
    return path;
}

- (BOOL)isAudioDownloaded {
    return [kFM fileExistsAtPath:[self audioLocalPath]];
}

@end

//
//  iTunesFileSharingTableViewController.m
//  FreeMusic
//
//  Created by EvilNOP on 8/10/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

#import "iTunesFileSharingTableViewController.h"
#import "FileDetector.h"

@interface iTunesFileSharingTableViewController ()

@property (nonatomic, strong) FileDetector *fileDetector;
@property (weak, nonatomic) IBOutlet UIImageView *oneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *twoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *threeImageView;

@end

@implementation iTunesFileSharingTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *attribute = @{
                                NSForegroundColorAttributeName: colorForKey(@"firstFontColor"),
                                NSFontAttributeName: [UIFont systemFontOfSize:18.0 weight:UIFontWeightMedium],
                                NSKernAttributeName: @1
                                };
    UILabel *titleLabel = [UILabel new];
    
    
    titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"iTunes FILE SHARING" attributes:attribute];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    self.navigationItem.rightBarButtonItem = [kGlobal createPlayerBarButtonItem];
    [kGlobal navigationBar:self.navigationController.navigationBar navigationItem:self.navigationItem backIndicatorWithImageName:@"Back"];
    
    self.tableView.tableFooterView = [UIView new];
    
    
    if (TARGET_IPHONE_SIMULATOR){
    } else {
        NSLog(@"%@", kUtil.documentsPath);
        [self fileDetect];
    }
    
    self.oneImageView.image = [kUtil tintImage:[UIImage imageNamed:@"One"] withColor:colorForKey(@"primaryColor")];
    self.twoImageView.image = [kUtil tintImage:[UIImage imageNamed:@"Two"] withColor:colorForKey(@"primaryColor")];
    self.threeImageView.image = [kUtil tintImage:[UIImage imageNamed:@"Three"] withColor:colorForKey(@"primaryColor")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.fileDetector != nil) {
        [self.fileDetector stop];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.fileDetector != nil) {
        [self.fileDetector run];
    }
}

- (void)fileDetect {
    
    self.fileDetector = [FileDetector detectFilesAtPath:kUtil.documentsPath detectingHandler:^(FileDetector *detector, Boolean changed, NSDictionary *subPathToAttributes) {
        
        NSMutableDictionary *subFilePathToAttributes = [NSMutableDictionary dictionary];
        [subPathToAttributes enumerateKeysAndObjectsUsingBlock:^(id key, NSDictionary *attributes, BOOL *stop) {
            if (attributes[NSFileType] == NSFileTypeRegular) {
                subFilePathToAttributes[key] = attributes;
            }
        }];
        
        NSArray *filePaths = [subFilePathToAttributes allKeys];
        
        if (filePaths.count > 0) {
            if ([MBProgressHUD HUDForView:self.tableView] == nil) {
                [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
            }
        }
        
        if (filePaths.count > 0 && changed == NO) { // 开始导入
            [self.fileDetector stop];
            
            for (NSString *path in filePaths) {
                
                NSString *sourcePath = [kUtil.documentsPath stringByAppendingPathComponent:path];
                NSURL *sourceURL = [NSURL fileURLWithPath:sourcePath];
                
                Track *track = [[Track alloc] init];
                track.title = [path lastPathComponent];
                track.remoteID = [NSString stringWithFormat:@"%u", arc4random()];
                track.source = TrackSourceImport;
                
                [track save];
                
                [kFM moveItemAtURL:sourceURL toURL:[NSURL fileURLWithPath:track.audioLocalPath] error:nil];
            }
            
            [self.fileDetector run];
            [MBProgressHUD hideHUDForView:self.tableView animated:YES];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];
            hud.label.text = @"Import completed!";
            hud.mode = MBProgressHUDModeText;
            [hud hideAnimated:YES afterDelay:1.0];
        }
    }];
    
    NSString *adRegex = [NSString stringWithFormat:@"(^%@($|/))", @".ad"];
    NSString *dataRegex = [NSString stringWithFormat:@"(^%@($|/))", @".data"];
    NSString *hiddenFilesRegex = @"(^\\.|/\\.)"; // 以.开头 或 包含'/.'字符的路径
    NSString *regex = [@[adRegex, dataRegex, hiddenFilesRegex] componentsJoinedByString:@"|"];
    self.fileDetector.ignoreFilesBlock = ^ BOOL (FileDetector *detector, NSString *subPath) {
        return [subPath rangeOfString:regex options:NSRegularExpressionSearch].length > 0;
    };
    
    [self.fileDetector run];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (UIImage *)musicImageWithMusicURL:(NSURL *)url {
    NSData *data = nil;
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:url options:nil];
    for (NSString *format in [asset availableMetadataFormats]) {
        for (AVMetadataItem *metadataItem in [asset metadataForFormat:format]) {
            if ([metadataItem.commonKey isEqualToString:@"artwork"]) {
                data = (NSData *)metadataItem.value;
                break;
            }
        }
    }
    if (!data) {
        return [UIImage imageNamed:@"LargeTrackArtwork"];
    }
    return [UIImage imageWithData:data];
}

@end

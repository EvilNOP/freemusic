//
//  Global.swift
//  FreeMusic
//
//  Created by EvilNOP on 8/8/16.
//  Copyright © 2016 EvilNOP. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: - Shortcut
var kGlobal: Global {
    return Global.sharedInstance() as! Global
}

var kUtil: FEUtil {
    return FEUtil.sharedInstance() as! FEUtil
}

var kGoogleDriveSource: GoogleDriveSource {
    return GoogleDriveSource.sharedInstance
}

var kDropboxSource: DropboxSource {
    return DropboxSource.sharedInstance
}

var kOneDriveSource: OneDriveSource {
    return OneDriveSource.sharedInstance
}

var kDriveFileFetcher: DriveFileFetcher {
    return DriveFileFetcher.sharedInstance() as! DriveFileFetcher
}

var kOnlineParamsManager: OnlineParamsManager {
    return OnlineParamsManager.sharedInstance() as! OnlineParamsManager
}

// MARK: - Helper
func newJSONWithData(data: NSData?) -> SwiftyJSON.JSON? {
    if data != nil {
        return SwiftyJSON.JSON(data: data!)
    }
    
    return nil
}

func colorForKey(key: String?) -> UIColor {
    return kGlobal.colorForKey(key) ?? UIColor.clearColor()
}

//
//  LoadingIndicatorView.h
//  TripNote
//
//  Created by EvilNOP on 13-5-22.
//  Copyright (c) 2013年 iMac. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoadingIndicatorWrapperDelegate;

@interface LoadingIndicatorWrapper : UIView {
    UIActivityIndicatorView *_indicator;
    UILabel *_label;
    UIView *_indicatorBackgroundView;
    UIButton *_cancelButton;
}

@property (weak) id<LoadingIndicatorWrapperDelegate> delegate;
- (void)setText:(NSString*)text;
- (void)setCancelButtonVisible:(BOOL)visible;

@end



@protocol LoadingIndicatorWrapperDelegate <NSObject>

- (void)loadingIndicatorWrapperDidCancel:(LoadingIndicatorWrapper*)loadingIndicatorWrapper;

@end

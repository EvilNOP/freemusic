//
//  LoadingIndicatorView.m
//  TripNote
//
//  Created by EvilNOP on 13-5-22.
//  Copyright (c) 2013年 iMac. All rights reserved.
//

#import "LoadingIndicatorWrapper.h"
#import <QuartzCore/QuartzCore.h>

#define kWrapperColor FEHexRGBA(0x000000, 0.7)

@implementation LoadingIndicatorWrapper

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    _indicatorBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
    _indicatorBackgroundView.backgroundColor = kWrapperColor;
    _indicatorBackgroundView.layer.cornerRadius = 5.0;
    _indicatorBackgroundView.layer.masksToBounds = YES;
    [self addSubview:_indicatorBackgroundView];
    
    _indicator = [[UIActivityIndicatorView alloc] init];
    _indicator.frame = CGRectMake(0, 0, 40, 40);
    _indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    [_indicatorBackgroundView addSubview:_indicator];
    
    _label = [[UILabel alloc] init];
    _label.numberOfLines = 2;
    _label.font = [UIFont boldSystemFontOfSize:13];
    _label.textColor = [UIColor whiteColor];
    _label.backgroundColor = [UIColor clearColor];
    _label.textAlignment = NSTextAlignmentCenter;
    [_indicatorBackgroundView addSubview:_label];
    
    _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
    [_cancelButton setImage:[UIImage imageNamed:@"LoadingIndicatorWrapper.bundle/Cancel.png"] forState:UIControlStateNormal];
    [_cancelButton addTarget:self action:@selector(cancelButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    _cancelButton.hidden = YES;
    [self addSubview:_cancelButton];
    
    self.backgroundColor = FEHexRGBA(0x000000, 0.2);
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview) {
        [_indicator startAnimating];
    }
    else {
        [_indicator stopAnimating];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _indicatorBackgroundView.center = CGPointMake(self.width/2.0, self.height/2.0);
    _indicator.center = CGPointMake(_indicatorBackgroundView.width/2.0, _indicatorBackgroundView.height/2.0 - 8);
    _cancelButton.center = CGPointMake(_indicatorBackgroundView.right-3, _indicatorBackgroundView.y+3);
    
    CGSize size = [@"x" sizeWithFont:_label.font];
    float heightForTwoLine = size.height * 2;
    _label.frame = CGRectMake(0, (_indicatorBackgroundView.height/2.0)+10, _indicatorBackgroundView.width, heightForTwoLine);
}

#pragma mark - Public
- (void)setText:(NSString *)text
{
    _label.text = text;
}

- (void)setCancelButtonVisible:(BOOL)visible
{
    _cancelButton.hidden = !visible;
}

- (void)cancelButtonHandler:(id)sender
{
    if ([_delegate respondsToSelector:@selector(loadingIndicatorWrapperDidCancel:)]) {
        [_delegate loadingIndicatorWrapperDidCancel:self];
    }
}

@end

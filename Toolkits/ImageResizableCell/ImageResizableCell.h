//
//  ImageResizableCell.h
//  OnePass
//
//  Created by EvilNOP on 14/9/14.
//  Copyright (c) 2014年 Elite. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageResizableCell : UITableViewCell

@property (nonatomic, readonly) UITableViewCellStyle style;
@property (nonatomic) UIEdgeInsets imageViewMargin;
@property (nonatomic) UIEdgeInsets separatorMargin;
@property (nonatomic) BOOL hidesSeparator;

@end

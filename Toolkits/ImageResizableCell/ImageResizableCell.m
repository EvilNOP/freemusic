//
//  ImageResizableCell.m
//  OnePass
//
//  Created by EvilNOP on 14/9/14.
//  Copyright (c) 2014年 Elite. All rights reserved.
//

#import "ImageResizableCell.h"

@interface ImageResizableCell () {
    UIView *_separator;
}
@end

@implementation ImageResizableCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initializeImageResizeCell];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _style = style;
        [self initializeImageResizeCell];
    }
    
    return self;
}

- (void)initializeImageResizeCell
{
    _separator = [[UIView alloc] init];
    _separator.backgroundColor = FEHexRGBA(0xadadad, 1.0);
    [self addSubview:_separator];
    _separatorMargin = UIEdgeInsetsMake(0, 15, 0, 0);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _separator.frame = CGRectMake(_separatorMargin.left,
                                  self.height - 0.5,
                                  self.width - _separatorMargin.left - _separatorMargin.right,
                                  0.5);
    
    if (_style == UITableViewCellStyleValue2) {
        return;
    }
    
    float height = self.height - _imageViewMargin.top - _imageViewMargin.bottom;
    float width = height;
    self.imageView.frame = CGRectMake(_imageViewMargin.left, _imageViewMargin.top, width, height);
    self.textLabel.x = self.imageView.right + _imageViewMargin.right;
    
    if (_style == UITableViewCellStyleDefault) {
        self.textLabel.width -= (self.textLabel.right - self.contentView.width);
    }
    else if (_style == UITableViewCellStyleSubtitle) {
        self.textLabel.width -= (self.textLabel.right - self.contentView.width);
        self.detailTextLabel.x = self.textLabel.x;
        self.detailTextLabel.width = self.textLabel.width;
    }
    else if (_style == UITableViewCellStyleValue1) {
        float detailLabelWidth = 0;
        float detailLabelHeight = 0;
        float paddingRight = 8;
        if (self.detailTextLabel.text.length > 0) {
            CGSize size = [self.detailTextLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.detailTextLabel.height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.detailTextLabel.font} context:nil].size;
            detailLabelWidth = size.width;
            detailLabelHeight = self.detailTextLabel.height;
            
            self.detailTextLabel.frame = CGRectMake(self.contentView.width-detailLabelWidth-paddingRight,
                                                    (self.contentView.height-detailLabelHeight)/2.0,
                                                    detailLabelWidth,
                                                    detailLabelHeight);
        }
        
        self.textLabel.width = self.contentView.width - detailLabelWidth - paddingRight - self.textLabel.x;
    }
}

#pragma mark - Public
- (void)setHidesSeparator:(BOOL)hidesSeparator
{
    _hidesSeparator = hidesSeparator;
    _separator.hidden = hidesSeparator;
}

@end

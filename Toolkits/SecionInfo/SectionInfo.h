//
//  SectionInfo.h
//  memo
//
//  Created by EvilNOP on 15/1/23.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SectionInfo;
SectionInfo* newSectionInfo(NSString*title, NSArray *items, int tag);

@interface SectionInfo : NSObject

@property (strong, nonatomic) NSString *title;
@property (nonatomic) int tag;
@property (strong, nonatomic) NSArray *items;

- (id)initWithTitle:(NSString*)title items:(NSArray*)items tag:(int)tag;

@end

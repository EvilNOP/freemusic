//
//  SectionInfo.m
//  memo
//
//  Created by EvilNOP on 15/1/23.
//  Copyright (c) 2015年 EvilNOP. All rights reserved.
//

#import "SectionInfo.h"

SectionInfo* newSectionInfo(NSString*title, NSArray *items, int tag)
{
    return [[SectionInfo alloc] initWithTitle:title items:items tag:tag];
}

@implementation SectionInfo

- (id)initWithTitle:(NSString*)title items:(NSArray*)items tag:(int)tag
{
    self = [super init];
    if (self) {
        self.title = title;
        self.items = items;
        self.tag = tag;
    }

    return self;
}

@end

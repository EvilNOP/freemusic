//
//  UIColor+iOS7.h
//  FFCircularProgressView
//
//  Created by EvilNOP on 16/07/13.
//  Copyright (c) 2013 EvilNOP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (iOS7)

+ (UIColor *) ios7Blue;
+ (UIColor *) ios7Gray;

@end
